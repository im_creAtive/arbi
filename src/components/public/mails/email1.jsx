import { loginPage, playMarket } from './emailConst'

const email1 = (name) => ({
  "Title": "Welcome to Arbi",
  "Body": `
    <p>Hi ${name},</p>
     
    <p>Welcome to Arbi. You have just been added as a user to Arbi. I’d just like to let you know that we are
    here to answer any questions, which you can send to <a href="mailto:contact@arbi.io">contact@arbi.io</a>. Feedback is very appreciated.</p>
    
    <p><a href="${playMarket}">Click here</a> first to get the Arbi app, Then <a href="${loginPage}">click here</a> to get started straight away.</p>
    
    <p>Khalid and team Arbi.</p>
  `
})


export default email1;