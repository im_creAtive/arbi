
const email3 = (name) => ({
  "Title": "Your message to Arbi Team",
  "Body": `
    <p>Hi ${name},</p>  
    <p>Thank you for contacting us. Your message has been received and we will get back to you within 24 hours or less. Promised.</p>
    <p>
      Warm regards,<br>
      Arbi Customer Team.<br>
      <a href="arbi.io">www.arbi.io</a>
    </p>
  `
})


export default email3;