const emailConfirm = (name, href) => ({
  "Title": "Your access link for Arbi",
  "Body": `
    <p>Hi ${name},</p>
    <p>Thanks for signing up. Your email is confirmed. Please login using <a href="${href}">this activation link</a>.</p>
    
    <p>You will be asked to set your password and to login.</p>
    <p>Enjoy the free account. Play with it. Share it with friends and others. And please provide us with your feedback on how to improve this as much
    as possible.</p>
    
    <p>Arbi Customer Team.</p>
  `
})


export default emailConfirm;