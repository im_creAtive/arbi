import React from 'react'
import './style.css'

const getRandomId = () => {
  return "file-input-" + Math.floor(Math.random() * (1000 - 1)) + 1;
}

const FileInputThumbnail = (props) => {
  const randomId = getRandomId();

  return (
    <label htmlFor={randomId} className="purple-file-input">
      { props.photo && props.photo.uri ? (<img src={props.photo.uri} />) : null }
      <input id={randomId} type="file" onChange={e=>{props.onSelectPhoto(e)}} />
      <span>Select file ...</span>
      <div className="fileName">{ props.photo && props.photo.uri ? props.photo.name : '' }</div>
    </label>
  )
} 

export default FileInputThumbnail;