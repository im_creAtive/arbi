import React from 'react';
import {connect} from 'react-redux';
import * as modelAction from './../../../actions/modelAction';
import defaultLoaderSrc from './gif-load.gif';
import defaultErrorSrc from './default-cover.png';

const loadingStyle = {
  width: '30%',
  margin: '60px auto',
  display: 'block'
}

class Image extends React.Component {

  constructor(props){
      super(props);
      
      this.state = {
          file: this.props.loaderSrc ? this.props.loaderSrc : defaultLoaderSrc,
          isShowLoader: this.props.isShowLoader ? this.props.isShowLoader : false
      }
  }

  componentDidMount(){
      this._mounted = true;

      modelAction.downloadFile(this.props.src, this.state.isShowLoader).then(file=>{
          if(this._mounted){
              this.state.file = URL.createObjectURL(file);
              this.setState(this.state);
          }
      }, error=>{
          if(this._mounted){
              this.state.file = defaultErrorSrc;
              this.setState(this.state);
          }
      });
  }

  componentWillUnmount(){
      this._mounted = false;
  }

	render(){
    	return(
        <img style={this.state.file === defaultLoaderSrc ?  loadingStyle : this.props.style} srcSet={this.state.file} />
    	)
    }
}

export default Image;