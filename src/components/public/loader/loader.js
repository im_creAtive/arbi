import React from 'react';
import './loader.css';
import {connect} from 'react-redux';

class Loader extends React.Component {
	render(){
    	return(
            <section hidden={this.props.loader.calls == 0} id="loader-section">
                <div id="loader-element">
                    <div className="sk-folding-cube">
                        <div className="sk-cube1 sk-cube"></div>
                        <div className="sk-cube2 sk-cube"></div>
                        <div className="sk-cube4 sk-cube"></div>
                        <div className="sk-cube3 sk-cube"></div>
                    </div>
                </div>
            </section>
    	)
    }
}

export default connect(store => ({ loader: store.loader }))(Loader);