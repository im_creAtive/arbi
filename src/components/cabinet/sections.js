import React, { Component } from 'react';
import Upload from './sections/upload-file';
import Library from './sections/library';
import Users from './sections/users';
import Telemetry from './sections/telemetry';
import '../../sources/css/admin/main.css';

import { Switch , Route , Redirect } from 'react-router'

class CabinetSections extends Component {
    render() {
        return (
            <main className="content">
                <div className="section-contents">
                    <div className="content-card">
                    <section className="container-fluid">
                        <Telemetry />
                        <div className="row">
                            <div className="col-sm-12 test">
                                <Switch>
                                    <Route exact path="/cabinet/upload" component={Upload}/>
                                    <Route exact path="/cabinet/library" component={Library}/>
                                    <Route exact path="/cabinet/users" component={Users}/>
                                    <Redirect from="/cabinet" to="/cabinet/upload" />
                                </Switch>
                            </div>
                        </div>
                    </section>
                </div>
                </div>
            </main>
           
        );
    }
}

export default CabinetSections;
