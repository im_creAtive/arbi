import React, { Component } from 'react';
import config from './../../api/config';
import * as userAction from './../../actions/userAction';

import Header from './sections/header'
import Menu from './sections/menu'
import CabinetSections from './sections'
import '../../sources/css/admin/app.css'
import 'react-select/dist/react-select.css';

//require("bootstrap/less/bootstrap.less");

class CabinetPage extends Component {

    constructor(props){
		super(props);
    }
    
    componentDidMount(){
        userAction.getCurrent();
    }
    
    componentWillReceiveProps(){
        if (!config.getToken()){
            this.props.history.push("/");
        }
    }

    render() {
        return(
            <div id="content-admin">
                <Header />
                <Menu />
                <CabinetSections />
            </div>
        )
    }
}

export default CabinetPage;
