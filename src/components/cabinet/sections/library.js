import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link, NavLink } from 'react-router-dom';
// import SwitchButton from 'react-switch-button';
import { SortableContainer, SortableElement, arrayMove } from 'react-sortable-hoc';

import config from './../../../api/config';

import * as notification from './../../../helpers/notificationHelper';

import * as folderAction from './../../../actions/folderAction';
import * as modelAction from './../../../actions/modelAction';
import * as targetAction from './../../../actions/targetAction';

import ReactPaginate from 'react-paginate';
import Image from './../../public/image/image';

// import 'react-switch-button/dist/react-switch-button.css';
import "../../../sources/css/admin/app.css";
import '../../../sources/css/admin/library.css';

import addFolder from '../../../sources/img/addFolder.png';
import formatModel from '../../../sources/img/obj.png';
import formatAsset from '../../../sources/img/unity.png';
import formatTarget from '../../../sources/img/target.png';
import FolderController from './folderController';

const Mode_Models = 'Mode_Models';
const Mode_Targets = 'Mode_Targets';


class Library extends Component {

    constructor(props){
        super(props);
        this.state = {
            mode: Mode_Models,
            folder:{
                Name: ""
            },
            pageModels: {
                Filter: {
                    FolderId: null,
                    Name: '',
                },
                Items: [],
                ObjectsOnPage: 50,
                CurrentPage: 1,
                TotalPages: 0
            },
            pageTargets: {
                Filter: {
                    FolderId: null,
                    Name: '',
                },
                Items: [],
                ObjectsOnPage: 50,
                CurrentPage: 1,
                TotalPages: 0
            },
            search: ''
        };
    }

    isInRole(role){
        return this.props.store.user.data.RoleIds ? this.props.store.user.data.RoleIds.find(x=>x == role || x == 'Admin') != null : false;
    }

    onChangeMode(e){

        if(e) e.preventDefault();

        this.setState({ mode: this.state.mode === Mode_Models ? Mode_Targets : Mode_Models }, () => {
          this.onLoad()
        })

    }

    onChangeFolderName(value, prop){
        this.state.folder[prop] = value;
        this.setState(this.state);
    }

    onCreateFolder(){
        let folder = Object.assign({}, this.state.folder);
        folder.IndexOfFolder = "" + this.props.store.folders.list.length;

        folderAction.add(folder).then(ok=>{
            notification.success('Folder created')
            folderAction.get();
            this.setState({ folder: { Name: "" } })
        }, error=>{
            notification.error(error);
        });
    }

    async componentDidMount(){
        await folderAction.get();
        this.onLoad();
        this.defaultFolder();
    }

    onLoadModels(){
      let pageModels = Object.assign({}, this.state.pageModels);
      let pageTargets = Object.assign({}, this.state.pageTargets);
      pageModels.Items = [];
      pageTargets.Items = [];
      pageModels.Filter.Name = this.state.search;
      pageTargets.Filter.Name = this.state.search;

      this.setState({ pageModels, pageTargets });

      modelAction.filter(pageModels).then(page => {
        pageModels = page;
        this.setState({ pageModels });
      }, error => {
        notification.error(error);
      });
    }

    onLoadTargets(){
      let pageModels = Object.assign({}, this.state.pageModels);
      let pageTargets = Object.assign({}, this.state.pageTargets);
      pageModels.Items = [];
      pageTargets.Items = [];
      pageModels.Filter.Name = this.state.search;
      pageTargets.Filter.Name = this.state.search;

      this.setState({ pageModels, pageTargets });

      targetAction.filter(pageTargets).then(page => {
        pageTargets = page;
        this.setState({ pageTargets });
      }, error => {
        notification.error(error);
      });
    }

    onLoad(){
        switch(this.state.mode){
            case Mode_Models:{
                this.onLoadModels();
                break;
            }
            case Mode_Targets:{
                this.onLoadTargets();
                break;
            }
            default:{
                throw new Error();
            }
        }
    }

    onChangeFilter(value, prop){
        this.state.pageModels.Filter[prop] = value;
        this.setState(this.state);
    }

    onChangeObjectsOnPage(items){
        this.state.pageModels.ObjectsOnPage = items;
        this.setState(this.state);
        this.onLoad();
    }

    async onSelectFolder(id){
        let trackersFolder = this.props.store.folders.list.find(folder => folder.Name == "Image trackers");
        //alert('ee');
        trackersFolder.Id == id ?
                                await this.setState({mode: Mode_Targets}) :
                                await this.setState({mode: Mode_Models});
        this.onChangeFilter(id, 'FolderId');
        this.onLoad();
    }

    defaultFolder(){
        let folder = this.props.store.folders.list.find(folder => folder.Name == "3D models")
        this.onSelectFolder(folder.Id);
    }

    // onDeleteFolder(id){
    //     folderAction.remove(id).then(ok=>{
    //         notification.success('Folder deleted');
    //         folderAction.get()
    //     }, error=>{
    //         notification.error(error);
    //     })
    // }

    onDeleteModel(id){
        modelAction.remove(id).then(ok=>{
            notification.success('Model deleted');
            this.onLoad();
        }, error=>{
            notification.error(error);
        });
    }

    onDeleteTarget(id){
        targetAction.remove(id).then(ok=>{
            notification.success('Model deleted');
            this.onLoad();
        }, error=>{
            notification.error(error);
        });
    }

    handlePageClick(page){
        this.state.pageModels.CurrentPage = page.selected + 1;
        this.setState(this.state);
        this.onLoad();
    }

    async onSortEnd(e){
      let folders = Array.from(this.props.store.folders.list);

      folders = arrayMove(folders, e.oldIndex, e.newIndex);

      folderAction.reIndex(folders);
    }

    onChangeSearch(value){
      this.setState({ search: value }, () => {
        if(this.searchDebounce) clearTimeout(this.searchDebounce);

        this.searchDebounce = setTimeout(() => {
          if(this.state.mode === Mode_Models){
            this.onLoadModels();
          } else if(this.state.mode === Mode_Targets) {
            this.onLoadTargets();
          }
        }, 250);
      })
    }

    showPagination(){
      if(this.state.mode === Mode_Models){
        return this.state.pageModels.TotalPages > 1;
      } else if(this.state.mode === Mode_Targets) {
        return this.state.pageTargets.TotalPages > 1;
      }

      return false;
    }

    render() {

        var options = [5, 10, 20, 30, 40, 50].map((value, index)=>{
            return (<option value={value} key={index}>{value} per page</option>)
        });

        const self = this;
        function folderFilter(folder) {
          /* return folder.Name !== "All models";

          if(self.state.mode === Mode_Models){
            return folder.Name !== "All models";
          } else {
            return folder.Name !== "Unity 3D" && folder.Name !== '3D models';
          } */
          return true;
        }

        /* var folders = this.props.store.folders.list.length > 0 ? this.props.store.folders.list.filter(folderFilter).map(function(folder, index) {
            return (
                <FolderController
                  key={index}
                  folder={folder}
                  active={folder.Id === this.state.pageModels.Filter.FolderId}
                  editable={this.isInRole("CanModelEdit")}
                  onSelectFolder={this.onSelectFolder.bind(this)} />
            );
        }.bind(this)) : null; */

        const SortableItem = SortableElement(({folder, index}) =>
          <FolderController
            key={index}
            folder={folder}
            active={folder.Id === this.state.pageModels.Filter.FolderId}
            editable={this.isInRole("CanModelEdit")}
            onSelectFolder={this.onSelectFolder.bind(this)} />
        );
      
        const SortableList = SortableContainer(({items}) => {
          return (
            <div>
              {items.map((folder, index) => (
                <SortableItem disabled={folder.Type === 'System'} key={`item-${index}`} index={index} folder={folder} />
              ))}

              <div className="mdl-card mdl-card-folder mb-4">
                <div className="input-group form-group testAddFolderBtn">

                  <button hidden={!this.isInRole("CanModelEdit")}
                          disabled={this.state.folder.Name.length < 2}
                          onClick={this.onCreateFolder.bind(this)}
                          type="button">

                      <img src={addFolder} alt="" />
                  </button>

                  <input  hidden={!this.isInRole("CanModelEdit")}
                          type="text"
                          className="form-control"
                          onChange={e=>this.onChangeFolderName(e.target.value, 'Name')}
                          value={this.state.folder.Name}
                          placeholder="Add new folder" />
                </div>
              </div>
            </div>
          );
        });

        var mAction = modelAction;
        var models = this.state.pageModels.Items.length > 0 ?
            this.state.pageModels.Items.map(function(model, index) {
                return (
                    <div className="col-sm-12 col-md-6 col-lg-3 mb-4 mdl-card mdl-card-folder images" key={index}>
                        <div className="imgDiv">
                            <Image isShowLoader={false} src={model.Photo} />
                        </div>

                        <div className="mdl-card__actions">
                            <h6 className="mdl-card__title cardName">{model.Name}</h6>
                            <a className="deleteLink" hidden={!this.isInRole("CanModelEdit")} onClick={function(){this.onDeleteModel(model.Id)}.bind(this)}>
                                <i className="mdi mdi-delete inline"></i>
                            </a>
                            { model.Type === 'model' ? (<img src={formatModel} alt="format" />) : null }
                            { model.Type === 'bundle' ? (<img src={formatAsset} alt="format" />) : null }
                        </div>
                    </div>
                );
            }.bind(this)) : null;

        var targets = this.state.pageTargets.Items.length > 0 ?
            this.state.pageTargets.Items.map(function(target, index) {
                return (
                    <div className="col-sm-12 col-md-6 col-lg-3 mb-4 mdl-card mdl-card-folder images" key={index}>

                        <div className="imgDiv">
                            <Image isShowLoader={false} src={target.File + '&type=Target'} />
                        </div>
                        <div className="mdl-card__actions">
                            <h6 className="mdl-card__title cardName">{target.Name}</h6>
                            <a className="deleteLink" hidden={!this.isInRole("CanModelEdit")} onClick={function(){this.onDeleteTarget(target.Id)}.bind(this)}>
                                <i className="mdi mdi-delete inline"></i>
                            </a>
                            <img src={formatTarget} alt="format" />
                            { /* <p>Model: {target.Model.Name}</p> */ }
                        </div>
                    </div>
                );
            }.bind(this)) : null;

        if (this.state.mode == Mode_Models){
            if (!models && this.state.search === ''){
                models = (
                  <div className="emptyFoldersDiv">
                    { /* <div className="col-sm-12 col-md-6 col-lg-3 mx-auto py-4 mdl-card mdl-card-folder">
                        <p className="mb-5">No models found. Please add assets</p>
                        <NavLink to="/cabinet/upload" className="purple-btn">
                            Upload assets
                        </NavLink>
                    </div> */ }

                    No models found. Please add assets
                  </div>
                );
            }
        }else if(this.state.mode == Mode_Targets){
            if (!targets){
                targets = (
                  <div className="emptyFoldersDiv">
                    <div className="col-sm-12 col-md-6 col-lg-3 mx-auto py-4 mdl-card mdl-card-folder">
                        <p className="mb-5">No targets found. Please add assets</p>
                        <NavLink to="/cabinet/upload" className="purple-btn">
                            Upload assets
                        </NavLink>
                    </div>
                  </div>
                );
            }
        }


        return (
          <section className="uploads">
              <div className="row">
                <div className="col-sm-12">
                  <div className="row ">
                      <div className="col-sm-12 col-md-6 col-lg-3 mb-4">
                          <div className="input-group  form-group">

                              <button hidden={!this.isInRole("CanModelEdit")}
                                      className="btn save-btn user-button"
                                      disabled={this.state.folder.Name.length < 2}
                                      onClick={this.onCreateFolder.bind(this)}
                                      type="button">
                                  <i className="fa fa-plus" aria-hidden="true"></i>
                              </button>

                              <input  hidden={!this.isInRole("CanModelEdit")}
                                      type="text"
                                      className="form-control"
                                      onChange={e=>this.onChangeFolderName(e.target.value, 'Name')}
                                      value={this.state.folder.Name}
                                      placeholder="Add new folder" />
                          </div>
                      </div>
                      <div className="col-sm-12 col-md-6 col-lg-3 mb-4">
                          <div className="input-group form-group">
                              {/*<SwitchButton name="switch-8" label="Switch mode" mode="select"
                                            defaultChecked={this.state.mode == Mode_Models}
                                            onChange={e=>this.onChangeMode(e.target.checked)}
                                            labelRight="Models" label="Targets"/>*/}
                              { /* <div className="switcher">
                                <p className="mr-2 mt-1">Trackers</p>
                                <label onClick={this.onChangeMode.bind(this)} className="switch">
                                  <input type="checkbox" checked={this.state.mode === Mode_Models} readOnly />
                                    <span className="slider round"></span>
                                </label>
                                <p className="ml-2 mt-1">3D Models</p>
                              </div> */ }
                          </div>
                      </div>
                      <div className="col-sm-12 col-md-12 col-lg-6 mb-4">
                          <div className="input-group form-group">
                                  <button className="btn cancel-btn user-button" onClick={this.onLoad.bind(this)} type="button">
                                      <i className="fa fa-search" aria-hidden="true"></i>
                                  </button>
                              <input type="text" className="form-control" placeholder="Search assets" value={this.state.search} onChange={e => this.onChangeSearch(e.target.value)} />
                          </div>
                      </div>
                  </div>
                  <div className="row">
                    <div id="folder" className="col-md-12">

                          { /* folders */ }

                          <SortableList

                            lockToContainerEdges={true}
                            distance={10}
                            axis="xy"
                            items={this.props.store.folders.list.filter(folderFilter)} 
                            onSortEnd={this.onSortEnd.bind(this)} />

                    </div>
                  </div>
                  <div className="row model-list">
                      <div className="col-sm-12 center">
                          <div className="row">

                              {models}

                              {targets}

                          </div>
                      </div>
                  </div>
                  <div className="row paginationRow">
                      <div className="form-group col-sm-2">
                          <select value={this.state.pageModels.ObjectsOnPage} onChange={e=>this.onChangeObjectsOnPage(e.target[e.target.selectedIndex].value)} className="form-control">
                              {options}
                          </select>
                      </div>
                      <div className="form-group col-sm-6">
                        { this.showPagination() ? (
                          <ReactPaginate
                            previousLabel={"Previous"}
                            nextLabel={"Next"}
                            breakLabel={<a href="">...</a>}
                            breakClassName={"page-item"}
                            pageCount={this.state.pageModels.TotalPages}
                            marginPagesDisplayed={3}
                            pageRangeDisplayed={3}
                            initialPage={this.state.pageModels.CurrentPage -1}
                            pageClassName={"page-item"}
                            pageLinkClassName={"page-link"}
                            nextClassName={"page-link"}
                            previousClassName={"page-link"}
                            onPageChange={this.handlePageClick.bind(this)}
                            containerClassName={"pagination"}
                            subContainerClassName={"pages pagination"}
                            activeClassName={"active"}
                          />
                        ) : null }
                      </div>
                  </div>
              </div>
          </div>
        </section>

        );
    }
}

export default connect(store => ({ store }))(Library);
