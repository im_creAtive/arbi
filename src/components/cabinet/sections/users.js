import React, { Component } from 'react';
import {connect} from 'react-redux';
import {NotificationManager} from 'react-notifications';
import SwitchButton from 'react-switch-button';

import * as userAction from './../../../actions/userAction';
//import * as roleAction from './../../../actions/roleAction';

import 'react-switch-button/dist/react-switch-button.css';
import "../../../sources/css/admin/app.css";
import "../../../sources/css/admin/users.css";


class Users extends Component {
    constructor(props){
        super(props);

        this.state = {
            user: {
                RoleIds: []
            },
            roles: [
                {
                    title: "User can only view assets on desktop and mobile app.",
                    value: "CanModelView",
                },
                {
                  title: "User is allowed to upload & manage assets.",
                  value: "CanModelEdit",
                },
                {
                    title: "User can manage & add users under the allocated plan.",
                    value: "CanUserEdit",
                }
            ],
            searchUser: ''
        };
    }

    componentDidMount(){
        //userAction.get();
    }

    async onDeleteUser(id){
      try{
        let user = await userAction.getById(id);
        user.IsEnable = false;

        await userAction.modify(id, user);
        await userAction.get();

        NotificationManager.success('User deleted');
        window.$('.deleteDialog').hide();
      } catch(e) {
        console.log(e);
        NotificationManager.error('Error');
      }
    }

    onLoadSelectedUser(id) {
        userAction.getById(id).then(user=>{
            this.setState({user});
        }, error=>{
            NotificationManager.error('Error');
        })
    }

    onSaveSelectedUser(){
        userAction.modify(this.state.user.Id, this.state.user).then(res=>{
            NotificationManager.success('User saved');
            userAction.get();
        }, error=>{
            NotificationManager.error('Error');
        });
    }

    onChangeStatus(isActive, userId){
        userAction.modifyStatus(userId, isActive).then(async (ok) =>{
            NotificationManager.success('User status updated');
            await userAction.get();
            console.log('this users', this.props.store.users.list);
        }, error=> {
            NotificationManager.error('Error');
        });
    }

    onChangeUserInfo (e, prop){
        this.state.user[prop] = e.target.value;
        this.setState(this.state);
    }

    isInRole(roleName){
        return this.props.store.roles.list.find(x=>x.Name == roleName) ? true : false;
    }

    onChangeRole(value){
      console.log(value)

      let user = Object.assign(this.state.user);
      let index = user.RoleIds.indexOf(value);

      if(index > -1){
        user.RoleIds.splice(index, 1);
      } else {
        user.RoleIds.push(value);
      }

      this.setState({ user });
    }

    changeDateFormat(lastLogged) {
        if (typeof(lastLogged) == 'string') {
            return lastLogged.replace("T", " ");
        } 
    }

    onSearchUser(e){
        this.setState({ searchUser: e.target.value })
    }

    onClickDelete(userId){
        document.getElementById('delete-dialog-' + userId).style.display="block";
    }

    onClickDeleteHide(userId){
        document.getElementById('delete-dialog-' + userId).style.display="none";
    }

    onShowForm(){
      if(!this.isInRole("CanUserEdit")){
        return NotificationManager.error('Access denied');
      }
      window.$('#modalAddUser').modal('show');
    }

    render() {
        const roles = this.state.roles.length > 0 ? this.state.roles.map((role, index) => {
          let isChecked = this.state.user.RoleIds.find(x=>x == role.value) ? true : false;
            
          return (
            <div className="form-check" key={index}>
              <label className="form-check-label">
                <input className="form-check-input mr-2" type="checkbox"
                  checked={isChecked}
                  onChange={this.onChangeRole.bind(this, role.value)}
                  aria-label="Checkbox for following text input" />
                  {role.title}
              </label>
            </div>
          );
        }) : null;

        var userList = this.state.searchUser.length > 0 ? this.props.store.users.list.filter(user => {
            if (user.FirstName.indexOf(this.state.searchUser) > -1 ||
                user.LastName.indexOf(this.state.searchUser) > -1) {
                    return user;
                    // return true; 
            }
        }) : this.props.store.users.list;

        var users = userList.map(function(user, index) {
            var isEdited = this.props.store.user.data.Id != user.Id;
            return(
                <tr key={index}>
                    <td className="text-center">{user.FirstName} {user.LastName}</td>
                    <td className="text-center">{this.changeDateFormat(user.LastLogged)}</td>
                    <td>54 SCANS</td>
                    <td className="text-center">
                        <button hidden={!isEdited} type="button" className="button-edit"
                                onClick={()=>{this.onLoadSelectedUser(user.Id)}}
                                data-toggle="modal" data-target="#modalEditUser"
                                data-upgraded="MaterialButton">
                            <i className="mdi mdi-edit"></i>
                        </button>
                    </td>
                    <td className="text-center">
                     {/*{isEdited ? <SwitchButton name={"user-" + index} onChange={e=>this.onChangeStatus(e.target.checked, user.Id)} key={index} label="Switch mode" mode="select" defaultChecked={user.IsActive} labelRight="ON" label="OFF"/> : ""}*/}
                        {isEdited ? 
                                    (<div className="switcher">
                                      <p className="mr-2 mt-1">OFF</p>
                                      <label className="switch">
                                      <input name={"user-" + index}
                                             onChange={e=>this.onChangeStatus(e.target.checked, user.Id)}
                                             key={index}
                                             mode="select" 
                                             defaultChecked={user.IsActive}
                                             // defaultChecked="false"
                                             type="checkbox" />
                                      <span className="slider round"></span>
                                      </label>
                                      <p className="ml-2 mt-1">ON</p>
                                    </div>)
                                  : ""}
                    </td>
                    <td className="text-center">
                        <button hidden={!isEdited} className="button-edit" 
                                data-toggle="modal"
                                data-target="#modalConfirmDeleteUser" 
                                data-upgraded="MaterialButton"
                                onClick={this.onClickDelete.bind(this, user.Id)}>
                            <i className="mdi mdi-delete"></i>
                        </button>
                        <div className="deleteDialog py-3" id={"delete-dialog-" + user.Id}>
                            <p>Are you sure?</p>
                                <button type="button" className="btn save-btn modal-btn" 
                                        onClick={this.onDeleteUser.bind(this, user.Id)}>
                                    Yes
                                </button>
                                <button type="button" className="btn cancel-btn modal-btn"
                                        onClick={this.onClickDeleteHide.bind(this, user.Id)}>
                                    No
                                </button>
                        </div>
                    </td>
                </tr>
            );
        }.bind(this));

        var editModelUser =  this.state.user ? (
                <div key="modalEditUser" className="modal fade" id="modalEditUser" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h2 className="modal-title" id="exampleModalLabel">User Info</h2>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>FIRST NAME</label>
                                        <input type="text" value={this.state.user.FirstName}
                                                onChange={e=>this.onChangeUserInfo(e,"FirstName")}
                                                name="first-name" className="form-control" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>LAST NAME</label>
                                        <input type="text" value={this.state.user.LastName}
                                                onChange={e=>this.onChangeUserInfo(e,"LastName")}
                                                name="last-name"  className="form-control" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>EMAIL</label>
                                        <input type="email" value={this.state.user.Email} name="email"
                                                disabled className="form-control" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>СHANGE PASSWORD</label>
                                        <input type="password" name="password" className="form-control" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>CONFIRM PASSWORD</label>
                                        <input type="password" name="re-password" className="form-control" />
                                    </div>
                                </div>
                                {roles}
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn save-btn modal-btn" data-dismiss="modal"
                                        onClick={this.onSaveSelectedUser.bind(this)} >
                                    Save
                                </button>
                                <button type="button" className="btn cancel-btn modal-btn" data-dismiss="modal">
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            ) : null;

        return (
            <section className="uploads">
                <div className="row">
                    <div className="col-md-3 col-sm-3 col-xs-12 mb-4">
                        <div className="input-group form-group" onClick={this.onShowForm.bind(this)}>
                            <button className="btn save-btn user-button" type="button">
                                <i className="fa fa-plus" aria-hidden="true"></i>
                            </button>

                            <input  style={{ cursor: 'pointer' }} type="text" className="form-control" disabled placeholder="Add user" />
                        </div>
                    </div>
                    <div className="col-md-9 col-sm-9 col-xs-12 mb-4">
                        <div className="input-group form-group">                   
                            <button className="btn cancel-btn user-button" type="button">
                                <i className="fa fa-search" aria-hidden="true"></i>
                            </button>
                            <input type="text" className="form-control" 
                                    // onChange={e => this.onSearchUser(e.target.value)}
                                    onChange={this.onSearchUser.bind(this)}
                                    value={this.state.searchUser}
                                    placeholder="Search users" />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="card-table">
                            <div className="pa-5" id="table-card">
                                <table className="table table-responsive mb-0">
                                    <thead>
                                        <tr>
                                            <th>NAME</th>
                                            <th>LAST LOGGED IN</th>
                                            <th>SCANS USED</th>
                                            <th className="text-center">EDIT</th>
                                            <th className="text-center">ACTIVE/INACTIVE </th>
                                            <th className="text-center">DELETE USER</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {users}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                {editModelUser}
            </section>
        );
    }
}

export default connect(store => ({ store }))(Users);
