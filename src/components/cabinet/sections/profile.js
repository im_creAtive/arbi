import React, { Component } from 'react';
import {connect} from 'react-redux';
import '../../../sources/css/admin/profile.css';
import {NotificationManager} from 'react-notifications';
import * as userAction from './../../../actions/userAction';
import * as authorizationAction from '../../../actions/authorizationAction'

class Profile extends Component {

    constructor(props){
        super(props);
        this.state = {
            user: {}
        };
    }

    componentDidMount(){
        userAction.getCurrent().then(user => {
            this.state.user = user;
            this.setState(this.state.user);   
        }, Error => {
            NotificationManager.error('Error');
        });
    }

    onChangeInfo(value, prop){
      console.log(value, prop)
        let user = Object.assign({}, this.state.user);

        user[prop] = value;

        this.setState({ user });
    }

    onSave(){
        let user = Object.assign({}, this.state.user);

        delete user.currentPlan;

        userAction.modifyCurrent(user).then(ok=>{
            NotificationManager.success('Saved');

            window.$('#profileModalWindow').modal('hide');

            // logout if password is changed
            if(user.Password !== undefined && user.Password !== ''){
              NotificationManager.success('Password is changed. Please sign in.');
              authorizationAction.logout();
            }
        }, error => {
            NotificationManager.error('Error');
        })
    }

    render() {
        return (
            <div className="modal-dialog" role="document" id="reg-user">
                <div className="modal-content">
                    <div className="modal-header">
                        <h2 className="modal-title" id="exampleModalLabel">User Info</h2>
                    </div>
                    <div className="modal-body">
                        <div className="form-group">
                            <div className="col-sm-12 mdl-input p-0">
                                <label>FIRST NAME</label>
                                <input type="text" value={this.state.user.FirstName} onChange={e=>this.onChangeInfo(e.target.value, 'FirstName')} name="first-name" className="form-control" />
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-sm-12 mdl-input p-0">
                                <label>LAST NAME</label>
                                <input type="text" value={this.state.user.LastName} onChange={e=>this.onChangeInfo(e.target.value, 'LastName')} name="last-name"  className="form-control" />
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-sm-12 mdl-input p-0">
                                <label>EMAIL</label>
                                <input type="email" name="email" value={this.state.user.Email} disabled className="form-control" />
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-sm-12 mdl-input p-0">
                                <label>PASSWORD</label>
                                <input type="password" name="password" value={this.state.user.Password} onChange={e=>this.onChangeInfo(e.target.value, 'Password')} className="form-control" />
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-sm-12 mdl-input p-0">
                                <label>CONFIRM PASSWORD</label>
                                <input type="password" name="re-password"  value={this.state.user.PasswordConfirm} onChange={e=>this.onChangeInfo(e.target.value, 'PasswordConfirm')} className="form-control" />
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary save-btn" onClick={this.onSave.bind(this)}>Save</button>
                        <button type="button" className="btn btn-primary cancel-btn" onClick={this.componentDidMount.bind(this)} data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(store => ({ store }))(Profile);