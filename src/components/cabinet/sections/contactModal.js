import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as messageActions from '../../../actions/messageAction'
import cabinetContact from '../../public/mails/cabinetContact'
import { NotificationManager } from 'react-notifications'

class ContactUs extends Component{

  state = {
    fullName: '',
    email: '',
    phone: '',
    message: ''
  }

  onShow(){
    if(this.props.user.data) this.setState({
      fullName: `${this.props.user.data.FirstName} ${this.props.user.data.LastName}`,
      email: this.props.user.data.Email,
      phone: this.props.user.data.PhoneNumber,
      message: ''
    })

    window.$('#modalContactUs').modal('show');
  }

  onHide(){
    window.$('#modalContactUs').modal('hide');
  }

  async onSend(){
    try{
      let body = cabinetContact(
        this.state.fullName,
        this.state.email,
        this.state.phone,
        this.state.message
      );

      await messageActions.admin(body);
      NotificationManager.success('Your message sent. We will get back to you as soon as possible.');
    } catch(e) {
      NotificationManager.error(e.message || e || 'Unknown error');
    }

    window.$('#modalContactUs').modal('hide');
  }

  render(){
    return (
      <div className="mdl-card__actions button">
        <button type="button" className="purple-btn" onClick={this.onShow.bind(this)}>
          CONTACT US
        </button>

        <div 
          key="modalContactUs" 
          className="modal fade" 
          id="modalContactUs" 
          tabIndex="-1" 
          role="dialog"
          aria-labelledby="exampleModalLabel" 
          aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h2 className="modal-title" id="exampleModalLabel">Contact us</h2>
                    </div>
                    <div className="modal-body">
                      <div className="form-group">
                        <label>FULL NAME</label>
                        <input 
                          type="text" 
                          className="form-control"
                          value={this.state.fullName}
                          onChange={e => this.setState({ fullName: e.target.value })} />
                      </div>

                      <div className="form-group">
                        <label>EMAIL</label>
                        <input 
                          type="text" 
                          className="form-control"
                          value={this.state.email}
                          onChange={e => this.setState({ email: e.target.value })} />
                      </div>

                      <div className="form-group">
                        <label>PHONE</label>
                        <input 
                          type="text" 
                          className="form-control"
                          value={this.state.phone}
                          onChange={e => this.setState({ phone: e.target.value })} />
                      </div>

                      <div className="form-group">
                        <label>MESSAGE</label>
                        <textarea 
                          className="form-control"
                          value={this.state.message}
                          onChange={e => this.setState({ message: e.target.value })} />
                      </div>

                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn save-btn modal-btn"
                                onClick={this.onSend.bind(this)}>
                            Send
                        </button>
                        <button type="button" data-dismiss="modal" className="btn cancel-btn modal-btn"
                                onClick={this.onHide.bind(this)}>
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
      </div>
    )
  }

}

export default connect(state => ({ user: state.user }))(ContactUs);