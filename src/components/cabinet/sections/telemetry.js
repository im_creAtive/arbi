import React, { Component } from 'react';
import {NotificationManager} from 'react-notifications';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';

import "../../../sources/css/admin/app.css";
import "../../../sources/css/admin/users.css";

import * as userAction from './../../../actions/userAction';
import * as roleAction from './../../../actions/roleAction';
import * as paymentPlanAction from './../../../actions/paymentPlanAction';
import * as accountAction from './../../../actions/accountAction';

import done from '../../../sources/img/done.png';
import confirm from '../../../sources/img/confirm.png';
import user from '../../../sources/img/user.png';
import line from '../../../sources/img/line.jpg';
import done_green from '../../../sources/img/done_green.png';

import UpgradePlanModal from './upgradePlanModal';
import ContactUsModal from './contactModal';

import email1 from '../../public/mails/email1';
import * as messageAction from '../../../actions/messageAction';

function getDefaultState() {
  return {
    user: {
      FirstName: "",
      LastName: "",
      Email: "",
      Password: "",
      PasswordConfirm: "",
      RoleIds: [],
      IsActive: true,
      IsEnable: true
    }
  }
}

class Telemetry extends Component {
    constructor(props){
      super(props);
      this.state = getDefaultState();
    }

    isInRole(roleName){
      return this.props.store.roles.list.find(x=>x.Name == roleName) ? true : false;
    }

    componentDidMount(){
      roleAction.get();
      userAction.get();
    }

    onChange(e, prop){
      this.state.user[prop] = e.target.value;
      this.setState(this.state);
    };

    onChangeRole(value){
      console.log('onChangeRole', value);

      let user = Object.assign({}, this.state.user);
      let index = user.RoleIds.indexOf(value);

      if(index > -1){
        user.RoleIds.splice(index, 1);
      } else {
        user.RoleIds.push(value);
      }

      this.setState({ user })
    }

    onSave(){
      let history = this.props.history;

      if(this.state.user.Password.length < 6){
        return NotificationManager.error('Password should be at least 6 characters');
      }

      if(this.state.user.Password !== this.state.user.PasswordConfirm){
        return NotificationManager.error('Password and password confirm is invaild.');
      }

      userAction.add(this.state.user).then(res=>{
        NotificationManager.success('User created');
        userAction.get();
        this.setState(getDefaultState());

        messageAction.customer(this.state.Email, email1(this.state.FirstName));

        window.$('#modalAddUser').modal('hide');
        history.push("/cabinet/users");
      }, error=>{
        NotificationManager.error(error.message || error, 'Error');
      });
    }

    onCancel(){
      this.setState(getDefaultState());
    }

    upgradePlanModal() {

    }

    checkout(){

    }

    checkoutSuccess(){
      document.getElementsByClassName('checkoutBody')[0].style.display="none";
      document.getElementsByClassName('checkoutSuccess')[0].style.display="block";
    }

    contactModal(){
      document.getElementsByClassName('plans')[0].style.display="none";
      document.getElementsByClassName('contactModal')[0].style.display="block";
    }

    changeRoleName(name){
      switch(name){
        case 'CanModelEdit':
          return 'User is allowed to upload & manage assets.'
        case 'CanModelView':
          return 'User can only view assets on desktop and mobile app.'
        case 'CanUserEdit':
          return 'User can manage & add users under the allocated plan.'
        default:
          return name
      }
    }

    render() {
        var roles = this.props.store.roles.list.length > 0 ?
          this.props.store.roles.list.map(function (role, index) {
              return (
                <div className="form-check" key={index}>
                  <label className="form-check-label">
                    <input className="form-check-input mr-2" type="checkbox"
                      checked={this.state.user.RoleIds.indexOf(role.Name) > -1}
                      onChange={this.onChangeRole.bind(this, role.Name)} />
                      {this.changeRoleName(role.Name)}
                  </label>
                </div>
              );
          }.bind(this)) : null;

        var modalAddUser =  (
                <div key="modalAddUser" className="modal fade" id="modalAddUser" tabIndex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h2 className="modal-title" id="exampleModalLabel">User Info</h2>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>FIRST NAME</label>
                                        <input type="text" name="first-name" className="form-control"
                                            value={this.state.user.FirstName}
                                            onChange={(e)=>this.onChange(e, 'FirstName')}
                                        />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>LAST NAME</label>
                                        <input type="text" name="last-name"  className="form-control"
                                            value={this.state.user.LastName}
                                            onChange={(e)=>this.onChange(e, 'LastName')}
                                        />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>EMAIL</label>
                                        <input type="email" name="email" className="form-control"
                                            value={this.state.user.Email}
                                            onChange={(e)=>this.onChange(e, 'Email')}
                                        />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>PASSWORD</label>
                                        <input type="password" name="password" className="form-control"
                                            value={this.state.user.Password}
                                            onChange={(e)=>this.onChange(e, 'Password')}/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>CONFIRM PASSWORD</label>
                                        <input type="password" name="re-password" className="form-control"
                                            value={this.state.user.PasswordConfirm}
                                            onChange={(e)=>this.onChange(e, 'PasswordConfirm')}/>
                                    </div>
                                </div>
                                {roles}
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn save-btn modal-btn"
                                        onClick={this.onSave.bind(this)}>
                                    Save
                                </button>
                                <button type="button" data-dismiss="modal" className="btn cancel-btn modal-btn"
                                        onClick={this.onCancel.bind(this)}>
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            )

        return (
            <div className="row telemetryRow">
                <div className="col-lg-3 col-md-6 col-sm-6 col-12 mb-4">
                    <div className="mdl-card mdl-card-admin">
                        <div className="mdl-card__title">
                            <h2 className="mdl-card__title-text">Scans</h2>
                        </div>
                        <div className="mdl-card__supporting-text green">
                            <p style={{ padding: '40px 0' }}>
                              0 scans of 200<br />
                              left
                            </p>
                        </div>
                        <div className="mdl-card__actions">
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-md-6 col-sm-6 col-12 mb-4">
                    <div className="mdl-card mdl-card-admin">
                        <div className="mdl-card__title">
                            <h2 className="mdl-card__title-text">Storage</h2>
                        </div>
                        <div className="mdl-card__supporting-text green">
                            <p style={{ padding: '40px 0' }}>
                              0 MB of 1000 MB<br />
                              left  
                            </p>
                        </div>
                        <div className="mdl-card__actions">
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-md-6 col-sm-6 col-12 mb-4">
                    <div className="mdl-card mdl-card-admin">
                        <div className="mdl-card__title">
                            <h2 className="mdl-card__title-text">Users</h2>
                        </div>
                        <div className="mdl-card__supporting-text green">
                            <p>{this.props.store.users.list.length} users active</p>

                        </div>
                        <div className="mdl-card__actions button">
                            <button hidden={!this.isInRole("CanUserEdit")} type="button"
                                    className="green-btn"
                                    data-toggle="modal" data-target="#modalAddUser">
                                ADD USER
                            </button>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 col-md-6 col-sm-6 col-12 mb-4">
                    <div className="mdl-card mdl-card-admin">
                        <div className="mdl-card__title">
                            <h2 className="mdl-card__title-text">Current Plan</h2>
                        </div>
                        <div className="mdl-card__supporting-text">
                            <p>{ this.props.store.user.data.currentPlan ? this.props.store.user.data.currentPlan.Name : '-' }</p>
                            { /* <p>Valid until 24 June 2017</p> */ }
                        </div>
                        { /* <UpgradePlanModal /> */ }

                        <ContactUsModal />
                    </div>
                </div>
                {modalAddUser}
            </div>
        );
    }
}
export default withRouter(connect(store => ({ store }))(Telemetry));
