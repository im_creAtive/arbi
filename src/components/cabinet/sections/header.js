import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link, NavLink } from 'react-router-dom';
import nouser from '../../../sources/img/nouser.jpg';
import "../../../sources/css/admin/app.css";

import * as authorizationAction from './../../../actions/authorizationAction';
import * as userAction from './../../../actions/userAction';
import * as paymentAction from './../../../actions/paymentAction';

import * as notification from './../../../helpers/notificationHelper';
import { NotificationManager } from 'react-notifications';

const paymentTypes = [
  'ideal',
  'creditcard',
  'mistercash',
  'sofort',
  'banktransfer',
  'directdebit',
  'belfius',
  'kbc',
  'bitcoin',
  'paypal',
  'paysafecard'
];

class Header extends Component {

    constructor(props){
        super(props);

        this.state = {
          Amount: 199,
          PaymentSystem: "creditcard"
        }
    }

    componentDidMount(){
        userAction.getCurrent();
        paymentAction.get();
    }

    onLogout(){
        authorizationAction.logout();
    }

    isInRole(role){
        return this.props.store.user.data.RoleIds ? this.props.store.user.data.RoleIds.find(x=>x == role) != null : false;
    }

    async onAddMoney(){
      let payment = {
        Amount: Number(this.state.Amount),
        PaymentSystem: this.state.PaymentSystem,
        RedirectTo:  window.location.href
      }

      try{
        let data = await paymentAction.add(payment);
        notification.success('Redirecting...');
        
        window.location.href = data;
      } catch(e) {
        notification.error(e);
      }
    }

    render() {
        let userName = (
            this.props.store.user.data.FirstName + ' ' + this.props.store.user.data.LastName
        );

        let payments = this.props.store.payments.bundle.History.length ? this.props.store.payments.bundle.History.map((payment, index) => {
  			return (
            <a key={index} className="dropdown-item payment-item">
              <span className="info">
                {payment.PaymentType == 'AddMoney' ? 'Add money' : 'Get money'}<br />
                <small>{payment.CreateDate.replace('T', ' ')}</small>
              </span>
              <span className="amount">€ {payment.Amount}</span>
            </a>
          );
  		  }) : null;

        let paymentTypesOptions = paymentTypes.map((type, index) => {
          return (
            <option key={index} value={type}>{type}</option>
          )
        });

        return (
            <header id="admin-header">
                   <nav className="navbar navbar-expand-lg navbar-light bg-faded">
                           <div className="col">
                               <button className="navbar-toggler" type="button" data-toggle="collapse"
                                      data-target="#navbarSupportedContent2" aria-controls="navbarSupportedContent2"
                                      aria-expanded="false" aria-label="Toggle navigation">
                                   <span className="navbar-toggler-icon"></span>
                               </button>
                           </div>

                                                     {/* Mobile Nav */}

                            <div className="collapse navbar-collapse" id="navbarSupportedContent2">
                                <ul className="navbar-nav mr-auto">
                                  <li className="nav-item active">
                                    <NavLink hidden={!this.isInRole("CanModelEdit")} to="/cabinet/upload"
                                             activeClassName="is-active" className="navigation-link">
                                        <i className="mdi-group_add"></i>
                                        <p>UPLOAD</p>
                                    </NavLink>
                                  </li>
                                  <li className="nav-item">
                                    <NavLink hidden={!this.isInRole("CanModelView")} to="/cabinet/library"
                                             activeClassName="is-active" className="navigation-link" >
                                        <i className="mdi-grid_on"></i>
                                        <p>LIBRARY</p>
                                    </NavLink>
                                  </li>
                                  <li className="nav-item dropdown">
                                    <NavLink hidden={!this.isInRole("CanUserEdit")} to="/cabinet/users"
                                             activeClassName="is-active" className="navigation-link">
                                        <i className="fa fa-cog" aria-hidden="true"></i>
                                        <p>USERS</p>
                                    </NavLink>
                                  </li>
                                  <li className="nav-item">
                                    <NavLink to="/" data-toggle="modal" data-target="#profileModalWindow"
                                             activeClassName="active" className="navigation-link">
                                        <i className="fa fa-search-plus" aria-hidden="true"></i>
                                        <p>PROFILE</p>
                                    </NavLink>
                                  </li>
                                  <li className="nav-item">
                                    <NavLink  to="/login" className="navigation-link" onClick={this.onLogout.bind(this)}
                                              activeClassName="is-active" >
                                        <i className="fa fa-power-off"></i>
                                        <p>LOGOUT</p>
                                    </NavLink>
                                  </li>

                                  <li className="text-center mt-3">
                                    <div className="dropdown">
                                       <a className="drop btn btn-secondary dropdown-toggle"  id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                           <img className="inline user-logo"  src={nouser} alt="" />John Doe
                                       </a>
                                       <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                           <a className="dropdown-item" href="#">Profile</a>
                                           <a className="dropdown-item" href="#">Logout</a>
                                       </div>
                                    </div>
                                  </li>
                                </ul>
                              </div>

                           <div className="col"  id="profileCol">
                               <div className="float-right">
                                   <div className="dropdown">
                                       <a className="drop btn btn-secondary dropdown-toggle"  id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                           <img className="inline user-logo"  src={nouser} alt="" />{userName}
                                       </a>
                                       <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                           <NavLink  to="/" className="dropdown-item" data-toggle="modal" data-target="#profileModalWindow">
                                              Profile
                                           </NavLink>
                                           <a className="dropdown-item" onClick={this.onLogout.bind(this)} href="/login">Logout</a>
                                       </div>
                                   </div>
                               </div>

                              { /* <div className="float-right">
                                   <div className="dropdown">
                                       <a style={{ padding: '13px' }} className="drop btn btn-secondary dropdown-toggle"  id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                           Balance: {this.props.store.payments.bundle.Balance.Total - this.props.store.payments.bundle.Balance.Used}
                                       </a>
                                       <div className="dropdown-menu payment-list" aria-labelledby="dropdownMenuLink">
                                          --- data-toggle="modal" data-target="#addMoneyModal" ---
                                          <a
                                            className="dropdown-item"
                                            onClick={e => { e.preventDefault(); NotificationManager.info('This section is temporarily unavailable.') }} href="/">Add money</a>
                                           { payments !== null ? (<div className="dropdown-divider"></div>) : null }
                                           {payments}
                                       </div>
                                   </div>
                              </div> */ }

                           </div>
                   </nav>

                   <div className="modal fade" id="addMoneyModal" tabIndex="-1" role="dialog"aria-hidden="true">
                     <div className="modal-dialog" role="document">
                       <div className="modal-content">
                         <div className="modal-header">
                           <h5 className="modal-title">Add money</h5>
                           <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                           </button>
                         </div>
                         <div className="modal-body">
                           <div className="form-group">
                             <form>
                               <div className="form-group row">
                                 <label className="col-sm-4 col-form-label">Amount</label>
                                 <div className="col-sm-8">
                                  <div className="input-group">
                                    <input
                                     type="text"
                                     className="form-control"
                                     value={this.state.Amount}
                                     onChange={e => this.setState({ Amount: e.target.value })} />
                                     <div className="input-group-addon">€</div>
                                  </div>
                                 </div>
                               </div>
                               <div className="form-group row">
                                 <label className="col-sm-4 col-form-label">Type</label>
                                 <div className="col-sm-8">
                                   <select
                                    className="form-control"
                                    defaultValue="creditcard"
                                    value={this.state.PaymentSystem}
                                    onChange={e => this.setState({ PaymentSystem: e.target.value })} >
                                      { paymentTypesOptions }
                                   </select>
                                 </div>
                               </div>
                             </form>
                           </div>
                         </div>
                         <div className="modal-footer">
                           <button type="button" className="btn save-btn modal-btn" onClick={this.onAddMoney.bind(this)}>Continue</button>
                           <button type="button" className="btn cancel-btn modal-btn" data-dismiss="modal">Cancel</button>
                         </div>
                       </div>
                     </div>
                   </div>
            </header>

        );
    }
}
export default connect(store => ({ store }))(Header);
