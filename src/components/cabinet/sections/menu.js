import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link, NavLink } from 'react-router-dom';

import * as authorizationAction from './../../../actions/authorizationAction';

import Profile from './profile';

import '../../../sources/css/admin/menu.css';

import logo from '../../../sources/img/arbi_logo_white.png'

class Menu extends Component {

    onLogout(){
        authorizationAction.logout();
    }

    isInRole(role){
        return this.props.store.user.data.RoleIds ? this.props.store.user.data.RoleIds.find(x=>x == role) != null : false;
    }

    render() {
        return (
            <div className="left-menu">
                <span className="title1">
                    <img src={logo} />
                </span>
                <nav className="navigation">
                    <NavLink hidden={!this.isInRole("CanModelEdit")} to="/cabinet/upload" href="#"  activeClassName="is-active" className="navigation-link">
                        <i className="mdi-cloud_upload"></i>
                        <p>UPLOAD</p>
                    </NavLink>
                    <NavLink hidden={!this.isInRole("CanModelView")} to="/cabinet/library" activeClassName="is-active" className="navigation-link" >
                        <i className="mdi-grid_on"></i>
                        <p>LIBRARY</p>
                    </NavLink>
                    <NavLink hidden={!this.isInRole("CanUserEdit")} to="/cabinet/users" activeClassName="is-active" className="navigation-link">
                        <i className="mdi-group_add"></i>
                        <p>USERS</p>
                    </NavLink>
                    <NavLink  to="/" activeClassName="active" data-toggle="modal" data-target="#profileModalWindow"  className="navigation-link">
                        <i className="mdi-person"></i>
                        <p>PROFILE</p>
                    </NavLink>
                    <NavLink  to="/login" className="navigation-link"  onClick={this.onLogout.bind(this)} activeClassName="is-active" >
                        <i className="fa fa-power-off"></i>
                        <p>LOGOUT</p>
                    </NavLink>
                </nav>
                <div className="modal fade" id="profileModalWindow" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <Profile />
                </div>
            </div>


        );
    }
}
export default connect(store => ({ store }))(Menu);
