import React, { Component } from 'react';
import { connect } from 'react-redux';
import {NotificationManager} from 'react-notifications';
import Select from 'react-select';
import FileInputThumbnail from '../../public/finput';

import * as notification from './../../../helpers/notificationHelper';

import * as folderAction from './../../../actions/folderAction';
import * as modelAction from './../../../actions/modelAction';
import * as targetAction from './../../../actions/targetAction';

import '../../../sources/css/admin/upload-file.css';

var FileDragAndDrop = require('react-file-drag-and-drop');
var FileInput = require('react-file-input');


class Upload extends Component {

    constructor(props){
        super(props);
        this.state = {
            model: {
                Name: "",
                FolderId: null
            },
            target: {
                Name: "",
                FolderId: null,
                Id: null,
            },
            files: [],
            photo: null,
            models: [],
            targets: []
        }
    }

    async componentDidMount(){
        await Promise.all([
          folderAction.get(),
          this.loadModels(),
          this.loadTargets()
        ]);

        this.onChangeTab('3D models');
    }

    loadModels (){
        modelAction.get().then(models => {
            this.setState({ models })
        }, error => {
            notification.error(error);
        });
    }

    loadTargets (){
        targetAction.filter({
          Filter: {
            FolderId: null,
            Name: '',
          },
          ObjectsOnPage: 10000,
          CurrentPage: 1
        }).then(targets => {
            this.setState({ targets: targets.Items })
        }, error => {
            notification.error(error);
        });
    }

    onCreateModel(type = ''){
        let photo = this.state.photo;
        let files = this.state.files;
        let model = Object.assign({}, this.state.model);
        model.Type = type;

        if(model.Name === "") return notification.error('Model name is required fild');
        if(!photo) return notification.error('Thumbnail is required file');
        

        modelAction.add(model).then(model=>{
            this.loadModels();
            // notification.success('Created');
            /*modelAction.uploadPhoto(model.Id, photo).then(ok=>{
                // notification.success('Photo Uploaded');
            }, error => {
                notification.error(error);
            });
            modelAction.uploadFiles(model.Id, files).then(ok=>{
                // notification.success('Files Uploaded');
                notification.success('Upload successful');
            }, error => {
                notification.error(error);
            }); */

            Promise.all([
              modelAction.uploadFiles(model.Id, files),
              modelAction.uploadPhoto(model.Id, photo)
            ]).then(() => {
              notification.success('Upload successful');
              this.onClearInput();
            }).catch(e => {
              notification.error(e.message || e);
            })
        }, error=> {
            notification.error(error);
        })
    }

    onCreateTarget(){
        targetAction.add(this.state.target).then(ok=>{
            notification.success('Target created');
            targetAction.uploadFile(ok.Id,this.state.photo).then(ok=>{
                notification.success('Upload successful');
                this.onClearInput();
            }, error=>{
                console.log('HERE');
                notification.error(error);
            })
        }, error=>{
            notification.error(error);
        });
    }

    async onTargetChange (value, prop){
        this.state.target[prop] = value;
		await this.setState(this.state);
        console.log('onTargetChange', this.state);
    }

    onModelChange(value, prop){
        this.state.model[prop] = value;
		this.setState(this.state);
    }

    onSelectPhoto(value, type = null){
        if(value.target.files.length < 1) return;

        let photo = value.target.files[0];

        const wList = ["jpg", "png", "JPG", "PNG", "jpeg", "JPEG"];
        
        let parts = photo.name.split('.');
        if(wList.indexOf(parts[parts.length - 1]) < 0){
          return notification.error('Only these file extensions are allowed: ' + wList.join(', '));
        }

        const self = this;
        const reader = new FileReader();
        reader.onload = (e) => {
            photo.uri =  e.target.result;
            self.setState({ photo });
        };
        reader.readAsDataURL(photo);
    }

    onSelectFile(value, type){
        if(value.target.files.length === 0) return;
        
        var file = value.target.files[0];
        file.target = type;



        this.addFile(file);
    }

    async onSelectTarget(value){
        this.state.photo = value.target.files[0];
        await this.setState(this.state);
        console.log('onSelectTarget', this.state);
        // notification.success('Your select new target');
    }

    handleDrop (dataTransfer) {
        var files = dataTransfer.files;
        for (var i =0;i< files.length;i++){
            this.addFile(files[i]);
        }
    }

    addFile(file){
        if(!file) return;

        if(!file.target){
          let wList = ["zip", "obj", "fbx"];
          let parts = file.name.split('.');

          console.log(wList)

          if(wList.indexOf(parts[parts.length - 1]) < 0){
            if(file.target && file.target === "Android") this.refs.fileName1.value = null;
            if(file.target && file.target === "iOS") this.refs.fileName2.value = null;
            if(file.target && file.target === "Standalone") this.refs.fileName3.value = null;
  
            return notification.error('Only these file extensions are allowed: ' + wList.join(', '));
          }
        }

        if (!this.state.files.find(x=>x.name == file.name && x.target == file.target)){
            this.setState({files: [...this.state.files, file]});
        }
    }

    delFile(index){
        let files = Object.assign(this.state.files);
        files.splice(index, 1);
        this.setState({ files });
    }

    handleChange(event) {
        if(event.target.files.length === 0) return;
        console.log('Selected file:', event.target.files[0]);
        this.addFile(event.target.files[0]);
        //this.setState({files: [...this.state.files, event.target.files[0]]});
    }

    onClearInput(){
        //window.$('input[type=file]').value(null);
        this.setState({
            files: [],
            photo: null,
            model: {
              Name: "",
              FolderId: null
            },
            target: {
              Name: "",
              FolderId: null,
              Id: null
            }
        });
    }

    onChangeTab(type){
      let folder = this.props.store.folders.list.find(folder => folder.Name === type);
      if(!folder) return;

      this.setState({
        model: {
          ...this.state.model,
          FolderId: folder.Id
        },
        target: {
          ...this.state.target,
          FolderId: folder.Id
        }
      })
    }

    render() {

        var selectedFiles = this.state.files.length > 0 ? this.state.files.map(function (file, index) {
            return (
                <div key={index}>
                    <p>{file.name}</p>
                    <p>{(file.size / 1024 / 1024).toFixed(2) + " MB."}</p>
                    <a onClick={()=>this.delFile(index)}>X</a>
                </div>
            );
        }.bind(this)) : null;

        function isModel(folder) {
          return folder.Name !== "Image trackers" && folder.Name !== "All models";
        }

        function isTarget(folder) {
          return folder.Name !== "3D models" && folder.Name !== "All models" && folder.Name !== "Unity 3D";
        }

        var modelFolders = this.props.store.folders.list.length > 0 ? this.props.store.folders.list.filter(isModel).map((folder, index) => {
            return { value: folder.Id, label: folder.Name }
        }) : null;

        var targetFolders = this.props.store.folders.list.length > 0 ? this.props.store.folders.list.filter(isTarget).map((folder, index) => {
            return { value: folder.Id, label: folder.Name }
        }) : null;

        // var folders = this.props.store.folders.list.length > 0 ? this.props.store.folders.list.map((folder, index) => {
        //     return { value: folder.Id, label: folder.Name }
        // }) : null;

        var models = this.state.models.length > 0 ? this.state.models.filter(model => {
           return this.state.targets.findIndex(target => {
             return target.Model.Id === model.Id
           })
        }).map((model, index) => {
            return { value: model.Id, label: model.Name }
        }) : null;

        return (
            <div className="uploads">
                <ul className="nav nav-tabs nav-list" role="tablist">
                    <li className="nav-item col">
                        <a className="nav-link active" data-toggle="tab" href="#home" role="tab" onClick={this.onChangeTab.bind(this, '3D models')}>3D MODEL <span className="hidden-sm">(.obj)</span></a>
                    </li>
                    <li className="nav-item col">
                        <a className="nav-link" data-toggle="tab" href="#profile" role="tab" onClick={this.onChangeTab.bind(this, 'Unity 3D')}>UNITY <span className="hidden-sm">ASSETS</span></a>
                    </li>
                    <li className="nav-item col">
                        <a className="nav-link" data-toggle="tab" href="#messages" role="tab" onClick={this.onChangeTab.bind(this, 'Image trackers')}>IMAGE <span className="hidden-sm">TRACKER</span></a>
                    </li>
                </ul>
                <div className="tab-content">
                    <div className="tab-pane active" id="home" role="tabpanel">
                        <div className="contenido-upload">
                            <div className="width-600">
                                <FileDragAndDrop onDrop={this.handleDrop.bind(this)}>
                                    <form action="#" className=" mydropzone">
                                        <p>Upload your 3D model here. Make sure it is a .obj format. Combine the model, texture, material as a zip file and then upload it. Make sure
                                        that the model does not exceed 65.000 vertices per mesh and 250.000 polygons. Also, make sure that the .obj model name and texture
                                        name are exactly the same as the zip filename.</p>
                                        <div className="image-upload">
                                            <i className="mdi mdi-cloud_upload"></i>
                                            <p>Drag &amp; Drop file to upload.</p>
                                        </div>

                                        <button className="file-btn" type="button">
                                            OR SELECT FILE
                                        </button>
                                        <form className="fileInputForm">
                                            <FileInput name="myImage"
                                                       accept="*"
                                                       placeholder="My Image"
                                                       className="fileInput"
                                                       onChange={this.handleChange.bind(this)} />
                                        </form>
                                        <div className="message1">
                                            {selectedFiles}
                                        </div>
                                    </form>
                                </FileDragAndDrop>
                            </div>
                        </div>
                        <div className="card">
                            <form action="#" className="container">

                                <div className="row">
                                  <div className="col-6">

                                    <div className="form-group">
                                      <label className="label-form">UPLOAD THUMBNAIL <span className="required">REQUIRED</span></label>
                                      
                                      <FileInputThumbnail 
                                        photo={this.state.photo}
                                        onSelectPhoto={this.onSelectPhoto.bind(this)} />
                                    </div>

                                  </div>
                                  <div className="col-6">

                                    <div className="form-group">
                                      <label className="label-form ">NAME YOUR MODEL <span className="required">REQUIRED</span></label>
                                      <input type="text" onChange={e=> this.onModelChange(e.target.value,'Name')} value={this.state.model.Name} className="form-control" />
                                    </div>

                                  </div>
                                </div>
                                { /* <div className="form-group">
                                    <div className="col-sm-12 col-xs-12">
                                        <div className="row">
                                            <div className="col-sm-12 col-md-5 col-lg-5 ">
                                                <label className="label-form">SELECT FOLDER TO SAVE IN </label>
                                                <Select disabled autosize name="form-field-name" value={this.state.model.FolderId} options={modelFolders} onChange={(obj) => {this.onModelChange(obj ? obj.value : null,'FolderId')}}/>
                                            </div>
                                        </div>

                                    </div>
                                </div> */ }
                                <div className="form-group">
                                    <div className="float-right">
                                        <a onClick={this.onCreateModel.bind(this, 'model')}
                                            className="btn save-btn upload-btn">SAVE</a>
                                        <button className="btn-reset"
                                                onClick={this.onClearInput.bind(this)}>
                                            CANCEL
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="tab-pane" id="profile" role="tabpanel">
                        <div className="contenido-upload">
                            <div className="row">
                                <div className="col-sm-12 col-md-5 col-lg-6 ">
                                    <label htmlFor="inputFile" className="label-form">File (Android)</label>
                                    <input type="file" onChange={e=>{this.onSelectFile(e, 'Android')}}
                                            className="form-control" id="inputFile1" aria-describedby="inputFile"
                                            ref="fileName1" />
                                </div>
                                <div className="col-sm-12 col-md-5 col-lg-6 ">
                                    <label htmlFor="inputFile" className="label-form">File (iOS)</label>
                                    <input type="file" onChange={e=>{this.onSelectFile(e, 'iOS')}}
                                            className="form-control" id="inputFile2" aria-describedby="inputFile"
                                            ref="fileName2" />
                                </div>
                                { /* <div className="col-sm-12 col-md-5 col-lg-6 ">
                                    <label htmlFor="inputFile" className="label-form">File (Standalone)</label>
                                    <input type="file" onChange={e=>{this.onSelectFile(e, 'Standalone')}}
                                            className="form-control" id="inputFile3" aria-describedby="inputFile"
                                            ref="fileName3" />
                                </div> */ }
                            </div>
                        </div>
                        <div className="card">
                            <form action="#" className="container">
                                <div className="row">
                                  <div className="col-6">

                                    <div className="form-group">
                                      <label htmlFor="inputFile" className="label-form">UPLOAD THUMBNAIL <span className="required">REQUIRED</span></label>
                                      <FileInputThumbnail 
                                        photo={this.state.photo}
                                        onSelectPhoto={this.onSelectPhoto.bind(this)} />
                                    </div>

                                  </div>
                                  <div className="col-6">

                                    <div className="form-group">
                                      <label className="label-form">NAME YOUR MODEL <span className="required">REQUIRED</span></label>
                                      <input type="text" onChange={e=> this.onModelChange(e.target.value,'Name')}
                                        value={this.state.model.Name} className="form-control" />
                                    </div>
                                    
                                  </div>
                                </div>
                                { /* <div className="form-group">
                                    <div className="col-sm-12 col-xs-12">
                                        <div className="row">
                                            <div className="col-sm-12 col-md-5 col-lg-5 ">
                                                <label className="label-form">SELECT FOLDER TO SAVE IN</label>
                                                <Select disabled autosize name="form-field-name" value={this.state.model.FolderId} options={modelFolders} onChange={(obj) => {this.onModelChange(obj ? obj.value : null,'FolderId')}}/>
                                            </div>
                                        </div>
                                    </div>
                                </div> */ }
                                <div className="form-group">
                                    <div className="float-right">
                                        <a onClick={this.onCreateModel.bind(this, 'bundle')} className="btn save-btn upload-btn">SAVE</a>
                                        <button className="btn-reset" onClick={this.onClearInput.bind(this)}>
                                            CANCEL
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="tab-pane" id="messages" role="tabpanel">
                        <div className="card">
                            <form action="#" className="container">
                                <div className="form-group">
                                    <div className="col-sm-12 col-xs-12">
                                        <div className="row">
                                            <div className="col-sm-12 col-md-5 col-lg-6 ">
                                                <label htmlFor="inputFile" className="label-form">UPLOAD THUMBNAIL <span className="required">REQUIRED</span></label>
                                                { /* <input type="file" onChange={e=>{this.onSelectTarget(e)}}
                                                        className="form-control" id="inputFile" aria-describedby="inputFile" ref="fileName5" /> */}
                                                <FileInputThumbnail 
                                                  photo={this.state.photo}
                                                  onSelectPhoto={this.onSelectPhoto.bind(this)} />
                                            </div>

                                            <div className="col-sm-12 col-md-5 col-lg-6 ">
                                                <label className="label-form">TRACKER NAME <span className="required">REQUIRED</span></label>
                                                <input type="text" className="form-control"
                                                        onChange={(e) => {this.onTargetChange(e.target.value,'Name')}}
                                                        value={this.state.target.Name} />
                                            </div>

                                            <div className="col-sm-12 col-md-7 col-lg-6 ">
                                                <label className="label-form">FOLDER</label>
                                                <Select disabled autosize name="form-field-name" value={this.state.target.FolderId} options={targetFolders} onChange={(obj) => {this.onTargetChange(obj ? obj.value : null,'FolderId')}}/>
                                            </div>

                                            <div className="col-sm-12 col-md-7 col-lg-6 ">
                                                <label className="label-form">LINK IMAGE TRACKER TO MODEL <span className="required">REQUIRED</span></label>
                                                <Select autosize name="form-field-name" value={this.state.target.Id} options={models} onChange={(obj) => {this.onTargetChange(obj ? obj.value : null,'Id')}}/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="float-right">
                                        <a onClick={this.onCreateTarget.bind(this)} className="btn save-btn upload-btn">SAVE</a>
                                        <button className="btn-reset"
                                                onClick={this.onClearInput.bind(this)}>
                                            CANCEL
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
export default connect(store => ({ store }))(Upload);
