import React, { Component } from 'react';
import * as folderAction from './../../../actions/folderAction';
import * as modelAction from './../../../actions/modelAction';
import * as targetAction from './../../../actions/targetAction';
import * as notification from './../../../helpers/notificationHelper';

class FolderController extends Component {

  state = {
    dropdown: false,
    folderName: this.props.folder.Name
  }

  componentDidMount(){
    document.addEventListener('click', this.onClickHandler.bind(this), false);
  }

  componentWillUnmount(){
    document.removeEventListener('click', this.onClickHandler.bind(this), false);
  }

  onClickHandler(e){
    if(this.state.dropdown) this.setState({ dropdown: false });
  }

  onDeleteFolder(e){
    if(e){
      e.stopPropagation();
      e.preventDefault();
    }

    folderAction.remove(this.props.folder.Id).then(ok=>{
      this.setState({ dropdown: false })
      notification.success('Folder deleted');
      folderAction.get()
    }, error=>{
      notification.error(error);
    })
  }

  onContextMenuFolder(e = null){
    if(e) e.preventDefault();
    if(!this.props.editable || this.props.folder.Type === 'System') return;

    console.log(this.props.folder)

    this.setState({ dropdown: !this.state.dropdown })
  }

  onRenameFolder(e){
    if(e){
      e.preventDefault();
      e.stopPropagation();
    }

    window.$("#folder-rename-" + this.props.folder.Id).modal('show');
  }

  onClickSave(){
    let folder = Object.assign({}, this.props.folder);
    folder.Name = this.state.folderName;

    folderAction.modify(folder).then(ok=>{
      window.$("#folder-rename-" + this.props.folder.Id).modal('hide');
      window.$('body').removeClass('modal-open');
      window.$('.modal-backdrop').remove();
      notification.success('Folder renamed');
      folderAction.get();
    }, error => {
      notification.error(error);
    })
  }

  render(){
    return (
      <div
        className={
          "mdl-card mdl-card-folder mb-4 dropdown " +
          (this.props.active ? "active1" : '') + 
          (this.props.className ? " " + this.props.className : '')
        }
        onContextMenu={this.onContextMenuFolder.bind(this)}
        onClick={() => this.props.onSelectFolder(this.props.folder.Id)} >
          <div className="mdl-card__title inline">
              <a>
                  <i className="mdi mdi-folder inline"></i>
                  <h2 className="folderio">{this.props.folder.Name}</h2>
              </a>
          </div>
          <div className={"dropdown-menu" + (this.state.dropdown ? ' show' : '')} aria-labelledby="dropdownMenuButton">
            <a className="dropdown-item" href="#" onClick={this.onRenameFolder.bind(this)}><i className="fa fa-pencil-square-o" aria-hidden="true"></i>Rename</a>
            <a className="dropdown-item" href="" onClick={this.onDeleteFolder.bind(this)}><i className="fa fa-trash" aria-hidden="true"></i>Delete</a>
          </div>

          <div id={"folder-rename-" + this.props.folder.Id} className="modal" tabIndex="-1" role="dialog" onClick={e => e.stopPropagation()} onContextMenu={e => e.stopPropagation()}>
            <div className="modal-dialog modal-sm" role="document">
              <div className="modal-content" style={{ marginTop: '200px' }}>
                <div className="modal-header">
                  <h5 className="modal-title">Rename</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  <div className="form-group">
                    <input
                      value={this.state.folderName}
                      onChange={e => this.setState({ folderName: e.target.value })}
                      className="form-control"
                      placeholder="Enter folder name" />
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn save-btn modal-btn" onClick={this.onClickSave.bind(this)}>Save</button>
                  <button type="button" className="btn cancel-btn modal-btn" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
      </div>
    )
  }

}

export default FolderController;
