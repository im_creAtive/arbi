import React, { Component } from 'react';
import { connect } from 'react-redux';
import {NotificationManager} from 'react-notifications';

import * as paymentPlanAction from './../../../actions/paymentPlanAction';
import * as userAction from './../../../actions/userAction';
import * as paymentAction from './../../../actions/paymentAction';

import done from '../../../sources/img/done.png';
import confirm from '../../../sources/img/confirm.png';
import user from '../../../sources/img/user.png';
import line from '../../../sources/img/line.jpg';
import done_green from '../../../sources/img/done_green.png';

const defState = {
  stage: 'plans',
  selectedPlanId: null,
  termsAndConditions: false,
  countOfMonth: 3,
  customPlan: {
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    description: ""
  }
}

const plans = {
  [2]: {
    name: 'Starter Plan',
    price: 59
  },
  [3]: {
    name: 'Plus Plan',
    price: 149
  },
  [4]: {
    name: 'Pro Plan',
    price: 279
  }
}

class UpgradePlanModal extends Component {

  state = { ...defState };

  onReset(){
    return NotificationManager.info('This section is temporarily unavailable.');
    this.setState({ ...defState });
  }

  onChangeStage(stage){
    this.setState({ stage, termsAndConditions: false })
  }

  onCheckout(selectedPlanId){
    this.setState({ stage: 'checkout', selectedPlanId });
  }

  plan(){
    return plans[this.state.selectedPlanId];
  }

  async onUpgrade(){
    try{
      let response = await paymentPlanAction.upgrade(this.state.selectedPlanId, this.state.countOfMonth);
      if(response.status !== 200)  throw new Error('Something went wrong.. Try later.');
      let data = await response.json();

      if(data === null) throw new Error('You do not have enough money');

      this.onChangeStage('successUpgrade');
      userAction.getCurrent();
      paymentAction.get();
    } catch(e){
      NotificationManager.error(e.message);
    }
  }

  async onSendCustom(){
    try{
      if(this.state.customPlan.firstName === '') throw new Error('First name is required field');
      if(this.state.customPlan.lastName === '') throw new Error('Last name is required field');
      if(this.state.customPlan.email === '') throw new Error('Email is required field');
      if(this.state.customPlan.phone === '') throw new Error('Phone is required field');

      let body = encodeURI(`
        <html>
          <body>
            <h4>Pro Plan</h4>
            <p>First name: ${this.state.customPlan.firstName}</p>
            <p>Last name: ${this.state.customPlan.lastName}</p>
            <p>Email: ${this.state.customPlan.email}</p>
            <p>Phone: ${this.state.customPlan.phone}</p>
            <p>Description: ${this.state.customPlan.description}</p>
          </body>
        </html>
      `);

      let response = await paymentPlanAction.send('ENTERPRISE PLAN REQUEST', body);

      window.$('#modalUpgradePlan').modal('hide');
      window.$('#customPlanEnd').modal('show');
    } catch(e) {
      NotificationManager.error(e.message);
    }
  }

  render(){
    return (
      <div className="mdl-card__actions button">
        {/* data-toggle="modal" data-target="#modalUpgradePlan" */}
        <button type="button" className="purple-btn" onClick={this.onReset.bind(this)}>
          UPGRADE
        </button>

        <div className="modal fade" id="customPlanEnd" tabIndex="-1" role="dialog"aria-hidden="true">
          <div className="modal-dialog modal-sm" role="document">
            <div className="modal-content" style={{ marginTop: '200px' }}>
              <div className="modal-body">
                Thank You for your interest! We will get back to you as soon as possible.
              </div>
            </div>
          </div>
        </div>

        <div key="modalUpgradePlan"
          className="modal fade"
          id="modalUpgradePlan" tabIndex="-1" role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true">
            <div className={"modal-dialog" + (this.state.stage === 'contact' ? ' tall' : '')} role="document">
                <div className="modal-content">

                  { this.state.stage === 'plans' ? (
                    <div className="plans">
                      <div className="modal-header">
                          { this.props.user.data.currentPlan && this.props.user.data.currentPlan.Id === 1 ? (
                            <h2 className="modal-title" id="exampleModalLabel">
                                You're currently on Free Trial.
                                Upgrade to one of our memberships.
                            </h2>
                          ) : null }

                          { this.props.user.data.currentPlan && this.props.user.data.currentPlan.Id === 2 ? (
                            <h2 className="modal-title" id="exampleModalLabel">
                                You’re currently on the Starter Plan. 
                                Upgrade to Plus or Pro.
                            </h2>
                          ) : null }

                          { this.props.user.data.currentPlan && this.props.user.data.currentPlan.Id === 3 ? (
                            <h2 className="modal-title" id="exampleModalLabel">
                                You’re currently on the Plus Plan. 
                                Upgrade to Pro.
                            </h2>
                          ) : null }
                      </div>
                      <div className="modal-body">
                        <div className="text-center">
                          { this.props.user.data.currentPlan &&
                            this.props.user.data.currentPlan.Id === 1 ? (
                            <div className="pricing-plan">
                              <div className="pricing-plan-title green-bg">
                                <h2>Starter Plan</h2>
                                <h3>€<span>59</span>/month</h3>
                              </div>
                              <div className="pricing-plan-body">
                                <ul>
                                    <li>UNLIMITED USERS</li>
                                    <li>1 GB STORAGE</li>
                                    <li>1,000 SCANS</li>
                                    <li>IMAGE TRACKERS</li>
                                    <li>EMAIL SUPPORT</li>
                                </ul>
                              </div>
                              <button type="button" className="green-bg" onClick={this.onCheckout.bind(this, 2)}>Sign up</button>
                            </div>
                          ) : null }
                          
                          { this.props.user.data.currentPlan &&
                            (this.props.user.data.currentPlan.Id === 1 ||
                            this.props.user.data.currentPlan.Id === 2) ? (
                            <div className="pricing-plan">
                              <div className="pricing-plan-title violet-bg">
                                <h2>Plus Plan</h2>
                                <h3>€ <span>149</span>/month</h3>
                              </div>
                              <div className="pricing-plan-body">
                                <ul>
                                    <li>UNLIMITED USERS</li>
                                    <li>2 GB STORAGE</li>
                                    <li>5,000 SCANS</li>
                                    <li>IMAGE TRACKERS</li>
                                    <li>EMAIL SUPPORT</li>
                                    <li>CLIENT TRAINING</li>
                                </ul>
                              </div>
                              <button type="button" className="violet-bg" onClick={this.onCheckout.bind(this, 3)}>Sign up</button>
                            </div>
                          ) : null }

                          { this.props.user.data.currentPlan &&
                            (this.props.user.data.currentPlan.Id === 1 ||
                            this.props.user.data.currentPlan.Id === 2 ||
                            this.props.user.data.currentPlan.Id === 3) ? (
                            <div className="pricing-plan">
                              <div className="pricing-plan-title blue-bg">
                                <h2>Pro Plan</h2>
                                <h3>€ <span>279</span>/month</h3>
                              </div>
                              <div className="pricing-plan-body">
                                <ul>
                                    <li>UNLIMITED USERS</li>
                                    <li>CUSTOM STORAGE</li>
                                    <li>> 5,000 SCANS</li>
                                    <li>IMAGE TRACKERS</li>
                                    <li>PRIORITY SUPPORT</li>
                                    <li>CLIENT TRAINING</li>
                                    <li>KEY ACCOUNT MANAGER</li>
                                </ul>
                              </div>
                              <button type="button" className="blue-bg" onClick={this.onChangeStage.bind(this, 'contact')}>
                                Contact Us
                              </button>
                            </div>
                          ) : null }

                          { this.props.user.data.currentPlan &&
                            this.props.user.data.currentPlan.Id === 4 ? (
                            <div className="pro-upgrade">
                              We'll call you back for the upgrade.<br />
                              <button type="button" className="purple-btn">CALL ME BACK</button>
                            </div>
                          ) : null }
                        </div>
                      </div>
                    </div>
                  ) : null }

                  { this.state.stage === 'checkout' ? (
                    <div className="checkout">
                      <div className="modal-header">
                          <div className="text-center">
                              <img src={user} alt="" className="std" />
                              <p className="mt-2">Summary & customer info</p>
                          </div>
                          <div className="lineIcon pr-3">
                              <img src={line} alt="" className="std" />
                          </div>
                          <div className="text-center">
                              <img src={confirm} alt="" className="std" />
                              <p className="mt-2">Make payment</p>
                          </div>
                          <div className="lineIcon pl-3">
                              <img src={line} alt="" className="std" />
                          </div>
                          <div className="text-center">
                              <img src={done} alt="" className="std" />
                              <p className="mt-2">Sign-up confirmation</p>
                          </div>
                      </div>
                      <div className="modal-body px-5">
                        <div className="checkoutBody" style={{ textAlign: 'left' }}>
                          
                          <h4>Summary</h4>
                          <p className="checkoutTitle"><strong>{ this.plan().name }<span>Price</span></strong></p>
                          <label className="checkoutP">
                            <input 
                              type="radio" 
                              checked={this.state.countOfMonth === 1} 
                              onChange={() => this.setState({ countOfMonth: 1 })} />
                              &nbsp;1-month payment<span>€ { this.plan().price }</span>
                          </label>
                          <label className="checkoutP">
                            <input 
                              type="radio" 
                              checked={this.state.countOfMonth === 3} 
                              onChange={() => this.setState({ countOfMonth: 3 })} />
                              &nbsp;3-month payment<span>€ { this.plan().price * 3 }</span>
                          </label>
                          <label className="checkoutP">
                            <input 
                              type="radio" 
                              checked={this.state.countOfMonth === 12} 
                              onChange={() => this.setState({ countOfMonth: 12 })} />
                            &nbsp;12-month payment<span>€ { this.plan().price * 12 }</span>
                          </label>
                          <p className="checkoutTotal">Total<span>€ { this.plan().price * this.state.countOfMonth }</span></p>
          

                          <div className="checkoutFinal">
                              <input
                                checked={this.state.termsAndConditions}
                                className="my-3 mr-1"
                                type="checkbox"
                                aria-label="Checkbox for following text input"
                                onChange={e => this.setState({ termsAndConditions: !this.state.termsAndConditions })} />
                              I accept the <a href="">Terms & Conditions</a>
                              <button type="button" className="btn btn-block purple-btn my-3"
                                      onClick={this.onUpgrade.bind(this)} disabled={!this.state.termsAndConditions}>
                                  Check Out
                              </button>
                              <span onClick={this.onChangeStage.bind(this, 'plans')} className="other-plan">Select other plan</span>
                          </div>
                        </div>

                      </div>
                    </div>
                  ) : null }

                  { this.state.stage === 'successUpgrade' ? (
                    <div className="checkoutSuccess text-center">
                      <p>
                          Payment is proceeded successfully. You will receive a confirmation email and an email with
                          your account credentials. If you require any support, visit our suport page in your account
                          or contact us on contact@arbi.io.
                      </p>
                      <p className="my-4"><u><b>Valid until 23-08-2017</b></u></p>
                      <img src={done_green} />
                    </div>
                  ) : null }

                  { this.state.stage === 'contact' ? (
                    <div className="contactModal">
                      <div className="modal-header">
                          <p>Send us this form and we will get back to you within 24 hours</p>
                      </div>
                      <div className="modal-body">
                        <form>
                          <div className="row">
                              <div className="col-md-12 col-sm-12 mdl-input mx-auto py-3">
                                  <label>FIRST NAME <span className="required">required</span></label>
                                  <input
                                    type="text"
                                    name="first-name"
                                    className="form-control"
                                    value={this.state.customPlan.firstName}
                                    onChange={e => this.setState({ customPlan: { ...this.state.customPlan, firstName: e.target.value } })} />
                              </div>
                              <div className="col-md-12 col-sm-12 mdl-input mx-auto py-3">
                                  <label>LAST NAME <span className="required">required</span></label>
                                  <input
                                    type="text"
                                    name="first-name"
                                    className="form-control"
                                    value={this.state.customPlan.lastName}
                                    onChange={e => this.setState({ customPlan: { ...this.state.customPlan, lastName: e.target.value } })} />
                              </div>
                          </div>
                          <div className="row">
                              <div className="col-md-12 col-sm-12 mdl-input mx-auto py-3">
                                  <label>WORK EMAIL <span className="required">required</span></label>
                                  <input
                                    type="email"
                                    name="first-name"
                                    className="form-control"
                                    value={this.state.customPlan.email}
                                    onChange={e => this.setState({ customPlan: { ...this.state.customPlan, email: e.target.value } })} />
                              </div>
                          </div>
                          <div className="row">
                              <div className="col-md-12 col-sm-12 mdl-input mx-auto py-3">
                                  <label>PHONE NUMBER <span className="required">required</span></label>
                                  <input
                                    type="tel"
                                    name="first-name"
                                    className="form-control"
                                    value={this.state.customPlan.phone}
                                    onChange={e => this.setState({ customPlan: { ...this.state.customPlan, phone: e.target.value } })} />
                              </div>
                          </div>
                          <div className="row">
                              <div className="col-md-12 col-sm-12 mdl-input mx-auto py-3">
                                  <label>HOW TO YOU INTEND TO USE AUGMENTED REALITY?</label>
                                  <textarea
                                    type="text"
                                    rows="7"
                                    name="first-name"
                                    className="form-control"
                                    value={this.state.customPlan.description}
                                    onChange={e => this.setState({ customPlan: { ...this.state.customPlan, description: e.target.value } })} />
                              </div>
                          </div>
                          <div className="text-center my-4">
                            <button type="button" className="btn green-btn" onClick={this.onSendCustom.bind(this)}>SEND FORM</button>
                            <span onClick={this.onChangeStage.bind(this, 'plans')} className="other-plan">Select other plan</span>
                          </div>
                        </form>
                      </div>
                    </div>
                  ) : null }

                </div>
            </div>
        </div>
      </div>
    )
  }

}

export default connect(state => ({ user: state.user }))(UpgradePlanModal);
