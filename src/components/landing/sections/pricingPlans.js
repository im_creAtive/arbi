import React from 'react';
import { Link } from 'react-router-dom';

class PricingPlans extends React.Component {
	render(){
		return(
			<div className="text-center">
	            <div className="pricing-plan">
	              <div className="pricing-plan-title green-bg">
	                <h2>Starter Plan</h2>
	                <h3>€<span>59</span>/month</h3>
	              </div>
	              <div className="pricing-plan-body">
	                <ul>
	                	<li>UNLIMITED USERS</li>
	                	<li>1 GB STORAGE</li>
	                	<li>1,000 SCANS</li>
	                	<li>IMAGE TRACKERS</li>
	                	<li>EMAIL SUPPORT</li>
	                </ul>
	              </div>
	              <Link className="green-bg" to="/signup/2">Sign Up</Link>
	            </div>
	            <div className="pricing-plan">
	              <div className="pricing-plan-title violet-bg">
	                <h2>Plus Plan</h2>
	                <h3>€ <span>149</span>/month</h3>
	              </div>
	              <div className="pricing-plan-body">
	                <ul>
	                	<li>UNLIMITED USERS</li>
	                	<li>2 GB STORAGE</li>
	                	<li>5,000 SCANS</li>
	                	<li>IMAGE TRACKERS</li>
	                	<li>EMAIL SUPPORT</li>
	                	<li>CLIENT TRAINING</li>
	                </ul>
	              </div>
	              <Link className="violet-bg" to="/signup/3">Sign Up</Link>
	            </div>
	            <div className="pricing-plan">
	              <div className="pricing-plan-title blue-bg">
	                <h2>Pro Plan</h2>
	                <h3>€ <span>279</span>/month</h3>
	              </div>
	              <div className="pricing-plan-body">
	                <ul>
	                	<li>UNLIMITED USERS</li>
	                	<li>CUSTOM STORAGE</li>
	                	<li>> 5,000 SCANS</li>
	                	<li>IMAGE TRACKERS</li>
	                	<li>PRIORITY SUPPORT</li>
	                	<li>CLIENT TRAINING</li>
	                	<li>KEY ACCOUNT MANAGER</li>
	                </ul>
	              </div>
	              <Link className="blue-bg" to="/signup/4">Sign Up</Link>
	            </div>
			</div>
		)
	}
}

export default PricingPlans;
