import React, { Component } from 'react'
import {NotificationManager} from 'react-notifications';
import * as messageAction from '../../../actions/messageAction';
import * as mailAction from '../../../actions/mailAction';

import email3 from '../../public/mails/email3';

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const numbersRegex = /^([^0-9]*)$/
const phoneRegex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/

const defaultState = {
  name: '',
  company: '',
  email: '',
  phone: '',
  message: ''
}

class BottomForm extends Component{

  state = { ...defaultState };

  async onSubmitBottomForm(){
    try{
      this.validator();

      let mail = email3(this.state.name);

      await mailAction.afterFeedback(this.state.email, mail);
      await messageAction.admin(mail);

      window.$('#callbackThanks').modal('show');
      setTimeout(() => {
        window.$('#callbackThanks').modal('hide');
      }, 2000);
      this.setState({ ...defaultState })
    } catch(e) {
      NotificationManager.error(e.message || e);
    }
  }

  validator(){
    if(this.state.name === '') throw new Error('Name is required field.');
    if(this.state.company === '') throw new Error('Company is required field.');
    if(this.state.email === '') throw new Error('Email is required field.');
    if(this.state.phone === '') throw new Error('Phone is required field.');
    if(this.state.message === '') throw new Error('Message is required field.');

    if(!numbersRegex.test(this.state.name)){
      throw new Error('Name field is invalid');
    }

    if(!emailRegex.test(this.state.email)){
      throw new Error('Email field is invalid');
    }

    if(!phoneRegex.test(this.state.phone)){
      throw new Error('Phone number field is invalid');
    }
  }

  render(){
    return (
      <div className="col-sm-6">
        <div className="col-sm-12 contact-input-group">
          <div className="textfield">
            <label>Name <span className="required">Required</span></label>
            <input 
              type="text"
              value={this.state.name}
              onChange={e => this.setState({ name: e.target.value })} />
          </div>
        </div>
        <div className="col-sm-12 contact-input-group">
          <div className="textfield">
            <label>Company <span className="required">Required</span></label>
            <input 
              type="text"
              value={this.state.company}
              onChange={e => this.setState({ company: e.target.value })} />
          </div>
        </div>
        <div className="col-sm-12 contact-input-group">
          <div className="textfield">
            <label>Email <span className="required">Required</span></label>
            <input 
              type="email"
              value={this.state.email}
              onChange={e => this.setState({ email: e.target.value })} />
          </div>
        </div>
        <div className="col-sm-12 contact-input-group">
          <div className="textfield">
            <label>Phone incl. country code <span className="required">Required</span></label>
            <input 
              type="text"
              value={this.state.phone}
              onChange={e => this.setState({ phone: e.target.value })} />
          </div>
        </div>
        <div className="col-sm-12 contact-input-group">
          <div className="textfield">
            <label>Message <span className="required">Required</span></label>
            <textarea 
              value={this.state.message}
              onChange={e => this.setState({ message: e.target.value })}></textarea>
          </div>
        </div>
        <div className="col-sm-12">
          <div className="row">
          <div className="col-sm-12">
            <button type="submit" className="violet-bg" onClick={this.onSubmitBottomForm.bind(this)}>Submit</button>
          </div>
          </div>
        </div>
      </div>
    )
  }

}

export default BottomForm;