import React from 'react';
import * as authorizationAction from './../../../actions/authorizationAction';
import * as userAction from './../../../actions/userAction';
import * as notification from './../../../helpers/notificationHelper';
import {NotificationManager} from 'react-notifications';
import { withRouter } from 'react-router';
import { HashLink } from 'react-router-hash-link';

class Login extends React.Component {

  constructor(props){
      super(props);

      this.state = {
          user:{
              email: '',
              password: '',
              remember: false
          }
      };
  }
  
  componentDidMount(){
      let rememberEmail = localStorage.getItem('rememberEmail');
      
      if(rememberEmail) this.setState({
        user: {
          email: rememberEmail,
          password: '',
          remember: true
        }
      });
  }

  onLoginAction (e){
      if(e) e.preventDefault();

      let history = this.props.history;

      if(this.state.user.email === '') {
        return notification.error("Email field is required");
      }

      if(this.state.user.password === '') {
        return notification.error("Password field is required");
      }

      authorizationAction.login(this.state.user.email,this.state.user.password).then(()=>{
          userAction.getCurrent().then(user=>{
              if(user === undefined) return false;

              //notification.success('Authorized');

              // remember
              if(this.state.user.remember){
                localStorage.setItem('rememberEmail', this.state.user.email);
              } else {
                localStorage.removeItem('rememberEmail');
              }

              if (user.RoleIds.find(x=>x == "Admin")){
                  history.push("/superadmin");
              }else {
                  history.push("/cabinet");
              }
          }, error => {
              notification.error(error);
          })
          //history.push("/cabinet");
      }, error=>{
          try{
            error = JSON.parse(error);
            notification.error(error.error_description || error.error || 'Error. Account not found');
          } catch(e){
            console.log(e)
            notification.error('Error. Account not found');
          }
      });
  }

  onChange(value, prop){
      this.state.user[prop] = value;
      this.setState(this.state);
  }



	render(){
    	return(
            <section id="login-section">
            <div className="container">
              <form onSubmit={this.onLoginAction.bind(this)}>
                <div className="col-md-12 text-center">
                <h3 className="login-container-h">Already have an account?</h3>
                </div>
                <div className="col-md-12 text-center">
                <p id="login-container-p">Login to your account</p>
                <div className="login-container">
                    <div className="login-container-content">
                    <div className="col-md-12">
                        <h3 className="login-container-h">Welcome</h3>
                    </div>
                    <div className="col-md-12">
                        <div className="input-group">
                        <input 
                          type="text" 
                          onChange={e=> this.onChange(e.target.value, 'email')} 
                          className="form-control" 
                          placeholder="Username"
                          value={this.state.user.email} />
                        <span><i className="fa fa-user fa-2x login-icon" aria-hidden="true"></i></span>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="input-group">
                        <input 
                          type="password" 
                          onChange={e=> this.onChange(e.target.value, 'password')} 
                          className="form-control" 
                          placeholder="Password"
                          value={this.state.user.password} />
                        <span><i className="fa fa-lock fa-2x login-icon" aria-hidden="true"></i></span>
                        </div>
                    </div>
                    <div className="col-md-12 text-left">
                        <div className="form-check">
                        <label className="form-check-label">
                        <input 
                          type="checkbox" 
                          className="form-check-input"
                          checked={this.state.user.remember}
                          onChange={e=> this.onChange(!this.state.user.remember, 'remember')} />
                            Remember Me
                        </label>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <button type="submit" onClick={this.onLoginAction.bind(this)} id="sign-in-button" className="btn btn-block">
                            <i className="fa fa-sign-in" aria-hidden="true"></i>
                            Sign in
                        </button>
                    </div>
                    <div className="col-md-12 no-account">
                      <HashLink to="/#callback">Click here to create an account.</HashLink>
                    </div>

                  </div>
                </div>
                </div>
              </form>
            </div>
            </section>
    	)
    }
}

export default withRouter(Login);
