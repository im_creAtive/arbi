import React from 'react';
import * as authorizationAction from './../../../actions/authorizationAction';
import { withRouter } from 'react-router';
import {NotificationManager} from 'react-notifications';
import { HashLink } from 'react-router-hash-link';

import PricingPlans from './pricingPlans';
import BottomForm from './bottomForm';

import done from './../../../sources/img/done.jpg';
import email from './../../../sources/img/email.png';
import logo_obj from './../../../sources/img/logo_obj.jpg';
import logo_unity from './../../../sources/img/logo_unity.svg';
import arrow_down from './../../../sources/img/arrow_down.png';
import arrow_left from './../../../sources/img/arrow_left.png';
import arrow_right from './../../../sources/img/arrow_right.png';
import arbio_video from './../../../sources/video/arbio_video.mp4';
import poster_image from './../../../sources/img/poster_image.png';
import play_video from './../../../sources/img/play_video.png';
import tablet_laptop from './../../../sources/img/tablet_laptop.png';

import landingCallback from '../../public/mails/landingCallback';
import emailConfirm from '../../public/mails/emailConfirm';
import * as messageAction from '../../../actions/messageAction';
import * as mailAction from '../../../actions/mailAction';

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const numbersRegex = /^([^0-9]*)$/
const phoneRegex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/

class Landing extends React.Component {

	constructor(props){
		super(props);

		this.state = {
				user: {
					Email: "",
					FirstName: "",
					LastName: "",
					PhoneNumber: ""
        },
        callback: {
          fullname: '',
          company: '',
          phone: '',
          email: ''
        },
				videoButton: 'play'
		};
	}

	componentDidMount(){
		//this.landingVideo = document.getElementById('landing-video');
		//this.landingVideo.addEventListener('ended', this.onVideoEnd.bind(this), false);
	}

	playVideo(){
		this.setState({ videoButton: 'none' })
		this.landingVideo.play();
	}

	onVideoEnd(){
		this.setState({ videoButton: 'get-start' })
	}

	onRegister(e){
    if(e) e.preventDefault();

		let history = this.props.history;

		if(this.state.user.FirstName === '' || this.state.user.LastName === '' || this.state.user.Email === ''){
			return NotificationManager.error('All form fields must be filled');
    }

    if(!numbersRegex.test(this.state.user.FirstName)){
      return NotificationManager.error('First name field is invalid');
    }

    if(!numbersRegex.test(this.state.user.LastName)){
      return NotificationManager.error('Last name field is invalid');
    }

    if(!emailRegex.test(this.state.user.Email)){
      return NotificationManager.error('Email field is invalid');
    }

    if(!phoneRegex.test(this.state.user.PhoneNumber) && this.state.user.PhoneNumber !== ''){
      return NotificationManager.error('Phone number field is invalid');
    }

		authorizationAction.registration(this.state.user).then(async (ok) => {
			// NotificationManager.success('Your account created');
      // NotificationManager.success('Password sent with email');
      await mailAction.afterFeedback(this.state.user.Email, emailConfirm(this.state.user.FirstName, window.location.origin + '/emailconfirmed/' + this.state.user.Email));      
			window.$('#successRegister').modal('show');
      //history.push('/login');
      
      setTimeout(() => {
        window.$('#successRegister').modal('hide');
      }, 2000);

			this.setState({
				user: {
					Email: "",
					FirstName: "",
					LastName: "",
					PhoneNumber: ""
				}
      })
		}, error => {
			try{
        error = JSON.parse(error);
        NotificationManager.error(error.Message || error.error || 'Error. Try later.');
      } catch(e){
        console.log(e)
        NotificationManager.error('Error. Try later.');
      }
		})
	}

	onChange(value, prop){
		this.state.user[prop] = value;
		this.setState(this.state);
	}


	async onClickCallMeBack(){

    try{
      if(this.state.callback.fullname === '') throw new Error('Fullname is required field.');
      if(this.state.callback.company === '') throw new Error('Company is required field.');
      if(this.state.callback.phone === '') throw new Error('Phone is required field.');
      if(this.state.callback.email === '') throw new Error('Email is required field.');

      if(!numbersRegex.test(this.state.callback.fullname)){
        return NotificationManager.error('Full name field is invalid');
      }
  
      if(!emailRegex.test(this.state.callback.email)){
        return NotificationManager.error('Email field is invalid');
      }
  
      if(!phoneRegex.test(this.state.callback.phone)){
        return NotificationManager.error('Phone number field is invalid');
      }

      let mail = landingCallback(
        this.state.fullname, 
        this.state.callback.company, 
        this.state.callback.phone, 
        this.state.callback.email);
      await messageAction.admin(mail);

      window.$('#callbackThanks').modal('show')
      this.setState({ callback: { fullname: '', company: '', phone: '', email: '' } });
    } catch(e) {
      console.log(e)
      NotificationManager.error(e.message ? e.message : e);
    }

    window.$('#scheduleCallBackModal').modal('hide');
	}

  render(){

  	var scheduleCallBackModal = (
      <div key="scheduleCallBackModal" className="modal" id="scheduleCallBackModal" tabIndex="-1"
            role="dialog" aria-hidden="true">
        <div className="modal-dialog" role="document">
            <div className="modal-content callBackModalContent">
                <div className="modal-body">
                  <div className="form-container">
                    <div className="col-md-12">
                      <div className="input-group">
                        <input 
                          type="text" 
                          className="form-control" 
                          placeholder="Full name"
                          value={this.state.callback.fullname}
                          onChange={e => this.setState({ callback: { ...this.state.callback, fullname: e.target.value } })} />

                          <a className="trialIcon" href="" tabIndex="-1">
                          <span><i className="fa fa-user-circle" aria-hidden="true"></i></span>
                        </a>
                      </div>
                    </div>
                    <div className="col-md-12">
                      <div className="input-group">
                        <input 
                          type="text" 
                          className="form-control" 
                          placeholder="Company name"
                          value={this.state.callback.company}
                          onChange={e => this.setState({ callback: { ...this.state.callback, company: e.target.value } })} />
                        <a className="trialIcon" href="" tabIndex="-1">
                          <span><i className="fa fa-building" aria-hidden="true"></i></span>
                        </a>
                      </div>
                    </div>
                    <div className="col-md-12">
                      <div className="input-group">
                        <input 
                          type="text" 
                          className="form-control" 
                          placeholder="Email"
                          value={this.state.callback.email}
                          onChange={e => this.setState({ callback: { ...this.state.callback, email: e.target.value } })} />
                        <a className="trialIcon" href="" tabIndex="-1">
                          <span><i className="fa fa-envelope" aria-hidden="true"></i></span>
                        </a>
                      </div>
                    </div>
                    <div className="col-md-12">
                      <div className="input-group">
                        <input 
                          type="text" 
                          className="form-control" 
                          placeholder="Phone number incl. landcode"
                          value={this.state.callback.phone}
                          onChange={e => this.setState({ callback: { ...this.state.callback, phone: e.target.value } })} />
                        <a className="trialIcon" href="" tabIndex="-1">
                          <span><i className="fa fa-phone" aria-hidden="true"></i></span>
                        </a>
                      </div>
                    </div>
                    <div className="col-md-12 modalCallButton">
                      <button type="button" className="btn btn-block callBackModalBtn py-2"
                      		  onClick={this.onClickCallMeBack.bind(this)}>
                        <i className="fa fa-paper-plane-o" aria-hidden="true"></i>
                        Call me back
                      </button>
                    </div>
                  </div> {/*end of form-container*/}
                  { /* <div className="callbackHint" id="callbackHint1">
                  	<p className="mb-0">Keep your phone close. We will get back to you within 24 hours.</p>
                  </div> */ }
                </div>
            </div>
        </div>
      </div>
    )

    return(
      <div>

        <div className="modal" id="trialModal" tabIndex="-1"
              role="dialog" aria-hidden="true">
          <div className="modal-dialog" role="document">
              <div className="modal-content callBackModalContent">
                  <div className="modal-body">
                    <form onSubmit={this.onRegister.bind(this)}>
                      <div className="form-container">
                      <p>Create your free account!</p>
                        <div className="col-md-12">
                          <div className="input-group">
                            <input 
                              type="text" 
                              className="form-control" 
                              placeholder="Email"
                              value={this.state.user.Email}
                              onChange={e => this.setState({ user: { ...this.state.user, Email: e.target.value } })} />

                              <a className="trialIcon" href="" tabIndex="-1">
                              <span><i className="fa fa-check" aria-hidden="true"></i></span>
                            </a>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="input-group">
                            <input 
                              type="text" 
                              className="form-control" 
                              placeholder="First name"
                              value={this.state.user.FirstName}
                              onChange={e => this.setState({ user: { ...this.state.user, FirstName: e.target.value } })} />
                            <a className="trialIcon" href="" tabIndex="-1">
                              <span><i className="fa fa-check" aria-hidden="true"></i></span>
                            </a>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="input-group">
                            <input 
                              type="text" 
                              className="form-control" 
                              placeholder="Last name"
                              value={this.state.user.LastName}
                              onChange={e => this.setState({ user: { ...this.state.user, LastName: e.target.value } })} />
                            <a className="trialIcon" href="" tabIndex="-1">
                              <span><i className="fa fa-envelope" aria-hidden="true"></i></span>
                            </a>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="input-group">
                            <input 
                              type="text" 
                              className="form-control" 
                              placeholder="Phone number incl. landcode"
                              value={this.state.user.PhoneNumber}
                              onChange={e => this.setState({ user: { ...this.state.user, PhoneNumber: e.target.value } })} />
                            <a className="trialIcon" href="" tabIndex="-1">
                              <span><i className="fa fa-phone" aria-hidden="true"></i></span>
                            </a>
                          </div>
                        </div>
                        <div className="col-md-12 modalCallButton">
                          <button type="button" className="btn btn-block callBackModalBtn py-2"
                                onClick={this.onRegister.bind(this)} style={{ color: '#fff' }}>
                            <i className="fa fa-paper-plane-o" aria-hidden="true"></i>
                            Create account
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
              </div>
          </div>
        </div>

				<div className="modal fade" id="callbackThanks" tabIndex="-1" role="dialog"aria-hidden="true">
					<div className="modal-dialog modal-sm" role="document">
						<div className="modal-content" style={{ marginTop: '200px' }}>
							<div className="modal-body">
                Thank you for your message. We will reply within 24 hours or less. Promised!
							</div>
						</div>
					</div>
				</div>

				<div className="modal fade" id="successRegister" tabIndex="-1" role="dialog" aria-hidden="true">
					<div className="modal-dialog" role="document">
						<div className="modal-content" style={{ marginTop: '200px', padding: 40 }}>
							<div className="modal-body">
                Thanks for signing up. Check your email for the access link.
							</div>
						</div>
					</div>
        </div>
        

				<section id="section-home">
	        <div className="bg-image"></div>
	          <div id="opacity"></div>
	          <div className="container">
	            <div className="row">
	              <div className="col-lg-8 col-sm-7 main">
	                <div className="main-text text-left">
	                  <h1 className="">
                      Remember Pokemon Go?<br />Use the same Augmented Reality tech for your business. Free for designers & developers.
	                  </h1>
	                  <p className="">
                      Supporting OBJ and Unity assets. Upload your 3D assets and use them in AR in 1 minute.
	                  </p>
	                  <div id="mobileTrialBtnDiv">
	                  	<button data-toggle="modal" data-target="#trialModal" type="submit" className="btn btn-block trial-button">
	                        <i className="fa fa-paper-plane-o" aria-hidden="true"></i>
	                        Start trial
	                    </button>
	                  </div>
	                </div>
	              </div> {/*end of col-md-8*/}
	              <div className="col-lg-4 col-sm-5">
                  <div className="row-fluid">
                    <form onSubmit={this.onRegister.bind(this)}>
                      <div className="form-container" id="desktopForm">
                        <div className="col-md-12">
                          <p>
                          Create your free account!
                          </p>
                        </div>
                        <div className="col-md-12">
                          <div className="input-group">
                            <input value={this.state.user.FirstName} onChange={e=>{this.onChange(e.target.value,'FirstName')}} type="text" className="form-control" placeholder="First Name *" />
                            <a className="trialIcon" href="" tabIndex="-1">
                              <span><i className="fa fa-check" aria-hidden="true"></i></span>
                            </a>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="input-group">
                            <input type="text" value={this.state.user.LastName} onChange={e=>{this.onChange(e.target.value,'LastName')}} className="form-control" placeholder="Last Name *" />
                            <a className="trialIcon" href="" tabIndex="-1">
                              <span><i className="fa fa-check" aria-hidden="true"></i></span>
                            </a>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="input-group">
                            <input type="email" value={this.state.user.Email}  onChange={e=>{this.onChange(e.target.value,'Email')}} className="form-control" placeholder="Email *" />
                            <a className="trialIcon" href="" tabIndex="-1">
                              <span><i className="fa fa-envelope" aria-hidden="true"></i></span>
                            </a>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="input-group">
                            <input type="text" value={this.state.user.PhoneNumber}  onChange={e=>{this.onChange(e.target.value,'PhoneNumber')}} className="form-control" placeholder="Phone incl. country code" />
                            <a className="trialIcon" href="" tabIndex="-1">
                              <span><i className="fa fa-phone" aria-hidden="true"></i></span>
                            </a>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <button type="submit" onClick={this.onRegister.bind(this)} className="btn btn-block trial-button">
                            <i className="fa fa-paper-plane-o" aria-hidden="true"></i>
                            Create account
                          </button>
                        </div>
                      </div> {/*end of form-container*/}
                    </form>
	                </div> {/*end of form-container*/}
	              </div> {/*end of col-md-4*/}
	          </div> {/*end of row*/}
          	</div> {/*end of container*/}
	    </section>

	    <section id="unity">
	      <div className="container">
	        <div className="row">
	          <div className="col-md-12">
	            <h3>Bring your work to life with Augmented Reality. Completely free for designers & developers.</h3>
	          </div>
	          <div className="col-md-12">
	            <p>
                We’re on a mission to make Augmented Reality as easy as possible. With Arbi you can now upload your own 3D assets and showcase these in
                Augmented Reality. Share your work with clients, partners or your audience. Start using it and let us know how to make Arbi better for you
                and your business.
	            </p>
	            <p id="features"><b>We support these 3D formats:</b></p>
	          </div>
	          <div id="logos-container">
	          	<img src={logo_obj} width="50" />
	          	<img src={logo_unity} width="100" />
	          </div>
	        </div>
	      </div>
	    </section>

	    <section id="existing">
	      <div className="container-fluid tablet">
	        <div className="row">
	          <div className="col-md-12 text-center">
	            <h3>No SDK. No coding. Use your own 3D content.<br />
	            	Augmented reality in 1 minute.
	            </h3>
	            <div className="white-line"></div>
	          </div>
	          <div className="col-md-12 arrow-container">
		          <div className="col-md-12 arrow-div">
		            <p>
		              <img src={arrow_down} />
		              Design your 3D models using your own 3D software.
		              Export the designs and upload them in Arbi.
		            </p>
		          </div>
	  	          <div className="col-md-12 arrow-div">
		            <p>
		              <img src={arrow_left} />
		              Log into your Arbi account, upload the files and save them into your library.
		            </p>
		          </div>
	  	          <div className="col-md-12 arrow-div">
		            <p>
		              <img src={arrow_right} />
		              Use the Arbi app on smartphone or tablet, login to your library and view your 3D
		              content in Augmented Reality.
		            </p>
		          </div>
		      </div>
	        </div>
	      </div>
	    </section>

	    { /* <div id="video-arbio">
				{ this.state.videoButton === 'play' ? (
					<button type="submit" onClick={e => this.playVideo()}>
		        Watch the video
		        <img src={play_video} />
		      </button>
				) : null }
				{ this.state.videoButton === 'get-start' ? (
					<HashLink to="/#get-started" className="go-start">Get started</HashLink>
				) : null }
	      <video id="landing-video" src={arbio_video} poster={poster_image}></video>
	    </div> */ }

	    <section id="tablet">
	      <div className="container-fluid tablet">
	      	<div className="row">
	      	  { /* <div className="col-md-6 col-sm-6 img-container">
	      	  	<img src={tablet_laptop} />
            </div> */ }
	      	  <div className="col-md-6 col-sm-6 store-container">
	      	    <h3>No AR hardware required.<br />Just your smartphone or tablet</h3>
	      	    <p>
	      	      No need to invest in expensive AR hardware such as AR-glasses.
	      	      Use your smartphone or tablet to access your content in AR with the Arbi App.
	      	    </p>
	      	    <div className="store-btn-div">
	      	      <button id="play-store">
	      	      	<i className="fa fa-2x fa-play" aria-hidden="true"></i>
	      	      	<span>Download on the Play Store</span>
	      	      </button>
	      	    </div>
	      	    <div className="store-btn-div">
	      	      <button id="apple-store">
	      	      	<i className="fa fa-2x fa-apple" aria-hidden="true"></i>
	      	      	<span>Download on the Apple Store</span>
	      	      </button>
	      	    </div>
	      	  </div>
	      	  <div className="col-md-12 bottom-div">
	      	  	<p>
	      	  	  Deploy AR in Sales across Manufacturing,
	      	  	  Architecture & Interior<br />Design. Prototype & test new product-fit faster.
	      	  	</p>
	      	  </div>
	      	</div>
	      </div>
	    </section>

	    <section id="product">
	      <div className="container">
	      	<div className="row">
	      	  <div className="col-md-4 col-sm-4" id="column-1">
	      	    <h3>Take your product portfolio with you.</h3>
	      	    <div className="white-line"></div>
	      	    <p>
	      	      Being able to showcase our products in-real-life makes the client pitch much
	      	      more powerfull than a simple catalog.
	      	    </p>
	      	    <p>It makes the pitch tangible.</p>
	      	  </div>
  	      	  <div className="col-md-4 col-sm-4" id="column-2">
	      	    <h3>Design faster & cheaper.</h3>
	      	    <div className="white-line"></div>
	      	    <p>
	      	      Clients view their product designs in Augmented Reality and make adjustments
	      	      without a physical mock-up.
	      	    </p>
	      	    <p>Reduces cost & design-time.</p>
	      	  </div>
	      	  <div className="col-md-4 col-sm-4" id="column-3">
	      	    <h3>Pre-sell prototypes digitally.</h3>
	      	    <div className="white-line"></div>
	      	    <p>
	      	      Manufacturing prototypes is costly & time-consuming. We use digital prototypes
	      	      in Augmented Reality to market-test product interest.
	      	    </p>
	      	    <p>It's the next best thing.</p>
	      	  </div>
	      	</div>
	      </div>
	    </section>


	    { /* <section id="learn">
	    	<div className="container">
	    	  <div className="row">
	    	  	<div className="col-sm-12">
	    	  	  <h3>
	    	  	    Looking to learn how to benefit from Augmented Reality in Education,
	    	  	    Medical field or Advertising?
	    	  	  </h3>
	    	  	</div>
	    	  	<button type="button" onClick={() => window.$('#scheduleCallBackModal').modal('show')}>Learn more</button>
	    	  </div>
	    	</div>
      </section> */ }

	    { /* <div className="container-fluid background-divider" id="get-started"></div> */ }

	    <section id="callback">
	      <div className="container">
	        <div className="col-md-12">
	          <h3>Ready to Get Started?</h3>
	        </div>
	        <div className="col-md-12">
	          <p>Create your free account now</p>
            <div className="form-container">
              <div className="col-md-12">
                <p>
                Create your free account!
                </p>
              </div>
              <div className="col-md-12">
                <div className="input-group">
                  <input value={this.state.user.FirstName} onChange={e=>{this.onChange(e.target.value,'FirstName')}} type="text" className="form-control" placeholder="First Name *" />
                  <a className="trialIcon" href="" tabIndex="-1">
                    <span><i className="fa fa-check" aria-hidden="true"></i></span>
                  </a>
                </div>
              </div>
              <div className="col-md-12">
                <div className="input-group">
                  <input type="text" value={this.state.user.LastName} onChange={e=>{this.onChange(e.target.value,'LastName')}} className="form-control" placeholder="Last Name *" />
                  <a className="trialIcon" href="" tabIndex="-1">
                    <span><i className="fa fa-check" aria-hidden="true"></i></span>
                  </a>
                </div>
              </div>
              <div className="col-md-12">
                <div className="input-group">
                  <input type="email" value={this.state.user.Email}  onChange={e=>{this.onChange(e.target.value,'Email')}} className="form-control" placeholder="Email *" />
                  <a className="trialIcon" href="" tabIndex="-1">
                    <span><i className="fa fa-envelope" aria-hidden="true"></i></span>
                  </a>
                </div>
              </div>
              <div className="col-md-12">
                <div className="input-group">
                  <input type="text" value={this.state.user.PhoneNumber}  onChange={e=>{this.onChange(e.target.value,'PhoneNumber')}} className="form-control" placeholder="Phone  incl. country code" />
                  <a className="trialIcon" href="" tabIndex="-1">
                    <span><i className="fa fa-phone" aria-hidden="true"></i></span>
                  </a>
                </div>
              </div>
              <div className="col-md-12">
                <button type="submit" onClick={this.onRegister.bind(this)} className="btn btn-block trial-button">
                  <i className="fa fa-paper-plane-o" aria-hidden="true"></i>
                  Create account
                </button>
              </div>
            </div> {/*end of form-container*/}

              {/*
              
                <div className="form-container" id="desktopForm">
                        <div className="col-md-12">
                          <p>
                          Create your free account!
                          </p>
                        </div>
                        <div className="col-md-12">
                          <div className="input-group">
                            <input value={this.state.user.FirstName} onChange={e=>{this.onChange(e.target.value,'FirstName')}} type="text" className="form-control" placeholder="First Name *" />
                            <a className="trialIcon" href="" tabIndex="-1">
                              <span><i className="fa fa-check" aria-hidden="true"></i></span>
                            </a>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="input-group">
                            <input type="text" value={this.state.user.LastName} onChange={e=>{this.onChange(e.target.value,'LastName')}} className="form-control" placeholder="Last Name *" />
                            <a className="trialIcon" href="" tabIndex="-1">
                              <span><i className="fa fa-check" aria-hidden="true"></i></span>
                            </a>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="input-group">
                            <input type="email" value={this.state.user.Email}  onChange={e=>{this.onChange(e.target.value,'Email')}} className="form-control" placeholder="Email *" />
                            <a className="trialIcon" href="" tabIndex="-1">
                              <span><i className="fa fa-envelope" aria-hidden="true"></i></span>
                            </a>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <div className="input-group">
                            <input type="text" value={this.state.user.PhoneNumber}  onChange={e=>{this.onChange(e.target.value,'PhoneNumber')}} className="form-control" placeholder="Phone" />
                            <a className="trialIcon" href="" tabIndex="-1">
                              <span><i className="fa fa-phone" aria-hidden="true"></i></span>
                            </a>
                          </div>
                        </div>
                        <div className="col-md-12">
                          <button type="submit" onClick={this.onRegister.bind(this)} className="btn btn-block trial-button">
                            <i className="fa fa-paper-plane-o" aria-hidden="true"></i>
                            Create account
                          </button>
                        </div>
                      </div>
              
              */}
	        </div>
	      </div>
	    </section>

	    { /* <section id="pricing">
	      <div className="container">
	        <div className="row">
	          <div className="col-md-12" id="content-div">
	            <div className="col-md-12">
	              <h3 id="payment-plans">...or get started with one of our plans</h3>
	            </div>
	            <PricingPlans />
	          </div>
	        </div>
	      </div>
      </section> */ }

	    <div className="container-fluid background-divider"></div>

	    { /* <section id="FAQ">
	      <div className="container">
	        <div className="row">
	          <div className="col-sm-12">
	          	<h3>Frequently asked questions.</h3>
	          	<div className="white-line"></div>
	          	<div className="row">
	          	  <div className="col-md-6 col-sm-12">
	          	    <h3>Explain Augmented Reality to me as if I were a child.</h3>
	          	    <p>
	          	      You’ve most likely have seen or played Pokemon Go where Pokemon characters are
	          	      found in a real-life environment. That’s AR. Combining digital 3D content in a
	          	      real-life environment.
	          	    </p>
	          	  </div>
	          	  <div className="col-md-6 col-sm-12">
	          	    <h3>How does that help me?</h3><br />
	          	    <p>
	          	      You’re making the client conversation tangible. He doesn’t need to visualize what you’re
	          	      pitching or selling. This means shorter sales cycles & more revenue.
	          	    </p>
	          	  </div>
	          	</div>
	          	<div className="row">
	          	  <div className="col-md-6 col-sm-12">
	          	    <h3>How would it work for my business in sales?</h3>
	          	    <p>
	          	      Speaking about products always gets you the question: can you show me something?
	          	      With AR you can show your products in real life without them being physically present.
	          	    </p>
	          	  </div>
	          	  <div className="col-md-6 col-sm-12">
	          	    <h3>Is it a complex process to implement AR?</h3>
	          	    <p>
	          	      No. You dont adjust to us. We have adjusted our system to your existing process. All you
	          	      need to do is upload 3D product designs which already exist in your business.
	          	    </p>
	          	  </div>
	          	</div>
	          	<div className="row">
	          	  <div className="col-md-6 col-sm-12">
	          	    <h3>Do you have a practical example?</h3>
	          	    <p>
	          	      Take a trade-show. You got limited space & budget to show physical products. With AR
	          	      you can show as many products as you want by using your tablet.
	          	    </p>
	          	  </div>
	          	  <div className="col-md-6 col-sm-12">
	          	    <h3>What if I need more tailored 3D content?</h3>
	          	    <p>
	          	      We’ll be happy to work with you in developing specialized 3D content at very competitive prices.
	          	    </p>
	          	  </div>
	          	</div>
	          	<div className="row">
	          	  <div className="col-md-6 col-sm-12">
	          	    <h3>Give me another example.</h3>
	          	    <p>
	          	      Sure. You’re designing product prototypes for a client. Show them their design in AR
	          	      instead of a physical mock-up. Save money and speed up the process.
	          	    </p>
	          	  </div>
	          	  <div className="col-md-6 col-sm-12">
	          	    <h3>Are there other AR user cases?</h3>
	          	    <p>No. You dont adjust to us. We have adjusted our system to your existing process. All you need
	          	    to do is upload 3D product designs which already exist in your business.</p>
	          	  </div>
	          	</div>

	          </div>
	        </div>
	      </div>
      </section>

	    <div className="container-fluid background-divider"></div>*/ }

	    <section id="contact">
	      <div className="container">
	        <div className="row">
	          <div className="col-sm-12">
	            <h3>Questions, comments, requests, whatever it is - ask and we shall reply.</h3>
	          </div>
	          <div className="contact-container">
	          	<div className="row">
      	          <div className="col-sm-6 head">
		          	<h4>GET IN TOUCH WITH US</h4>
		          	<p>
									<a href="mailto:contact@arbi.io" className="get-in-touch-email">
			          	  <i className="fa fa-envelope" aria-hidden="true"></i>
			          	  contact@arbi.io
									</a>
		          	</p>
              </div>
              
		          <BottomForm />
	        	  </div>
	        	</div>
          </div>
	      </div>
	      {scheduleCallBackModal}
	    </section>

	  </div>
	)
  }
}

export default withRouter(Landing);
