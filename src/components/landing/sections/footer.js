import React from 'react';
import { HashLink } from 'react-router-hash-link';


class Footer extends React.Component {
	render(){
    	return(
    		<footer id="landingFooter">
    		  <div className="container">
    		    <div className="row">
    		      <div className="col-md-5 col-sm-5 col-xs-12">
    		      	<p>	&copy;2017 Arbi All Right Reserved.</p>
    		      </div>
      		      <div className="col-md-7 col-sm-7 col-xs-12 socialIconsCol">
    		      	<ul>
    		      	  { /* <li>
                        <HashLink to="/#logos-container">Features</HashLink>
                      </li>
                      <li>
                        <HashLink to="/#content-div" className="go-start">Pricing</HashLink>
                      </li>
                      { /* <li>
                        <a href="">Blog</a>
                      </li>
                      <li>
                        <HashLink to="/#contact">Contact</HashLink>
                      </li> */ }
                    </ul>
                    <div className="socialIcons">
                    <ul>
    		      	  <li>
    		      	  	<a href="https://www.facebook.com/arbiapp/">
    		      	  	  <i className="fa fa-facebook" aria-hidden="true"></i>
    		      	  	</a>
    		      	  </li>
    		      	  {/*<li>
    		      	  	<a href="">
    		      	  	  <i className="fa fa-twitter" aria-hidden="true"></i>
    		      	  	</a>
    		      	  </li>
    		      	  <li>
    		      	  	<a href="">
    		      	  	  <i className="fa fa-google-plus" aria-hidden="true"></i>
    		      	  	</a>
                  </li>*/ }
                  <li>
                    <a href="https://www.linkedin.com/company/17998258/">
                      <i className="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                  </li>
    		      	</ul>
                    </div>
    		      </div>
    		    </div>
    		  </div>
    		</footer>
    	)
    }
}

export default Footer;