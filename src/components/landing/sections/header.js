import React from 'react';
import arbi_logo from './../../../sources/img/arbi_logo.png';
import arbi_logo_white from './../../../sources/img/arbi_logo_white.png';
import done from './../../../sources/img/done.jpg';
import { Link } from 'react-router-dom';
import { HashLink } from 'react-router-hash-link';

class Header extends React.Component {
  componentDidMount(){
    const body = document.querySelector('html');
    const nav = document.getElementById('landingNav');

    if (this.props.enabledShowMenu) {
      nav.classList.add('color');
    }
    let onScroll = () => {
      if(body.scrollTop <= 0 && !this.props.enabledShowMenu){
        nav.classList.remove('color');
      } else {
        nav.classList.add('color');
      }
    };

    if(!this.props.color) document.addEventListener('scroll', onScroll.bind(this));
  }

  render(){

    return(
        <nav className={"navbar navbar-expand-lg fixed-top" + (this.props.color ? ' color nopadding' : '')} id="landingNav">
        <div className="container nav-container">
          { !this.props.color ? (
            <div id="navDivCallButton">
              <button type="button" id="navCallButton" data-toggle="modal" data-target="#scheduleCallBackModal">
                Schedule a call-back
              </button>
            </div>
          ) : null }
          
          <Link className="navbar-brand" to="/">
            <img src={arbi_logo} />
            <img src={arbi_logo_white} className="white" />
          </Link>
          <button className="navbar-toggler collapsed" type="button"
                  data-toggle="collapse" data-target="#navbarSupportedContent"
                  aria-controls="navbarSupportedContent" aria-expanded="false"
                  aria-label="Toggle navigation">
            <span><i className="fa fa-times" aria-hidden="true"></i></span>
            <span><i className="fa fa-bars" aria-hidden="true"></i></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              { /* <li className="nav-item active">
                <HashLink className="nav-link" to="/#logos-container">
                  Features
                  <span className="sr-only">(current)</span>
                </HashLink>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">Pricing</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">Blog</a>
              </li>
              <li className="nav-item">
                <HashLink to="/#contact" className="nav-link">Contact</HashLink>
              </li> */ }
            </ul>
            <div id="loginBtnDiv">
              <a id="login-btn" href='/login' type="submit">Login</a>
            </div>
          </div>
          </div>
        </nav>
    )
  }
}

export default Header;
