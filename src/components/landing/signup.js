import React from 'react';
import Header from './sections/header';
import Footer from './sections/footer';
import { connect } from 'react-redux';

import done from '../../sources/img/done.png';
import confirm from '../../sources/img/confirm.png';
import user from '../../sources/img/user.png';
import line from '../../sources/img/line.jpg';
import done_green from '../../sources/img/done_green.png';

import { store } from '../../index';
import { push } from 'react-router-redux';

import { NotificationManager } from 'react-notifications';

import * as authorizationAction from '../../actions/authorizationAction';
import * as paymentAction from '../../actions/paymentAction';
import * as messageAction from '../../actions/messageAction';
import * as mailAction from '../../actions/mailAction';

import emailConfirm from '../public/mails/emailConfirm';

const plans = {
  [2]: {
    name: 'Starter Plan',
    price: 59
  },
  [3]: {
    name: 'Plus Plan',
    price: 149
  },
  [4]: {
    name: 'Pro Plan',
    price: 279
  }
}

const paymentTypes = [
  'ideal',
  'creditcard',
  'mistercash',
  'sofort',
  'banktransfer',
  'directdebit',
  'belfius',
  'kbc',
  'bitcoin',
  'paypal',
  'paysafecard'
];

class SignUp extends React.Component {

  state = {
    user: {
      FirstName: '',
      LastName: '',
      Email: '',
      Phone: ''
    },
    confirm: false,
    paymentType: 'creditcard',
    countOfMonth: 3
  }

  async componentDidMount(){
    window.scroll(0, 0);

    if([2,3,4].indexOf(Number(this.props.match.params.planId)) < 0){
      store.dispatch(push('/'));
      return;
    }

    console.log(this.props.match.params.planId, plans);
  }

  plan(){
    return plans[this.props.match.params.planId];
  }

  async onCreateAccout(){
    try{
      if(this.state.user.FirstName === '') throw new Error('First name is required field');
      if(this.state.user.LastName === '') throw new Error('Last name is required field');
      if(this.state.user.Email === '') throw new Error('Email is required field');

      let response = await authorizationAction.registration(this.state.user);

      console.log('authorizationAction.registration', response);

      await mailAction.afterFeedback(this.state.user.Email, emailConfirm(this.state.user.FirstName, window.location.origin + '/emailconfirmed/' + this.state.user.Email));

      response = await paymentAction.setPaymentPlan(
        this.state.user.Email,
        Number(this.props.match.params.planId), 
        this.state.countOfMonth, 
        {
          PaymentSystem: this.state.paymentType,
          Amount: this.state.countOfMonth * this.plan().price,
          RedirectTo: window.location.origin + '/login'
        }
      )

      window.location = response;

      console.log('paymentAction.setPaymentPlan', response);
    } catch(e) {
      NotificationManager.error(e.message ? e.message : 'Error');
      console.log(e);
    }
  }

  render(){

    const paymentOptions = paymentTypes.map((type, index) => {
      return (
        <option value={type} key={index}>{type}</option>
      )
    })

    return(
      <div>
        <Header color={true} />
        <div id="signupForm">
        
          <div className="modal-header">
              <div className="text-center">
                  <img src={user} alt="" />
                  <p className="mt-2">Summary & customer info</p>
              </div>
              <div className="lineIcon pr-3">
                  <img src={line} alt="" />
              </div>
              <div className="text-center">
                  <img src={confirm} alt="" />
                  <p className="mt-2">Make payment</p>
              </div>
              <div className="lineIcon pl-3">
                  <img src={line} alt="" />
              </div>
              <div className="text-center">
                  <img src={done} alt="" />
                  <p className="mt-2">Sign-up confirmation</p>
              </div>
          </div>

          <div className="blocks"> 
          
            <div className="modal-body summary">
              <div className="checkoutBody" style={{ textAlign: 'left' }}>
                <h4>Summary</h4>
                <p className="checkoutTitle"><strong>{ this.plan().name }<span>Price</span></strong></p>
                <label className="checkoutP">
                  <input 
                    type="radio" 
                    checked={this.state.countOfMonth === 1} 
                    onChange={() => this.setState({ countOfMonth: 1 })} />
                    &nbsp;1-month payment<span>€ { this.plan().price }</span>
                </label>
                <label className="checkoutP">
                  <input 
                    type="radio" 
                    checked={this.state.countOfMonth === 3} 
                    onChange={() => this.setState({ countOfMonth: 3 })} />
                    &nbsp;3-month payment<span>€ { this.plan().price * 3 }</span>
                </label>
                <label className="checkoutP">
                  <input 
                    type="radio" 
                    checked={this.state.countOfMonth === 12} 
                    onChange={() => this.setState({ countOfMonth: 12 })} />
                  &nbsp;12-month payment<span>€ { this.plan().price * 12 }</span>
                </label>
                <p className="checkoutTotal">Total<span>€ { this.plan().price * this.state.countOfMonth }</span></p>

                <div className="row">
                  <div className="input-group col-6">
                    <select 
                      className="form-control" 
                      value={this.state.paymentType}
                      onChange={e => this.setState({ paymentType: e.target.value })} >
                      <option value="" disabled>Select payment</option>
                      { paymentOptions }
                    </select>
                  </div>
                </div>
                
              </div>

            </div>

            <div className="modal-body register">
              <h4>Register for an account</h4>

              <div className="form-container">
                <div className="col-md-12">
                  <div className="input-group">
                    <input 
                      type="text" 
                      className="form-control" 
                      placeholder="First name"
                      value={this.state.user.FirstName}
                      onChange={e => this.setState({ user: { ...this.state.user, FirstName: e.target.value } })} />
                    <a className="trialIcon" href="" tabIndex="-1">
                      <span><i className="fa fa-check" aria-hidden="true"></i></span>
                    </a>
                  </div>
                </div>
                <div className="col-md-12">
                  <div className="input-group">
                    <input 
                      type="text" 
                      className="form-control" 
                      placeholder="Last name"
                      value={this.state.user.LastName}
                      onChange={e => this.setState({ user: { ...this.state.user, LastName: e.target.value } })} />
                    <a className="trialIcon" href="" tabIndex="-1">
                      <span><i className="fa fa-check" aria-hidden="true"></i></span>
                    </a>
                  </div>
                </div>
                <div className="col-md-12">
                  <div className="input-group">
                    <input 
                      type="text" 
                      className="form-control" 
                      placeholder="Email"
                      value={this.state.user.Email}
                      onChange={e => this.setState({ user: { ...this.state.user, Email: e.target.value } })} />
                    <a className="trialIcon" href="" tabIndex="-1">
                      <span><i className="fa fa-envelope" aria-hidden="true"></i></span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            
            <div className="modal-body confirm">
              <h4>Confirm</h4>

              <div className="col-md-8">

                <div className="form-check">
                  <label className="form-check-label">
                    <input 
                    className="form-check-input" 
                      type="checkbox" 
                      checked={this.state.confirm}
                      onChange={() => this.setState({ confirm: !this.state.confirm })} />
                    &nbsp;I accept the <a href="">Terms & Conditions</a>
                  </label>
                </div>

                <button 
                  disabled={!this.state.confirm} 
                  type="button" 
                  className="btn btn-block trial-button"
                  onClick={this.onCreateAccout.bind(this)} >
                  <i className="fa fa-paper-plane-o" aria-hidden="true"></i>
                  Create account
                </button>
              </div>
            </div>

          </div>

        </div>
        <Footer />
      </div>
    )
  }
}
  

export default connect(state => ({ paymentPlans: state.paymentPlans }))(SignUp);