import React from 'react';
import Header from './sections/header';
import Footer from './sections/footer';

import done_green from '../../sources/img/done_green.png';

import { store } from '../../index';
import { push } from 'react-router-redux';

import { NotificationManager } from 'react-notifications';

import * as authorizationAction from '../../actions/authorizationAction';

class EmailConfirmed extends React.Component {

  state = {
    Email: this.props.match.params.email,
    Password: "",
    PasswordConfirm: ""
  }

  componentDidMount(){
    console.log(this.props)
  }

  async onConfirm(){
    try{
      if(this.state.Password === '' || this.state.Password !== this.state.PasswordConfirm) throw new Error('Password error');

      let response = await authorizationAction.confirmPassword(this.state);

      //console.log(response);

      store.dispatch(push('/login'));
      NotificationManager.success("Sign in with your email and password");
    } catch(e) {
      console.log(e)
      NotificationManager.error(e || 'Error')
    }
  }

  render(){
    return(
      <div>
        <Header color={true}/>
        <div id="EmailConfirmed">
          <div className="blocks">
            <div className="password">

              <div className="success">
                <img src={done_green} alt="" />
                <h5>Email confirmed. Please enter your password.</h5>
              </div>

              <div className="form-container">
                <div className="col-md-12">
                  <div className="input-group">
                    <input 
                      type="password" 
                      className="form-control" 
                      placeholder="Password"
                      value={this.state.Password}
                      onChange={e => this.setState({ Password: e.target.value })} />
                    <a className="trialIcon" href="" tabIndex="-1">
                      <span><i className="fa fa-key" aria-hidden="true"></i></span>
                    </a>
                  </div>
                </div>
                <div className="col-md-12">
                  <div className="input-group">
                    <input 
                      type="password" 
                      className="form-control" 
                      placeholder="Confirm password"
                      value={this.state.PasswordConfirm}
                      onChange={e => this.setState({ PasswordConfirm: e.target.value })} />
                    <a className="trialIcon" href="" tabIndex="-1">
                      <span><i className="fa fa-key" aria-hidden="true"></i></span>
                    </a>
                  </div>
                </div>
                <div className="col-md-12 modalCallButton">
                  <button type="button" className="btn btn-block callBackModalBtn py-2"
                        onClick={this.onConfirm.bind(this)}>
                    <i className="fa fa-check" aria-hidden="true"></i>
                    Confirm password
                  </button>
                </div>
              </div>

            </div>
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

export default EmailConfirmed;
