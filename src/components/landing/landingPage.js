import React from 'react';
import Header from './sections/header';
import Landing from './sections/landing';
import Footer from './sections/footer';


class LandingPage extends React.Component {
  render(){
    return(
      <div>
        <Header />
        <Landing />
        <Footer />
      </div>
    )
  }
}
  

export default LandingPage;