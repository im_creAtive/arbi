import React from 'react';
import Header from './sections/header';
import Login from './sections/login';
import Footer from './sections/footer';

class LoginPage extends React.Component {
  render(){
    return(
      <div>
        <Header enabledShowMenu={true}/>
        <Login />
        <Footer />
      </div>
    )
  }
}

export default LoginPage;
