import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'
import dateFormat from 'dateformat'
import { Link } from 'react-router-dom'
import { NotificationManager } from 'react-notifications'
import { withRouter } from 'react-router'

import * as knowledgeActions from '../../../actions/knowledgeAction'

import Header from './header'

class KnowledgePost extends Component {

  async componentDidMount() {
    window.scroll(0, 0)
    try {
      let post = await knowledgeActions.getById(this.props.match.params.id);
      if (!post) throw new Error('Post not found. Select an another knowledge post.')

      this.setState({ ...post })
    } catch (e) {
      console.log(e);
      NotificationManager.error(e.message)
      this.props.history.push('/support')
    }
  }

  render() {

    console.log(this.props)

    return (
      <div className={css(styles.wrapper)}>
        <Header />
        <div className={css(styles.postContainer)}>
          {this.state && (
            <div>
              <h2 className={css(styles.title)}>{this.state.TitleOfArticle}</h2>
              <p className={css(styles.desc)} dangerouslySetInnerHTML={{ __html: this.state.ArticleText.replace(/(?:\r\n|\r|\n)/g, '<br />') }}></p>
            </div>
          )}
        </div>
      </div>
    )
  }

}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column'
  },

  postContainer: {
    display: 'flex',
    background: '#E8E9ED',
    flex: 1,
    justifyContent: 'center',
    ':nth-child(1n) > div': {
      display: 'block',
      maxWidth: '1000px',
      width: '100%',
      margin: 5,
      padding: 50,
      background: '#fff',
      boxShadow: '0px 0px 27px 0px rgba(87,29,75,0.87)',
      '@media (max-width: 750px)': {
        padding: 15
      }
    }
  },

  title: {
    textAlign: 'center',
    padding: '50px 20%',
    wordBreak: 'break-word',
    color: '#2B2B58',
    '@media (max-width: 750px)': {
      padding: '50px 15px',
    },
    '@media (max-width: 400px)': {
      fontSize: 22
    }
  },

  desc: {

  },

  date: {
    textAlign: 'right'
  }
})

export default connect(state => ({ lang: state.lang }))(withRouter(KnowledgePost));