import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { NotificationManager } from 'react-notifications'

import * as ideaActions from '../../../actions/ideaAction'

const defState = {
  UserName: "",
  TitleOfIdea: "",
  TextOfIdea: ""
}

class IdeaModal extends Component{

  state = { ...defState }

  async onSubmit(){
    const form = Object.assign({}, this.state)

    try{
      if (form.UserName === '') throw new Error('User name is required field')
      if (form.TitleOfIdea === '') throw new Error('Title is required field')
      if (form.TextOfIdea === '') throw new Error('Description is required field')

      let post = await ideaActions.add(form);

      NotificationManager.success("Your idea successfully created")
      window.$('#ideaModal').modal('hide')
      this.setState({ ...defState }, () => {
        this.props.history.push('/support/idea/' + post.Id)
      })
    } catch(e) {
      NotificationManager.error(e.message)
    }
  }

  render(){
    return (
      <div className="modal fade" id="ideaModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">New idea</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="form-group">
                <label>User name</label>
                <input 
                  className="form-control"
                  value={this.state.UserName}
                  onChange={e => this.setState({ UserName: e.target.value })} />
              </div>
              <div className="form-group">
                <label>Title</label>
                <input
                  className="form-control"
                  value={this.state.TitleOfIdea}
                  onChange={e => this.setState({ TitleOfIdea: e.target.value })} />
              </div>
              <div className="form-group">
                <label>Description</label>
                <textarea
                  className="form-control"
                  value={this.state.TextOfIdea}
                  onChange={e => this.setState({ TextOfIdea: e.target.value })}></textarea>
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary" style={{ background: '#581e4c', borderColor: '#290c23' }} onClick={() => this.onSubmit()}>Add</button>
            </div>
          </div>
        </div>
      </div>
    )
  }

}

export default withRouter(IdeaModal)