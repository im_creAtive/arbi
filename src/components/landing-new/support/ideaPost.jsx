import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'
import dateFormat from 'dateformat'
import { Link } from 'react-router-dom'
import { NotificationManager } from 'react-notifications'
import { withRouter } from 'react-router'

import * as ideaActions from '../../../actions/ideaAction'
import * as ideaCommentsActions from '../../../actions/ideaCommentsAction'

import Header from './header'
import noUser from '../../../sources/img/nouser.jpg'

const defForm = {
  UserName: "",
  TextOfComments: ""
}

class IdeaPost extends Component {

  state = {
    comments: [],
    form: { ...defForm }
  }

  async componentDidMount() {
    window.scroll(0, 0)
    try {
      let post = await ideaActions.getById(this.props.match.params.id);
      if (!post) throw new Error('Post not found. Select an another idea post.')

      let comments = await ideaCommentsActions.getByIdeaId(post.Id);

      this.setState({ ...post, form: { ...defForm }, comments })
    } catch (e) {
      console.log(e);
      NotificationManager.error(e.message)
      this.props.history.push('/support')
    }
  }

  onChangeForm(field, value){
    this.setState({
      form: {
        ...this.state.form,
        [field]: value
      }
    })
  }

  async onSubmit(e){
    e.preventDefault();
    const form = Object.assign({}, this.state.form)

    try{
      if (form.UserName === '') throw new Error('User name is required field');
      if (form.TextOfComments === '') throw new Error('Comment is required field');

      console.log(form, this.state.Id);

      await ideaCommentsActions.add(form, this.state.Id)
      let comments = await ideaCommentsActions.getByIdeaId(this.state.Id);

      this.setState({ comments, form: { ...defForm } })
      NotificationManager.success("Your comment successfully created")
    } catch(e) {
      NotificationManager.error(e.message)
    }
  }

  render() {

    const comments = this.state.comments.map((comment, index) => {
      return (
        <div className={css(styles.comment)} key={index}>
          <img src={noUser} alt="avatar" />
          <div>
            <strong>{comment.UserName}</strong>
            <p>{comment.TextOfComments}</p>
          </div>
        </div>
      )
    })

    return (
      <div className={css(styles.wrapper)}>
        <Header />
        <div className={css(styles.postContainer)}>
          {this.state.Id && (
            <div>
              <h2 className={css(styles.title)}>{this.state.TitleOfIdea}</h2>
              <p className={css(styles.desc)} dangerouslySetInnerHTML={{ __html: this.state.TextOfIdea.replace(/(?:\r\n|\r|\n)/g, '<br />') }}></p>
              <p className={css(styles.date)}>By {this.state.UserName}</p>
              <hr />
              <h4>{this.state.comments.length} comment(s)</h4>
              <form onSubmit={e => this.onSubmit(e)} className={css(styles.form)}>
                <div className="form-group" style={{ maxWidth: 300 }}>
                  <label>User name</label>
                  <input
                    value={this.state.form.UserName}
                    onChange={e => this.onChangeForm('UserName', e.target.value)}
                    className="form-control" />
                </div>
                <div className="form-group">
                  <label>Comment</label>
                  <textarea 
                    value={this.state.form.TextOfComments}
                    onChange={e => this.onChangeForm('TextOfComments', e.target.value)}
                    className="form-control"></textarea>
                </div>
                <button className={css(styles.addBtn) + ' btn'}>Add comment</button>
              </form>

              { comments }
            </div>
          )}
        </div>
      </div>
    )
  }

}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column'
  },

  postContainer: {
    display: 'flex',
    background: '#E8E9ED',
    flex: 1,
    justifyContent: 'center',
    ':nth-child(1n) > div': {
      display: 'block',
      maxWidth: '1000px',
      width: '100%',
      margin: 5,
      padding: 50,
      background: '#fff',
      boxShadow: '0px 0px 27px 0px rgba(87,29,75,0.87)',
      '@media (max-width: 750px)': {
        padding: 15
      }
    }
  },

  title: {
    textAlign: 'center',
    padding: '50px 20%',
    wordBreak: 'break-word',
    color: '#2B2B58',
    '@media (max-width: 750px)': {
      padding: '50px 15px',
    },
    '@media (max-width: 400px)': {
      fontSize: 22
    }
  },

  desc: {

  },

  date: {
    textAlign: 'right'
  },

  form: {
    border: '1px solid #ccc',
    borderRadius: 5,
    padding: 15,
    margin: '15px 0'
  },

  addBtn: {
    background: '#581e4c',
    color: '#fff',
    cursor: 'pointer',
    ':hover': {
      background: '#38386e'
    }
  },

  comment: {
    padding: 10,
    display: 'flex',
    marginBottom: 10,
    wordBreak: 'break-word',
    ':nth-child(1n) > img': {
      width: 30,
      height: 30
    },
    ':nth-child(1n) > div': {
      flex: 1,
      paddingLeft: 10
    }
  }
})

export default connect(state => ({ lang: state.lang }))(withRouter(IdeaPost));