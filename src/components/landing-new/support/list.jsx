import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'
import dateFormat from 'dateformat'
import { Link } from 'react-router-dom'
import { NotificationManager } from 'react-notifications'
import { withRouter } from 'react-router'

import * as ideaActions from '../../../actions/ideaAction'
import * as knowledgeActions from '../../../actions/knowledgeAction'

import Header from './header'
import IdeaModal from './ideaModal'

class List extends Component {

  async componentDidMount() {
    window.scroll(0, 0)
    ideaActions.get()
    knowledgeActions.get()
  }

  render() {

    const ideas = this.props.idea.list.map((idea, index) => {
      return (
        <div className={css(styles.idea)} key={index}>
          <Link to={'/support/idea/' + idea.Id} className={css(styles.ideaTitle)}><i className="fa fa-arrow-circle-right" aria-hidden="true"></i> {idea.TitleOfIdea}</Link>
          <p className={css(styles.ideaDesc)}>By {idea.UserName}</p>
        </div>
      )
    })

    const knowledges = this.props.knowledge.list.map((knowledge, index) => {
      return (
        <div className={css(styles.knowledge)} key={index}>
          <Link to={'/support/knowledge/' + knowledge.Id} className={css(styles.knowledgeTitle)}><i className="fa fa-file-text" aria-hidden="true"></i> {knowledge.TitleOfArticle}</Link>
        </div>
      )
    })

    return (
      <div className={css(styles.wrapper)}>
        <Header />
        <div className={css(styles.main)}>
          <div className={css(styles.ideas)}>
            <h4 className={css(styles.ideasTiile)}><i className="fa fa-lightbulb-o" aria-hidden="true"></i> Features & ideas</h4>
            {ideas.length > 0 ? (
              <div className={css(styles.ideasList)}>
                {ideas}
              </div>
            ) : (
              <div className={css(styles.ideasListEmpty)}>
                  <p><i className="fa fa-info-circle" aria-hidden="true"></i> Ideas list is empty</p>
              </div>
            )}
            <div>
              <button data-toggle="modal" data-target="#ideaModal" className={css(styles.postIdea)}>Post a new idea <i className="fa fa-long-arrow-right" aria-hidden="true"></i></button>
            </div>
            <IdeaModal />
          </div>
          <div>
            <h4 className={css(styles.knowledgesTitle)}><i className="fa fa-info-circle" aria-hidden="true"></i> Knowledge base</h4>
            {knowledges.length > 0 ? (
              <div className={css(styles.knowledgeList)}>
                {knowledges}
              </div>
            ) : (
              <div className={css(styles.knowledgeListEmpty)}>
                <p><i className="fa fa-info-circle" aria-hidden="true"></i> Knowledge list is empty</p>
              </div>
            )}
          </div>
        </div>
      </div>
    )
  }

}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column'
  },

  main: {
    maxWidth: '1000px',
    width: '100%',
    display: 'flex',
    margin: '50px auto',
    flexDirection: 'column',
    '@media (max-width: 1030px)': {
      padding: '0 15px'
    }
  },

  ideas: {
    display: 'flex',
    padding: 25,
    background: '#fff',
    border: '1px solid #ccc',
    borderRadius: 5,
    flexDirection: 'column'
  },

  ideasTiile:{
    marginBottom: 15,
  },  

  ideasList: {
    display: 'flex',
    flexWrap: 'wrap',
  },

  ideasListEmpty: {
    textAlign: 'center',
    padding: '50px 0',
    fontSize: 20,
    color: '#8a8a8a'
  },

  idea: {
    width: 'calc(31%)',
    margin: '5px 1%',
    wordBreak: 'break-all',
    fontSize: 14,
    '@media (max-width: 850px)': {
      width: 'calc(48%)',
    },
    '@media (max-width: 550px)': {
      margin: '5px 0',
      width: '100%'
    }
  },

  ideaTitle: {
    color: '#001e99',
    fontWeight: 'bold',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    width: '100%',
    display: 'block'
  },

  ideaDesc: {
    maxHeight: 60,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    wordBreak: 'initial'
  },

  postIdea: {
    background: '#581e4c',
    color: '#fff',
    border: 0,
    padding: '5px 20px',
    borderRadius: 30,
    cursor: 'pointer',
    ':hover': {
      background: '#38386e'
    }
  },

  knowledgesTitle: {
    margin: '20px 0'
  },

  knowledgeListEmpty: {
    textAlign: 'center',
    padding: '50px 0',
    fontSize: 20,
    color: '#8a8a8a'
  },

  knowledgeList: {
    display: 'flex',
    flexWrap: 'wrap',
  },

  knowledge: {
    width: 'calc(50% - 15px)',
    marginRight: 15,
    marginBottom: 15,
    ':nth-child(2n)': {
      marginRight: 0
    },
    '@media (max-width: 620px)': {
      width: 'calc(100% - 15px)',
    }
  },

  knowledgeTitle: {
    color: '#001e99',
    wordBreak: 'break-all',
  }
})

export default connect(state => ({ lang: state.lang, knowledge: state.knowledge, idea: state.idea }))(withRouter(List));