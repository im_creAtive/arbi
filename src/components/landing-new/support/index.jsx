import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'
import { Switch, Route, Redirect } from 'react-router'

// components
import Topnav from '../restaurant/topnav'
import Footer from '../home/footer'
import List from './list'
import KnowledgePost from './knowledgePost'
import IdeaPost from './ideaPost'

class Support extends Component {

  componentDidMount() {
    window.scroll(0, 0)
  }

  render() {
    return (
      <div className={css(styles.wrapper)}>
        <Topnav />
        <Switch>
          <Route exact path="/support" component={List} />
          <Route path="/support/knowledge/:id" component={KnowledgePost} />
          <Route path="/support/idea/:id" component={IdeaPost} />
        </Switch>
        <Footer />
      </div>
    )
  }

}

const styles = StyleSheet.create({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh'
  }

})

export default connect(state => ({ lang: state.lang, knowledge: state.knowledge, idea: state.idea }))(Support);