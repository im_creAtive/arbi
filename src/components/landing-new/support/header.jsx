import React from 'react'
import { css, StyleSheet } from 'aphrodite'


import headerBg from '../../../sources/landing/callback-bg.jpg'

export default function (props) {
  return <div className={css(styles.header)}></div>
}

const styles = StyleSheet.create({
  header: {
    display: 'block',
    width: '100%',
    height: '115px',
    background: 'url(' + headerBg + ')',
    backgroundSize: 'cover',
    backgroundPosition: 'bottom'
  }
})