import React from 'react'
import { css, StyleSheet } from 'aphrodite'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'

import headerBg from '../../../sources/landing/corp/header.jpg'
import answersLeftBg from '../../../sources/landing/corp/answers-left-bg.png'
import answersRightBg from '../../../sources/landing/corp/answers-right-bg.png'
import discussionIcon from '../../../sources/landing/corp/answers-discussion-icon.png'
import conceptIcon from '../../../sources/landing/corp/answers-concept-icon.png'
import devIcon from '../../../sources/landing/corp/answers-dev-icon.png'

const Answers = (props) => {
  return (
    <div>
      <h3 className={css(styles.questions)}>
        {translate.question1}<br />
        {translate.question2}<br />
        {translate.question3}<br />
        {translate.question4}<br />
        {translate.question5}
      </h3>
      <div className={css(styles.line)}></div>
      <h2 className={css(styles.answers)}>{translate.weHere}</h2>

      <div className={css(styles.lnb)}>
        <span className={css(styles.firstLine)}></span>
        <span className={css(styles.firstBox)}></span>
        
        <div className={css(styles.secondTitle)}>
          <strong>{translate.outcome}</strong>
          <p>
            {translate.discussionOutcome}
          </p>
        </div>
        <span className={css(styles.secondBox)}></span>
        <span className={css(styles.secondLine)}></span>

        <div className={css(styles.thirdTitle)}>
          <strong>{translate.outcome}</strong>
          <p>
            {translate.proofOutcome}
          </p>
        </div>
        <span className={css(styles.thirdBox)}></span>
        <span className={css(styles.thirdLine)}></span>

        <div className={css(styles.fourTitle)}>
          <strong>{translate.outcome}</strong>
          <p>
            {translate.developOutcome}
          </p>
        </div>
        <span className={css(styles.fourBox)}></span>
        <span className={css(styles.fourLine)}></span>
      </div>

      <div className={css(styles.blocks)}>
        <div>
          <h4>{translate.discussion}</h4>
          <div className={css(styles.answer)}>
            <div>
              <img src={discussionIcon} alt="answer" />
              <p>
                {translate.discussionDesc}
              </p>
            </div>
          </div>
        </div>
        <div>
          <h4>{translate.proof}</h4>
          <div className={css(styles.answer)}>
            <div>
              <img src={conceptIcon} alt="answer" />
              <p>
                {translate.proofDesc}
              </p>
            </div>
          </div>
        </div>
        <div>
          <h4>{translate.develop}</h4>
          <div className={css(styles.answer)}>
            <div>
              <img src={devIcon} alt="answer" />
              <p>
                {translate.developDesc}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const translate = new MultiLanguage({
  en: {
    question1: 'How does Augmented Reality work?',
    question2: 'What is the value of it? Does is suit my business?',
    question3: 'How is it implemented? Can we track its perfomance?',
    question4: 'Can we engage consumers with it?',
    question5: 'How to pilot or validate user cases?',
    weHere: 'We here to take your hand and find out the ansers together.',
    discussion: 'Discussion & Brainstorming',
    discussionDesc: 'Together with key stakeholders we spend half a day to two days explaining AR, its applications , best practices and developing actionable user cases within your company.',
    outcome: 'Outcome',
    discussionOutcome: 'A workshop can deliver insight into whether Augmented Reality is a good fit and which user cases or challenges to tackle.',
    proof: 'Proof of concept',
    proofDesc: 'We either develop a proof of concept of start experimenting with the Arbi CMS or one of our-tailored solutions to gather data and feedback.',
    proofOutcome: 'Gathering valuable feedback and further customizing and customer value proposition for the final product.',
    develop: 'Application development',
    developDesc: 'In the final stage we can develop a full application with required features, data-integration, tracking & performance measurement.',
    developOutcome: 'A scalable AR solution that delivers real value for the customer, fully managed and hosted by us.',
  },
  nl: {
    question1: 'Hoe werkt Augmented Reality?',
    question2: 'Waar zit de waarde? Is het geschikt voor mijn business?',
    question3: 'Hoe wordt het geimplementeerd? Kunnen we de prestaties meten?',
    question4: 'Kunnen we consumenten ermee engagen?',
    question5: 'Hoe kunnen we een pilot draaien of user cases valideren?',
    weHere: 'Wij zijn er om samen hand-in-hand de antwoorden te vinden.',
    discussion: 'Discussie & Brainstorming',
    discussionDesc: 'Samen met de belangrijkste stakeholders spenderen we een halve dag tot twee dagen met het uitleggen van AR, het verkennen van toepassingen, best practices en het ontwikkelen van concrete user cases binnen jouw organisatie.',
    outcome: 'Resultaat',
    discussionOutcome: 'De workshop levert duidelijk inzicht in het gebruik van AR en of er een goede waardepropositie ligt om ermee door te gaan.',
    proof: 'Proof of Concept',
    proofDesc: 'We ontwikkelen een Proof of Concept of gaan experimenteren met de Arbi CMS om data en feedback te verzamelen. ',
    proofOutcome: 'Het verzamelen van waardevolle feedback om de waardepropositie en product duidelijk vorm te geven.',
    develop: 'Ontwikkeling applicatie',
    developDesc: 'In de laatste fase ontwikkelen we de applicatie door tot de market-ready vorm met alle features, data-integratie, tracking en prestatiemanagement.',
    developOutcome: 'Een schaalbare AR oplossing die waarde voor de eindgebruiker levert, volledig beheerd door ons.',
  }
})

const styles = StyleSheet.create({
  questions: {
    fontSize: 24,
    textAlign: 'center',
    marginTop: 100,
    lineHeight: 1.4,
    '@media (max-width: 880px)': {
      paddingLeft: 15,
      paddingRight: 15
    }
  },

  line: {
    margin: '75px auto',
    width: 800,
    borderBottom: '2px solid #380056',
    '@media (max-width: 880px)': {
      width: 'calc(100% - 30px)',
      marginLeft: 15,
      marginRight: 15
    }
  },

  answers: {
    width: 800,
    margin: '0 auto',
    textAlign: 'center',
    marginBottom: 15,
    '@media (max-width: 880px)': {
      width: 'calc(100% - 30px)',
      marginLeft: 15,
      marginRight: 15
    },
    '@media (max-width: 420px)': {
      fontSize: 25
    }
  },

  blocks: {
    ':nth-child(1n) > div': {
      width: 'calc(50% + 100px)',
      background: 'url('+ answersLeftBg +')',
      backgroundSize: 'cover',
      height: 500,
      margin: '200px 0',
      borderTopRightRadius: 10,
      borderBottomRightRadius: 10,
      position: 'relative',
      ':nth-child(1n) > h4': {
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'right',
        padding: '50px 100px',
        fontSize: 24,
        '@media (max-width: 1000px)': {
          padding: '30px',
          textAlign: 'center',
        }
      },
      '@media (max-width: 1200px)': {
        width: 'calc(40% + 100px)',
      },
      '@media (max-width: 1000px)': {
        width: '100%',
        borderRadius: 10,
        height: 600,
        margin: '300px 0',
      }
    },
    ':nth-child(1n) > div:nth-child(2n)': {
      background: 'url('+ answersRightBg +')',
      borderTopRightRadius: 0,
      borderBottomRightRadius: 0,
      borderTopLeftRadius: 10,
      borderBottomLeftRadius: 10,
      marginLeft: 'calc(50% - 100px)',
      ':nth-child(1n) > h4': {
        textAlign: 'left',
        '@media (max-width: 1000px)': {
          padding: '30px',
          textAlign: 'center',
        }
      },
      ':nth-child(1n) > div': {
        right: 'auto',
        left: -100,
        '@media (max-width: 1000px)': {
          left: 'auto',
          right: 'auto',
        }
      },
      '@media (max-width: 1200px)': {
        marginLeft: 'calc(60% - 100px)',
      },
      '@media (max-width: 1000px)': {
        width: '100%',
        borderRadius: 10,
        marginLeft: 0
      }
    },
    ':nth-child(1n) > div:nth-child(1)': {
      '@media (max-width: 1000px)': {
        marginTop: 150
      }
    }
  },

  answer: {
    background: '#380056',
    position: 'absolute',
    right: -100,
    top: 120,
    width: 600,
    borderRadius: 10,
    height: 450,
    display: 'flex',
    justifyContent: 'center',
    ':nth-child(1n) > div': {
      alignSelf: 'center',
      textAlign: 'center',
      ':nth-child(1n) > p': {
        marginTop: 50,
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
        width: 470,
        '@media (max-width: 550px)': {
          width: '100%'
        },
        '@media (max-width: 420px)': {
          fontSize: 16
        },
        '@media (max-width: 350px)': {
          fontSize: 14
        }
      },
      '@media (max-width: 550px)': {
        paddingLeft: 15,
        paddingRight: 15,
        width: 'calc(100% - 30px)'
      }
    },
    '@media (max-width: 1000px)': {
      right: 'auto',
      left: 'auto',
      top: 90,
      marginLeft: 15,
      marginRight: 15,
      width: 'calc(100% - 30px)'
    }
  },

  lnb: {
    position: 'relative',
  },

  firstLine: {
    display: 'block',
    width: 0,
    height: 100,
    borderLeft: '4px dashed #380056',
    marginLeft: 'calc(50% - 2px)',
    position: 'absolute',
    top: 0,
    '@media (max-width: 1000px)': {
      height: 150
    }
  },

  firstBox: {
    display: 'block',
    width: 200,
    height: 100,
    borderLeft: '4px dashed #380056',
    borderTop: '4px dashed #380056',
    marginLeft: 'calc(50% - 200px)',
    position: 'absolute',
    top: 100,
    '@media (max-width: 1000px)': {
      display: 'none',
    }
  },

  secondBox: {
    display: 'block',
    width: 200,
    height: 200,
    borderRight: '4px dashed #380056',
    borderTop: '4px dashed #380056',
    marginLeft: 'calc(50% + 200px)',
    position: 'absolute',
    top: 690,
    '@media (max-width: 1200px)': {
      marginLeft: 'calc(40% + 200px)',
    },
    '@media (max-width: 1000px)': {
      display: 'none',
    }
  },
  secondTitle: {
    position: 'absolute',
    width: 300,
    top: 530,
    textAlign: 'center',
    left: 'calc(50% + 250px)',
    fontSize: 14,
    fontWeight: 'bold',
    ':nth-child(1n) > strong': {
      fontSize: 20,
      marginBottom: 20,
      display: 'inline-block',
    },
    '@media (max-width: 1200px)': {
      left: 'calc(40% + 250px)',
    },
    '@media (max-width: 1000px)': {
      left: 'calc(50% - 150px)',
      top: 760
    }
  },
  secondLine: {
    display: 'none',
    width: 0,
    height: 100,
    borderLeft: '4px dashed #380056',
    marginLeft: 'calc(50% - 2px)',
    position: 'absolute',
    top: 910,
    '@media (max-width: 1000px)': {
      display: 'block',
    }
  },

  thirdBox: {
    display: 'block',
    width: 400,
    height: 200,
    borderLeft: '4px dashed #380056',
    borderTop: '4px dashed #380056',
    left: 'calc(50% - 400px)',
    position: 'absolute',
    top: 1390,
    '@media (max-width: 1200px)': {
      left: 'calc(60% - 400px)',
    },
    '@media (max-width: 1000px)': {
      display: 'none',
    }
  },
  thirdTitle: {
    position: 'absolute',
    width: 300,
    top: 1260,
    textAlign: 'center',
    left: 'calc(50% - 550px)',
    fontSize: 14,
    fontWeight: 'bold',
    ':nth-child(1n) > strong': {
      fontSize: 20,
      marginBottom: 20,
      display: 'inline-block',
    },
    '@media (max-width: 1200px)': {
      left: 'calc(60% - 550px)',
    },
    '@media (max-width: 1000px)': {
      left: 'calc(50% - 150px)',
      top: 1660
    }
  },
  thirdLine: {
    display: 'none',
    width: 0,
    height: 100,
    borderLeft: '4px dashed #380056',
    marginLeft: 'calc(50% - 2px)',
    position: 'absolute',
    top: 1800,
    '@media (max-width: 1000px)': {
      display: 'block',
    }
  },

  fourBox: {
    display: 'block',
    width: 200,
    height: 200,
    borderRight: '4px dashed #380056',
    borderTop: '4px dashed #380056',
    marginLeft: 'calc(50% + 200px)',
    position: 'absolute',
    top: 2085,
    '@media (max-width: 1200px)': {
      marginLeft: 'calc(40% + 200px)',
    },
    '@media (max-width: 1000px)': {
      display: 'none',
    }
  },
  fourTitle: {
    position: 'absolute',
    width: 300,
    top: 1950,
    textAlign: 'center',
    left: 'calc(50% + 250px)',
    fontSize: 14,
    fontWeight: 'bold',
    ':nth-child(1n) > strong': {
      fontSize: 20,
      marginBottom: 20,
      display: 'inline-block',
    },
    '@media (max-width: 1200px)': {
      left: 'calc(40% + 250px)',
    },
    '@media (max-width: 1000px)': {
      left: 'calc(50% - 150px)',
      top: 2560
    }
  },
  fourLine: {
    display: 'none',
    width: 0,
    height: 100,
    borderLeft: '4px dashed #380056',
    marginLeft: 'calc(50% - 2px)',
    position: 'absolute',
    top: 2700,
    '@media (max-width: 1000px)': {
      display: 'block',
    }
  },
})

export default connect(state => ({ lang: state.lang }))(Answers);