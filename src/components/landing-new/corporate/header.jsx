import React from 'react'
import { css, StyleSheet } from 'aphrodite'
import { connect } from 'react-redux'
import MultiLang from 'react-redux-multilang'

import headerBg from '../../../sources/landing/corp/header.jpg'


const Header = (props) => {
  return (
    <div className={css(styles.header)}>
      <h1 className={css(styles.title)}>{translate.text1}<br />{translate.text2}</h1>
    </div>
  )
}

const translate = new MultiLang({
  en: {
    text1: 'You got questions on Augmented Reality.',
    text2: 'We got the answers.'
  },
  nl: {
    text1: 'Jij hebt vragen over Augmented Reality.',
    text2: 'Wij hebben de antwoorden.'
  }
})

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: '100vh',
    background: 'url('+ headerBg +')',
    backgroundSize: 'cover',
  },

  title: {
    color: '#fff',
    fontSize: 35,
    textAlign: 'center',
    position: 'relative',
    top: 'calc(50% - 38px)',
    textShadow: '0px 0px 10px rgba(0,0,0,0.2)',
    '@media (max-width: 750px)': {
      paddingLeft: 30,
      paddingRight: 30
    },
    '@media (max-width: 480px)': {
      fontSize: 28
    }
  }
})

export default connect(state => ({ lang: state.lang }))(Header);