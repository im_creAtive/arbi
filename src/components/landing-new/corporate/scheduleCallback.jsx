import React, {Component} from 'react'
import {css, StyleSheet} from 'aphrodite'
import { connect } from 'react-redux'
import { NotificationManager } from 'react-notifications'
import * as messageAction from '../../../actions/messageAction'
import * as mailAction from '../../../actions/mailAction'
import email3 from '../../public/mails/email3'
import landingCallback from '../../public/mails/landingCallback'
import Modal from 'react-modal'
import MultiLanguage from 'react-redux-multilang'

import callbackBg from '../../../sources/landing/callback-bg.jpg'

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const numbersRegex = /^([^0-9]*)$/
const phoneRegex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/

const defaultState = {
  fullname: '',
  company: '',
  phone: '',
  email: '',
  open: false
}

class ScheduleCallback extends Component {

  state = { ...defaultState }

  async onClickCallMeBack(e){
    if(e) e.preventDefault();

    try{
      if(this.state.fullname === '') throw new Error('Fullname is required field.');
      if(this.state.company === '') throw new Error('Company is required field.');
      if(this.state.email === '') throw new Error('Email is required field.');
  
      if(!numbersRegex.test(this.state.fullname)) return NotificationManager.error('Full name field is invalid');
      if(!emailRegex.test(this.state.email)) return NotificationManager.error('Email field is invalid');
      if(this.state.phone !== '' && !phoneRegex.test(this.state.phone)) throw new Error('Phone number field is invalid');

      let userMail = email3(this.state.fullname);
      let adminMail = landingCallback(
        this.state.fullname, 
        this.state.company, 
        this.state.phone, 
        this.state.email);
      await messageAction.admin(adminMail);
      await mailAction.afterFeedback(this.state.email, userMail);

      window.$('#callbackThanks').modal('show');
      setTimeout(() => {
        window.$('#callbackThanks').modal('hide');
      }, 2000);

      this.setState({ ...defaultState })
    } catch(e) {
      NotificationManager.error(e.message || e)
    }
  }

  onRequestOpen(){
    this.setState({ open: true })
  }

  onRequestClose(){
    this.setState({ open: false })
  }

  render () {
    return (
      <span>
        <span onClick={() => this.onRequestOpen()}>{this.props.button || null}</span>

        <Modal
          isOpen={this.state.open}
          onRequestClose={() => this.onRequestClose()}
          contentLabel="Modal"
          style={modalStyle}
          className={{
            base: css(styles.modalBase)
          }}
        >
          <div>
            <h5 className={css(styles.modalTitle)}>{translate.scheduleCallback}</h5>

            <div className={css(styles.inputs)}>
              <input 
                placeholder={translate.fullName}
                value={this.state.fullname}
                onChange={e => this.setState({ fullname: e.target.value })} />
              <input 
                placeholder={translate.company}
                value={this.state.company}
                onChange={e => this.setState({ company: e.target.value })} />
              <input 
                placeholder={translate.email}
                value={this.state.email}
                onChange={e => this.setState({ email: e.target.value })} />
              <input 
                placeholder={translate.phone}
                value={this.state.phone}
                onChange={e => this.setState({ phone: e.target.value })} />
            </div>

            <button 
              onClick={this.onClickCallMeBack.bind(this)}
              className={css(styles.callbackBtn)}>{translate.send}</button>
          </div>
        </Modal>
      </span>
    )
  } 
}

const translate = new MultiLanguage({
  en: {
    fullName: 'Full Name',
    company: 'Company',
    email: 'Email',
    phone: 'Phone',
    send: 'Send callback',
    scheduleCallback: 'Schedule a call-back',
  },
  nl: {
    fullName: 'Voor-en achternaam',
    company: 'Bedrijf',
    email: 'Bedrijfsemail',
    phone: 'Telefoon',
    send: 'Verstuur terugbellen',
    scheduleCallback: 'Maak een terugbelafspraak',
  }
})

const modalStyle = {
  content: {
    background: 'url('+ callbackBg +')',
    backgroundSize: 'cover',
    width: '400px',
    zIndex: 10000,
    position: 'relative',
    left: 'calc(50% - 200px)',
    border: '1px solid rgb(47, 88, 128)',
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    zIndex: 10000,
  }
}

const styles = StyleSheet.create({
  modalBase: {
    padding: 15,
    borderRadius: 10,
    marginTop: 70,
    '@media (max-width: 450px)': {
      width: '300px',
      left: 'calc(50% - 150px)',
    }
  },

  modalContent: {
    background: 'url('+ callbackBg +')',
    backgroundSize: 'cover',
  },

  modalTitle: {
    color: '#fff',
    fontWeight: 'bold',
    marginBottom: 30
  },

  modalHeader: {
    borderBottom: '1px solid #403775'
  },

  inputs: {
    width: '100%',
    display: 'block',
    margin: '0 auto',
    ':nth-child(1n) > input': {
      width: '100%',
      background: 'transparent',
      border: 0,
      color: '#fff',
      fontSize: 18,
      borderBottom: '2px solid #fff',
      marginTop: 50,
      fontWeight: 'bold',
      '::placeholder': {
        color: '#fff',
      },
    },
    ':nth-child(1n) > textarea': {
      width: '100%',
      background: 'transparent',
      border: 0,
      color: '#fff',
      fontSize: 18,
      borderBottom: '2px solid #fff',
      marginTop: 50,
      fontWeight: 'bold',
      '::placeholder': {
        color: '#fff',
      },
      ':focus': {
        outline: 0
      }
    },
    ':nth-child(1n) > input:nth-child(1)': {
      marginTop: 0
    }
  },

  callbackBtn: {
    background: '#b15fc8',
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    padding: '5px 45px',
    borderRadius: 10,
    border: '3px solid #fff',
    margin: '0 auto',
    marginTop: '30px',
    display: 'block',
    cursor: 'pointer',
    ':hover': {
      background: '#cc7ee2'
    }
  }
})


export default connect(state => ({ lang: state.lang }))(ScheduleCallback);