import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'

import TopNav from '../restaurant/topnav'
import Header from './header'
import Answers from './answers'
import Callback from '../restaurant/callback'
import Footer from '../home/footer'


// images
import headerBg from '../../../sources/landing/corp/header.jpg'
import video from '../../../sources/video/arbio_video.mp4'
import playBtn from '../../../sources/landing/play_button.png'


class Corporate extends Component{

  componentDidMount(){
    window.scroll(0, 0)
  }

  render(){
    return (
      <div> 
        <TopNav />
        <Header />
        <Answers />
        <Callback />

        <div className={css(styles.block)}>
          <h3 className={css(styles.title)}>{translate.realCases}</h3>

          <div className={css(styles.cases)}>
            <div>
              <div>
                <button><img src={playBtn} alt="play" /></button>
                <video src={video} />
              </div>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled it to make a type 
                specimen book.
              </p>
            </div>

            <div>
              <div>
                <button><img src={playBtn} alt="play" /></button>
                <video src={video} />
              </div>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled it to make a type 
                specimen book.
              </p>
            </div>

            <div>
              <div>
                <button><img src={playBtn} alt="play" /></button>
                <video src={video} />
              </div>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled it to make a type 
                specimen book.
              </p>
            </div>

            <div>
              <div>
                <button><img src={playBtn} alt="play" /></button>
                <video src={video} />
              </div>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled it to make a type 
                specimen book.
              </p>
            </div>
          </div>
        </div>

        <Footer />
      </div>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    realCases: 'Real cases'
  },
  nl: {
    realCases: 'Voorbeeldcases'
  }
})

const styles = StyleSheet.create({
  block: {
    margin: '75px auto',
  },

  title: {
    textAlign: 'center',
    marginBottom: 75
  },

  cases: {
    display: 'flex',
    flexWrap: 'wrap',
    ':nth-child(1n) > div': {
      width: 'calc(50% - 50px)',
      float: 'left',
      marginRight: 50,
      marginBottom: 50,
      '@media (max-width: 800px)': {
        width: '100%',
        marginRight: 0
      }
    },
    ':nth-child(1n) > div:nth-child(2n)': {
      marginLeft: 50,
      marginRight: 0,
      '@media (max-width: 800px)': {
        marginLeft: 0
      }
    },
    ':nth-child(1n) > div > div': {
      position: 'relative',
      overflow: 'hidden',
    },
    ':nth-child(1n) > div > p': {
      padding: '15px 50px',
      textAlign: 'center',
      fontSize: 12,
      fontWeight: 'bold',
      '@media (max-width: 500px)': {
        padding: '15px 15px',
      }
    },
    ':nth-child(1n) > div > div > button': {
      background: 'transparent',
      border: 0,
      borderRadius: '50%',
      width: 100,
      height: 100,
      position: 'absolute',
      top: 'calc(50% - 50px)',
      left: 'calc(50% - 50px)',
      cursor: 'pointer',
      zIndex: 2,
      ':nth-child(1n) > img': {
        width: 100,
      }
    },
    ':nth-child(1n) > div > div > video': {
      width: '100%',
      background: '#000',
      borderTopRightRadius: 10,
      borderBottomRightRadius: 10,
      '@media (max-width: 800px)': {
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
      }
    },
    ':nth-child(1n) > div:nth-child(2n) > div > video': {
      borderTopRightRadius: 0,
      borderBottomRightRadius: 0,
      borderTopLeftRadius: 10,
      borderBottomLeftRadius: 10,
      '@media (max-width: 800px)': {
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
      }
    },
    '::after': {
      content: "''",
      display: 'block',
      clear: 'both',
    }
  }
})

export default connect(state => ({ lang: state.lang }))(Corporate);
