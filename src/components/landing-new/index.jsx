import React, { Component } from 'react'
import { Switch, Route, Redirect } from 'react-router'
import { setLanguage } from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'
import { connect } from 'react-redux'
import './style.css'

import Home from './home'
import Corporate from './corporate'
import Restaurant from './restaurant'
import Blog from './blog'
import Support from './support'

import enFlag from '../../sources/landing/en.png'
import nlFlag from '../../sources/landing/nl.png'

class Landing extends Component{

  onChangeLanguage(code){
    if(this.props.lang.code !== code){
      setLanguage(code)
      localStorage.setItem('language', code)
    }
  }
  
  render(){
    return(
      <div id="new-landing-wrapper">

        <div className={css(styles.languages)}>
          <img 
            src={enFlag} 
            alt="English flag" 
            className={this.props.lang.code === 'en' ? 'active' : ''}
            onClick={() => this.onChangeLanguage('en')} />
          <img 
            src={nlFlag} 
            alt="Netherland flag" 
            className={this.props.lang.code === 'nl' ? 'active' : ''}
            onClick={() => this.onChangeLanguage('nl')} />
        </div>

        <Switch>
          <Route path="/corporate" component={Corporate} />
          <Route path="/restaurant" component={Restaurant} /> 
          <Route path="/blog" component={Blog} /> 
          <Route path="/support" component={Support} /> 
          <Route path="/" component={Home} />
        </Switch>
      </div>
    )
  }

}

const styles = StyleSheet.create({
  languages: {
    position: 'absolute',
    right: 10,
    top: 0,
    background: 'rgba(70, 70, 70, 0.1)',
    padding: '3px 10px 5px 0',
    zIndex: 155,
    ':nth-child(1n) > img': {
      marginLeft: 10,
      cursor: 'pointer',
      opacity: 0.5,
      ':hover': {
        opacity: 1
      }
    },
    ':nth-child(1n) > img.active': {
      opacity: 1
    },
    '@media (max-width: 1200px)': {
      right: 'calc(50% - 39px)'
    }
  },
});

export default connect(state => ({ lang: state.lang }))(Landing)
