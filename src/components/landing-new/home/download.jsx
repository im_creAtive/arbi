import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'

// images
import iphone from '../../../sources/landing/home/iphone.png'
import appstore from '../../../sources/landing/home/appstore.png'
import googleplay from '../../../sources/landing/home/googleplay.png'

class Download extends Component{

  render(){
    return (
      <div className={css(styles.block)}>
        <h2 className={css(styles.title)}>Bring your work to life.</h2>

        <p className={css(styles.paragraph)}>
          Show 3D assets markerless | Customize Markers | Real-time marker optimization | Supporting IOS & Android
          <br /><br />
          Upload OBJ & Unity assets | Add unlimited users
        </p>

        <div className={css(styles.download)}>
          <div className={css(styles.iphone)}>
            <img src={iphone} alt="iphone" className={css(styles.img)} />
          </div>
          <div className={css(styles.description)}>
            <div>
              <p className={css(styles.text)}>No need to invest in expensive AR hardware such as AR-glasses. Use your smartphone or tablet to access your content in AR with the Arbi App.</p>
              
              <a href="http://apple.com" target="_blank" className={css(styles.downloadBtn, styles.appleBtn)}>
                <img src={appstore} alt="appstore" />
                <span>Download on the Apple Store</span>
              </a>
              <a href="http://google.com" target="_blank" className={css(styles.downloadBtn, styles.googleBtn)}>
                <img src={googleplay} alt="googleplay" />
                <span>Download on the Play Store</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    text: 'loolz'
  },
  nl: {
    text: 'lools'
  }
})

const styles = StyleSheet.create({
  block: {
    width: 1060,
    margin: '100px auto',
    textAlign: 'center',
    '@media (max-width: 1150px)': {
      width: '100%',
      padding: '0 20px'
    }
  },

  title: {
    color: '#38386e',
    fontSize: 30,
    marginBottom: 50
  },

  paragraph: {
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 50
  },

  download: {
    width: 880,
    margin: '50px auto',
    display: 'flex',
    '@media (max-width: 910px)': {
      display: 'block',
      width: '100%',
      padding: '0 20px'
    }
  },

  description: {
    flex: 1,
    justifyContent: 'center',
    display: 'flex',
    textAlign: 'left',
    ':nth-child(1n) > div': {
      alignSelf: 'center',
    }
  },

  text: {
    fontSize: 18,
    fontWeight: 'bold',
    padding: '0 0 0 100px',
    textAlign: 'left',
    marginBottom: 0,
    '@media (max-width: 960px)': {
      padding: '0 0 0 15px',
    },
    '@media (max-width: 570px)': {
      padding: '0 0 0 0',
    },
    '@media (max-width: 385px)': {
      marginBottom: 50
    }
  }, 

  img: {
    position: 'relative',
    left: -130
  },

  iphone: {
    width: 295,
    '@media (max-width: 910px)': {
      display: 'none',
    }
  },

  downloadBtn: {
    display: 'inline-block',
    background: 'red',
    padding: '15px 20px',
    marginTop: 50,
    marginLeft: 100,
    borderRadius: 10,
    textDecoration: 'none',
    boxShadow: '10px 10px 30px 0px rgba(214,214,214,1)',
    width: 458,
    ':nth-child(1n) > span': {
      fontSize: 22,
      color: '#fff',
      marginLeft: 20,
      fontWeight: 'bold',
      display: 'block',
      float: 'left',
      paddingTop: 13,
      '@media (max-width: 570px)': {
        fontSize: 14,
        paddingTop: 1
      },
      '@media (max-width: 385px)': {
        marginLeft: 10,
      },
      '@media (max-width: 345px)': {
        fontSize: 12
      }
    },
    ':nth-child(1n) > img': {
      float: 'left',
      '@media (max-width: 420px)': {
        width: 20
      }
    },
    '@media (max-width: 960px)': {
      marginLeft: 15
    },
    '@media (max-width: 570px)': {
      width: 'auto',
      marginLeft: 0
    },
    '@media (max-width: 385px)': {
      margin: '10px -20px'
    }
  },

  appleBtn: {
    background: '#669999'
  },

  googleBtn: {
    background: '#ed6c77'
  },
})

export default connect(state => ({ lang: state.lang }))(Download);