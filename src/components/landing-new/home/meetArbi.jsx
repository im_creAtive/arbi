import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'

// images
import obj from '../../../sources/landing/home/obj-icon.png'
import unity3d from '../../../sources/landing/home/unity3d-icon.png'

class MeetArbi extends Component{

  render(){
    return (
      <div className={css(styles.block)}>
        <h2 className={css(styles.title)}>Meet Arbi.</h2>

        <p className={css(styles.paragraph)}>
          We are on a mission to make Augmented Reality easy and accesible for everyone. 
        </p>
        <p className={css(styles.paragraph)}>
          Introducing the Arbi CMS. A quick and easy way to convert your 3D models to 
          Augmented Reality. Just upload your 3D assets and use them in Augmented Reality 
          immediately. Supporting OBJ & Unity Assets right now.
        </p> 
        
        <div>
          <img src={obj} alt="obj" className={css(styles.obj)} />
          <img src={unity3d} alt="unity" />
        </div>
      </div>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    text: 'loolz'
  },
  nl: {
    text: 'lools'
  }
})

const styles = StyleSheet.create({
  block: {
    width: 850,
    margin: '100px auto',
    textAlign: 'center',
    '@media (max-width: 910px)': {
      width: '100%',
      padding: '0 20px'
    }
  },

  title: {
    color: '#38386e',
    fontSize: 30,
    marginBottom: 50,
    '@media (max-width: 910px)': {
      width: '100%',
      padding: '0 20px'
    }
  },

  paragraph: {
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 50
  },

  obj: {
    marginRight: 15
  },
})

export default connect(state => ({ lang: state.lang }))(MeetArbi);