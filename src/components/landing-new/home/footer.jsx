import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'
import { Link } from 'react-router-dom'

class Footer extends Component{

  render(){
    return (
      <footer className={css(styles.footer)}>
        <div className={css(styles.lists)}>
          <ul>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/">FAQ</Link></li>
            <li><Link to="/">Contact us</Link></li>
          </ul>
          <ul>
            <li><Link to="/">{translate.forDesigners}</Link></li>
            <li><Link to="/corporate">{translate.forCorpporations}</Link></li>
            <li><Link to="/restaurant">{translate.forRestaurants}</Link></li>
          </ul>
          <ul>
            <li><Link to="/login">{translate.login}</Link></li>
            <li><Link to="/blog">Blog</Link></li>
          </ul>
          <ul>
            <li><Link to="/support">Support</Link></li>
          </ul>
        </div>

        <span className={css(styles.rights)}>2018 Arbi. All rights reserved</span>
      </footer>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    forDesigners: 'For Designers',
    forCorpporations: 'For Corporations',
    forRestaurants: 'For Restaurants',
    login: 'Log in',
    contactUs: 'Contact us'
  },
  nl: {
    forDesigners: 'Voor designers',
    forCorpporations: 'Voor bedrijven',
    forRestaurants: 'Voor restaurants',
    login: 'Login',
    contactUs: 'Contact'
  }
})

const styles = StyleSheet.create({
  footer: {
    background: '#380056',
    padding: 20
  },

  lists: {
    display: 'flex',
    flexWrap: 'wrap',
    ':nth-child(1n) > ul': {
      width: '23%',
      margin: '0 1%',
      marginBottom: 50,
      marginTop: 20,
      listStyle: 'none',
      padding: 0,
      ':nth-child(1n) > li > a': {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
      },
      '@media (max-width: 900px)': {
        width: '48%',
        marginBottom: 30,
      },
      '@media (max-width: 520px)': {
        width: '98%',
        marginBottom: 10,
        textAlign: 'center',
      }
    }
  },

  rights: {
    fontSize: 16,
    color: '#fff',
    display: 'block',
    fontWeight: 'bold',
    textAlign: 'center',
    '@media (max-width: 520px)': {
      marginTop: 30
    }
  }
})

export default connect(state => ({ lang: state.lang }))(Footer);