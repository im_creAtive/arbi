import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'

// images
import video from '../../../sources/video/arbio_video.mp4'
import playBtn from '../../../sources/landing/play_button.png'

class Video extends Component{

  state = {
    video: false
  }

  onClickPlay(){
    this.setState({ video: !this.state.video }, () => {
      this.video.play();
    })
  }

  onClickStop(){
    if(!this.state.video) return;

    this.setState({ video: !this.state.video }, () => {
      this.video.pause();
    })
  }

  render(){
    return (
      <div>
        <h2 className={css(styles.title)} style={{color: this.props.titleColor}}>{translate.title}</h2>

        <div className={css(styles.videoBlock)}>
          <video 
            ref={node => this.video = node} 
            src={video} 
            className={css(styles.video)}
            onClick={e => this.onClickStop()} />
          { !this.state.video ? <button className={css(styles.videoBtn)} onClick={e => this.onClickPlay()}></button> : null }
        </div>
      </div>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    title: 'Watch the video to see how it works.'
  },
  nl: {
    title: 'Watch the video to see how it works.'
  }
})

const styles = StyleSheet.create({
  title: {
    fontSize: 30,
    marginBottom: 50,
    textAlign: 'center',
    '@media (max-width: 910px)': {
      width: '100%',
      padding: '0 20px'
    }
  },

  video: {
    height: '100vh',
    width: '100%',
    background: '#000'
  },

  videoBlock: {
    position: 'relative',
    overflow: 'hidden',
  }, 

  videoBtn: {
    width: 100,
    height: 100,
    background: 'url('+ playBtn +')',
    border: 0,
    position: 'absolute',
    top: 'calc(50% - 50px)',
    left: 'calc(50% - 50px)',
    backgroundSize: 'cover',
    cursor: 'pointer',
    borderRadius: '50%'
  },
})

export default connect(state => ({ lang: state.lang }))(Video);