import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'

// components
import Header from './header'
import Video from './video'
import MeetArbi from './meetArbi'
import Download from './download'
import SignUp from './signup'
import Footer from './footer'

class Home extends Component{

  componentDidMount(){
    window.scroll(0, 0)
  }

  render(){
    console.log(this.props)

    return (
      <div>
        <Header />
        <MeetArbi />
        <Video 
          titleColor='#38386e'
        />
        <Download />
        <SignUp />
        <Footer />
      </div>
    )
  }

}

export default connect(state => ({ lang: state.lang }))(Home);