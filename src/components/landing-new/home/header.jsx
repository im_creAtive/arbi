import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'
import { Link } from 'react-router-dom'
import { HashLink } from 'react-router-hash-link'

// images
import cmsBg from '../../../sources/landing/home/home-cms-bg.jpg'
import corpBg from '../../../sources/landing/home/home-corp-bg.jpg'
import restBg from '../../../sources/landing/home/home-food-bg.jpg'
import cmsIcon from '../../../sources/landing/home/home-cms-icon.png'
import corpIcon from '../../../sources/landing/home/home-corp-icon.png'
import foodIcon from '../../../sources/landing/home/home-food-icon.png'

class Header extends Component{

  render(){
    console.log(this.props)

    return (
      <div>
        <div className={css(styles.header)}>
          <div className={css(styles.main)}>
            
            <div>
              <img src={cmsIcon} className={css(styles.mainImg)} alt="main img" />
              <h3 className={css(styles.title)}><span>Arbi</span> CMS</h3>
              <p className={css(styles.paragraph)}>
                We convert your own 3D models into Augumented Reality in 1 minute! Supporting 
                OBJ & Unity assets.
              </p>
              <p className={css(styles.paragraph)}>
                Free for designers & developers. Share your work in AR and let us know how we 
                can improve our product.
              </p>
              <HashLink to="/#signup" className={css(styles.createAccountBtn)}>Create your free account</HashLink>
            </div>

          </div>
          <div className={css(styles.secondary)}>
            <div className={css(styles.corp)}>

              <div>
                <div className={css(styles.corpDesc)}>
                  <h3 className={css(styles.title)}><span>Arbi</span> Corporate Solutions</h3>
                  <p className={css(styles.paragraph, styles.paragraph16)}>
                    Is Augumented Reality suited for business? Will it deliver value? Will my
                    employees accept it?<br />
                    How can we validate it?
                  </p>
                  <Link to="/corporate" className={css(styles.corpBtn)}>Let us guide you in the proccess</Link>
                </div>
                <div className={css(styles.corpIconBlock)}>
                  <img src={corpIcon} className={css(styles.corpIcon)} alt="corp icon" />
                </div>
              </div>

            </div>
            <div className={css(styles.rest)}>
            
              <div>
                <div className={css(styles.restDesc)}>
                  <h3 className={css(styles.title)}><span>Holo</span>Food</h3>
                  <p className={css(styles.paragraph, styles.paragraph16)}>
                    Let your guests view the menu-items in 3D holograms and deliver 
                    an amazing dining experience. Stand out from the crowd and go viral.
                  </p>
                  <span className={css(styles.restKinds)}>Restaurants | Catering | Hotels | Food Retailers</span>
                  <Link to="/restaurant" className={css(styles.restBtn)}>Schedule for a demo right now</Link>
                </div>
                <div className={css(styles.restIconBlock)}>
                  <img src={foodIcon} className={css(styles.restIcon)} alt="rest icon" />
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    text: 'loolz'
  },
  nl: {
    text: 'lools'
  }
})

const styles = StyleSheet.create({
  header: {
    background: '#fff',
    display: 'flex',
    height: '100vh',
    ':nth-child(1n) > div': {
      flex: 1
    },
    '@media (max-width: 970px)': {
      height: 'auto',
      display: 'block',
    }
  },

  secondary: {
    display: 'flex',
    flexDirection: 'column',
    ':nth-child(1n) > div': {
      flex: 1,
      justifyContent: 'start',
      paddingLeft: 50,
      '@media (max-width: 970px)': {
        padding: 30
      }
    }
  },

  // ============= main ================
  main: {
    background: 'url('+ cmsBg +')',
    backgroundSize: 'cover',
    backgroundPosition: 'bottom left',
    display: 'flex',
    justifyContent: 'center',
    ':nth-child(1n) > div': {
      alignSelf: 'center',
      width: 570,
      '@media (max-width: 1300px)': {
        width: '100%',
        padding: '0 30px'
      },
      '@media (max-width: 1190px)': {

      },
      '@media (max-width: 970px)': {
        padding: '50px 30px'
      }
    }
  },
  
  mainImg: {
    position: 'relative',
    top: 30,
    left: -85,
    marginTop: -30,
    '@media (max-width: 1300px)': {
      width: 500,
      left: 0
    },
    '@media (max-width: 1190px)': {
      width: 300,
      left: 'calc(50% - 150px)',
      top: 0
    },
    '@media (max-width: 970px)': {
      width: 400,
      left: 'calc(50% - 200px)',
      top: 0
    },
    '@media (max-width: 450px)': {
      width: 300,
      left: 'calc(50% - 150px)',
      top: 0
    }
  },
  // ============= main ================

  // ============= corp ================
  corp: {
    background: 'url('+ corpBg +')',
    backgroundPosition: 'bottom right',
    display: 'flex',
    justifyContent: 'center',
    ':nth-child(1n) > div': {
      width: 570,
      display: 'flex',
      alignSelf: 'flex-end',
      marginBottom: 50,
      '@media (min-width: 1500px)': {
        width: 720,
      },
      '@media (min-width: 1800px)': {
        width: 770,
      },
      '@media (max-width: 1150px)': {
        width: 400,
      },
      '@media (max-width: 970px)': {
        flexDirection: 'column-reverse',
        width: '100%',
        marginBottom: 0
      },
    },
    '@media (max-width: 970px)': {
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
    }
  },

  corpDesc: {
    width: 420,
    '@media (max-width: 970px)': {
      width: '100%'
    }
  },

  corpBtn: {
    background: '#001e99',
    fontSize: 18,
    border: '3px solid #fff',
    borderRadius: 15,
    padding: '5px 10px',
    fontWeight: 'bold',
    color: '#fff',
    cursor: 'pointer',
    display: 'inline-block',
    ':hover': {
      background: '#41529e',
      textDecoration: 'none',
    },
    '@media (max-width: 450px)': {
      textAlign: 'center',
    }
  },

  corpIcon: {
    width: 160,
    position: 'relative',
    marginLeft: -10,
    alignSelf: 'center',
    '@media (min-width: 1500px)': {
      width: 300,
      marginLeft: 0,
    },
    '@media (min-width: 1800px)': {
      width: 350,
    },
    '@media (max-width: 1150px)': {
      display: 'none',
    },
    '@media (max-width: 970px)': {
      display: 'block',
      width: 400,
      marginLeft: 'calc(50% - 200px)'
    },
    '@media (max-width: 450px)': {
      display: 'block',
      width: 300,
      marginLeft: 'calc(50% - 150px)'
    },
  },

  corpIconBlock: {
    display: 'flex'
  },
  // ============= corp ================

  // ============= rest ================
  rest: {
    background: 'url('+ restBg +')',
    backgroundPosition: 'bottom right',
    display: 'flex',
    justifyContent: 'center',
    ':nth-child(1n) > div': {
      width: 570,
      display: 'flex',
      alignSelf: 'flex-start',
      marginTop: 25,
      '@media (min-width: 1500px)': {
        width: 720,
      },
      '@media (min-width: 1800px)': {
        width: 770,
      },
      '@media (max-width: 1150px)': {
        width: 400,
      },
      '@media (max-width: 970px)': {
        flexDirection: 'column-reverse',
        width: '100%',
        marginBottom: 0
      },
    },
    '@media (max-width: 970px)': {
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
    }
  },

  restDesc: {
    width: 420,
    '@media (max-width: 970px)': {
      width: '100%'
    }
  },

  restBtn: {
    background: '#3b4100',
    fontSize: 18,
    border: '3px solid #fff',
    borderRadius: 15,
    padding: '5px 10px',
    fontWeight: 'bold',
    color: '#fff',
    cursor: 'pointer',
    display: 'inline-block',
    ':hover': {
      background: '#41529e',
      textDecoration: 'none',
    },
    '@media (max-width: 450px)': {
      textAlign: 'center',
    }
  },

  restIcon: {
    width: 160,
    position: 'relative',
    marginLeft: -10,
    alignSelf: 'center',
    '@media (min-width: 1500px)': {
      width: 300,
      marginLeft: 0,
    },
    '@media (min-width: 1800px)': {
      width: 350,
    },
    '@media (max-width: 1150px)': {
      display: 'none',
    },
    '@media (max-width: 970px)': {
      display: 'block',
      width: 400,
      marginLeft: 'calc(50% - 200px)'
    },
    '@media (max-width: 450px)': {
      display: 'block',
      width: 300,
      marginLeft: 'calc(50% - 150px)'
    },
  },

  restIconBlock: {
    display: 'flex'
  },

  restKinds: {
    color: '#cdfffc',
    fontSize: 16,
    marginBottom: 20,
    display: 'block'
  },
  // ============= rest ================

  title: {
    color: '#fff',
    fontSize: 30,
    overflow: 'visible',
    marginBottom: 25,
    ':nth-child(1n) > span': {
      borderBottom: '4px solid #fff',
      paddingBottom: 5
    },
    '@media (max-width: 460px)': {
      lineHeight: '1.5'
    }
  },

  paragraph: {
    fontWeight: 'bold',
    color: '#fff',
    margin: 0,
    padding: 0,
    marginBottom: 25,
    fontSize: 18
  },
  paragraph16: {
    fontSize: 16,
    '@media (max-width: 1150px)': {
      fontSize: 14
    }
  },

  createAccountBtn: {
    background: '#581e4c',
    fontSize: 18,
    border: '3px solid #fff',
    borderRadius: 15,
    padding: '5px 10px',
    fontWeight: 'bold',
    color: '#fff',
    display: 'inline-block',
    cursor: 'pointer',
    ':hover': {
      background: '#41529e',
      textDecoration: 'none',
    }
  }
})

export default connect(state => ({ lang: state.lang }))(Header);