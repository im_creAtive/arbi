import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'

import * as authorizationAction from '../../../actions/authorizationAction'
import * as mailAction from '../../../actions/mailAction'
import { NotificationManager } from 'react-notifications'
import emailConfirm from '../../public/mails/emailConfirm'

// images
import callbackBg from '../../../sources/landing/callback-bg.jpg'

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const numbersRegex = /^([^0-9]*)$/
const phoneRegex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/

class SignUp extends Component{

  state = {
    Email: "",
    FirstName: "",
    LastName: "",
    PhoneNumber: ""
  }

  async onSignUp(e){
    if(e) e.preventDefault();

		try {
      if(
        this.state.FirstName === '' || 
        this.state.LastName === '' || 
        this.state.Email === ''
      ) throw new Error('All form fields must be filled')
      if(!numbersRegex.test(this.state.FirstName)) throw new Error('First name field is invalid')
      if(!numbersRegex.test(this.state.LastName)) throw new Error('Last name field is invalid')
      if(!emailRegex.test(this.state.Email)) throw new Error('Email field is invalid')
      if(!phoneRegex.test(this.state.PhoneNumber) && this.state.PhoneNumber !== '') throw new Error('Phone number field is invalid')

      await authorizationAction.registration(this.state);
      await mailAction.afterFeedback(this.state.Email, emailConfirm(this.state.FirstName, window.location.origin + '/emailconfirmed/' + this.state.Email));      
			window.$('#successRegister').modal('show');
      
      setTimeout(() => {
        window.$('#successRegister').modal('hide');
      }, 2000);

			this.setState({
				Email: "",
        FirstName: "",
        LastName: "",
        PhoneNumber: ""
      })

    } catch (error) {
      if(error.message){
        NotificationManager.error(error.message);mailAction
      } else {
        try{
          error = JSON.parse(error);
          NotificationManager.error(error.Message || error.error || 'Error. Try later.');
        } catch(e){
          console.log(e)
          NotificationManager.error('Error. Try later.');
        }
      }
    }
	}

  render(){
    return (
      <form className={css(styles.form)} onSubmit={this.onSignUp.bind(this)} id="signup" autoComplete="off">
        <div className="modal fade" id="successRegister" tabIndex="-1" role="dialog" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content" style={{ marginTop: '200px', padding: 40 }}>
              <div className="modal-body">
                Thanks for signing up. Check your email for the access link.
              </div>
            </div>
          </div>
        </div>

        <span className={css(styles.title)}>Create your free account.</span>

        <div className={css(styles.inputs)}>
          <input 
            placeholder={translate.firstName}
            value={this.state.FirstName}
            onChange={e => this.setState({ FirstName: e.target.value })} />
          <input 
            placeholder={translate.lastName}
            value={this.state.LastName}
            onChange={e => this.setState({ LastName: e.target.value })} />
          <input 
            placeholder={translate.email}
            value={this.state.Email}
            onChange={e => this.setState({ Email: e.target.value })} />
          <input 
            placeholder={translate.phone}
            value={this.state.PhoneNumber}
            onChange={e => this.setState({ PhoneNumber: e.target.value })} />
        </div>

        <button className={css(styles.button)}>Create account</button>
      </form>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    firstName: 'First Name',
    lastName: 'Last Name',
    email: 'Email',
    phone: 'Phone'
  },
  nl: {
    firstName: 'Voornaam',
    lastName: 'Achternaam',
    email: 'Bedrijfsemail',
    phone: 'Telefoon'
  }
})

const styles = StyleSheet.create({
  form: {
    background: 'url('+ callbackBg +')',
    backgroundSize: 'cover',
    padding: '50px 0'
  },

  title: {
    fontSize: 30,
    color: '#fff',
    textAlign: 'center',
    display: 'block',
    fontWeight: 'bold'
  },

  inputs: {
    width: 670,
    display: 'flex',
    flexWrap: 'wrap',
    margin: '0 auto',
    ':nth-child(1n) > input': {
      flex: 1,
      background: 'transparent',
      border: 0,
      color: '#fff',
      fontSize: 18,
      borderBottom: '2px solid #fff',
      marginTop: 50,
      fontWeight: 'bold',
      '::placeholder': {
        color: '#fff',
      },
      '@media (max-width: 550px)': {
        width: '100%',
        flex: 'none',
      }
    },
    ':nth-child(1n) > input:nth-child(2n)': {
      marginLeft: 30,
      '@media (max-width: 550px)': {
        marginLeft: 0
      }
    },
    '@media (max-width: 750px)': {
      width: '100%',
      padding: '0 20px',
    }
  },

  button: {
    background: '#b15fc8',
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    padding: '5px 45px',
    borderRadius: 10,
    border: '3px solid #fff',
    margin: '50px auto',
    display: 'block',
    cursor: 'pointer',
    ':hover': {
      background: '#cc7ee2'
    }
  }
})

export default connect(state => ({ lang: state.lang }))(SignUp);