import React, {Component} from 'react'
import {css, StyleSheet} from 'aphrodite'

import Header from './header'
import Description from './description'
import Cards from './cards'
import TopNav from './topnav'

import Video from '../home/video'
import Footer from '../home/footer'
import SignUp from '../home/signup'
import Callback from './callback'

import callbackBg from '../../../sources/landing/callback-bg.jpg'

class Restaurant extends Component {

  componentDidMount(){
    window.scroll(0, 0)
  }

  render () {
    return (
      <div className={css(styles.restaurant)}>
        <TopNav />
        <Header />
        <Description />
        <Video
          titleColor='#000'
        />
        <Cards />

        <Callback />

        <Footer />
      </div>
    )
  } 
}


const styles = StyleSheet.create({
  restaurant : {
    backgroundColor: '#fff'
  },
})


export default Restaurant;