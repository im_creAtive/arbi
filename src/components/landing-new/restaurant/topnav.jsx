import React, {Component} from 'react'
import {css, StyleSheet} from 'aphrodite'
import { connect } from 'react-redux'
import { Link, NavLink } from 'react-router-dom'
import ScheduleCallback from '../corporate/scheduleCallback'
import MultiLanguage, { setLanguage } from 'react-redux-multilang'

import mobileMenuBg from '../../../sources/landing/corp/answers-left-bg.png'

class TopNav extends Component {

  state = { mobileMenu: false }

  toggleMenu(){
    this.setState({ mobileMenu: !this.state.mobileMenu })
  }

  render () {
    return (
      <div>

        <div className={css(styles.headerMenu)}>
          <div>
            <ScheduleCallback button={
              <button className={css(styles.callbackButton, this.state.mobileMenu && styles.callbackButtonOpened)}>
                <span>{translate.scheduleCallback}</span>
                <i className="fa fa-phone" aria-hidden="true"></i>
              </button>
            } />
            <div className={css(styles.headerMenuItems, this.props.dark && styles.headerMenuItemsDark)}>
              <NavLink exact to="/">{translate.forDesigners}</NavLink>
              <NavLink to="/corporate">{translate.forCorpporations}</NavLink>
              <NavLink to="/restaurant">{translate.forRestaurants}</NavLink>
            </div>
            <Link to="/login" className={css(styles.loginButton)}>Login</Link>
            
            <i 
              className={css(styles.mobileMenuIcon, this.state.mobileMenu && styles.mobileMenuIconOpened, this.props.dark && styles.mobileMenuIconDark) + " fa fa-bars"} 
              aria-hidden="true"
              onClick={() => this.toggleMenu()} ></i>
            <div className={css(styles.mobileMenu, this.state.mobileMenu && styles.mobileMenuOpened)}>
              <ul className={css(styles.mobileMenuList)}>
                <li><NavLink exact to="/">{translate.forDesigners}</NavLink></li>
                <li><NavLink to="/corporate">{translate.forCorpporations}</NavLink></li>
                <li><NavLink to="/restaurant">{translate.forRestaurants}</NavLink></li>
                <li><NavLink to="/login">Login</NavLink></li>
              </ul>
            </div>
          </div>
        </div>

        <div className="modal fade" id="callbackThanks" tabIndex="-1" role="dialog"aria-hidden="true">
					<div className="modal-dialog modal-sm" role="document">
						<div className="modal-content" style={{ marginTop: '200px' }}>
							<div className="modal-body">
                Thank you for your message. We will reply within 24 hours or less. Promised!
							</div>
						</div>
					</div>
				</div>

      </div>
    )
  } 
}

const translate = new MultiLanguage({
  en: {
    scheduleCallback: 'Schedule a call-back',
    forDesigners: 'For Designers',
    forCorpporations: 'For Corporations',
    forRestaurants: 'For Restaurants',
  },
  nl: {
    scheduleCallback: 'Maak een terugbelafspraak',
    forDesigners: 'Voor designers',
    forCorpporations: 'Voor bedrijven',
    forRestaurants: 'Voor restaurants',
  }
})


const styles = StyleSheet.create({  
  headerMenu: {
    overflow: 'auto',
    position: 'absolute',
    zIndex: 100,
    left: 'calc(50% - 500px)',
    ':nth-child(1n) > div': {
      width: 1000,
      height: 50,
      margin: '0 auto',
      marginTop: 30,
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      '@media (max-width: 1050px)': {
        width: 900
      },
      '@media (max-width: 940px)': {
        width: 700
      },
      '@media (max-width: 750px)': {
        width: '100%'
      }
    },
    '@media (max-width: 1050px)': {
      left: 'calc(50% - 450px)',
    },
    '@media (max-width: 940px)': {
      left: 'calc(50% - 350px)',
    },
    '@media (max-width: 750px)': {
      left: 25,
      width: 'calc(100% - 50px)'
    }
  },

  callbackButton: {
    backgroundColor: '#581E4C',
    border: '3px solid #fff',
    borderBottomLeftRadius: 18,
    borderBottomRightRadius: 18,
    borderTopLeftRadius: 18,
    borderTopRightRadius: 18,
    padding: '13px 24px',
    color: 'white',
    lineHeight: 1,
    fontWeight: 'bold',
    fontSize: 16,
    position: 'relative',
    zIndex: 2,
    ':hover': {
      backgroundColor: '#41529e',
      cursor: 'pointer'
    },
    ':nth-child(1n) > i': {
      display: 'none',
      '@media (max-width: 1050px)': {
        display: 'block',
        fontSize: 22,
        margin: '-3px -5px'
      }
    },
    ':nth-child(1n) > span': {
      display: 'block',
      '@media (max-width: 1050px)': {
        display: 'none',
      }
    }
  },
  callbackButtonOpened: {
    position: 'fixed',
  },

  loginButton: {
    backgroundColor: '#001E99',
    border: '3px solid #fff',
    borderBottomLeftRadius: 18,
    borderBottomRightRadius: 18,
    borderTopLeftRadius: 18,
    borderTopRightRadius: 18,
    padding: '7px 45px',
    color: 'white',
    lineHeight: 1,
    fontWeight: 'bold',
    fontSize: 18,
    height: 37,
    display: 'inline-block',
    ':hover': {
      backgroundColor: '#41529e',
      cursor: 'pointer',
      textDecoration: 'none',
    },
    '@media (max-width: 750px)': {
      display: 'none',
    }
  },

  headerMenuItems: {
    float: 'left',
    fontSize: 14,
    width: '53%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    ':nth-child(1n) > a': {
      color: '#ffffff',
      textDecoration: 'none',
      marginRight: 25,
      fontWeight: 600,
      ':hover': {
        textDecoration: 'underline',
      },
    },
    ':nth-child(1n) > a.active': {
      textDecoration: 'underline',
    },
    '@media (max-width: 1050px)': {
      width: '63%'
    },
    '@media (max-width: 750px)': {
      display: 'none',
    }
  },
  
  headerMenuItemsDark:{
    ':nth-child(1n) > a': {
      color: '#38386E'
    }
  },

  mobileMenuIcon: {
    fontSize: 30,
    color: '#fff',
    display: 'none',
    cursor: 'pointer',
    position: 'relative',
    zIndex: 20,
    '@media (max-width: 750px)': {
      display: 'block',
    }
  },
  mobileMenuIconOpened: {
    position: 'fixed',
    right: 25
  },
  mobileMenuIconDark: {
    color: '#38386E',
  },

  mobileMenu: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100vh',
    paddingTop: 100,
    background: 'url('+ mobileMenuBg +')',
    backgroundSize: 'cover',
    backgroundPosition: 'bottom left',
    zIndex: 1,
    marginLeft: '-100%',
    transition: '0.3s',
  },

  mobileMenuOpened: {
    transition: '0.3s',
    marginLeft: 0
  },

  mobileMenuList: {
    listStyle: 'none',
    padding: 0,
    margin: 0,
    ':nth-child(1n) > li > a': {
      padding: '20px 25px',
      fontSize: 22,
      color: '#fff',
      fontWeight: 'bold',
      display: 'block',
    }
  }
})


export default connect(state => ({ lang: state.lang }))(TopNav);