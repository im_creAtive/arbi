import React, {Component} from 'react'
import {css, StyleSheet} from 'aphrodite'


class Description extends Component {
  render () {
    return (
      <div className={css(styles.topDescription)}>
        <h3>The Future is here.</h3>
        <p>Great-looking food sells. But with most menu’s in plain-text, customers end up visualizing what dishes look like and asking whats in it. </p>
        <p>With Augmented Reality, we showcase your menu-items as 3D holograms right there at the table delivering an amazing customer experience.</p>
      </div>
    )
  } 
}


const styles = StyleSheet.create({
  topDescription: {
    background: 'white',
    overflow: 'hidden',
    marginBottom: 150,
    ':nth-child(1n) > h3': {
      display: 'table',
      margin: '0 auto',
      marginTop: 70,
      fontSize: 35,
      color: '#581e4c',
      '@media (max-width: 400px)': {
        fontSize: 30
      }
    },
    ':nth-child(1n) > p': {
      margin: '0 auto',
      marginTop: 40,
      textAlign: 'center',
      width: 970,
      fontWeight: 600,
      fontSize: 24,
      color: 'black',
      '@media (max-width: 1000px)': {
        width: 'calc(100% - 30px)',
        marginLeft: 15,
        marginRight: 15,
        textAlign: 'center',
      }
    }
  },
})


export default Description;