import React, {Component} from 'react'
import {css, StyleSheet} from 'aphrodite'

import cardBackground from '../../../sources/landing/rest/cube-bg.png'

class Card extends Component {
  render () {
    return (
      <div className={css(styles.card_block)}>
        <div>
          <h6>{this.props.cardTitle ? this.props.cardTitle : ''}<br/>{this.props.cartTitleSecond ? this.props.cartTitleSecond : ''}</h6>
          <div className={css(styles.paragraphs)}>
            <p>{this.props.cardFirst ? this.props.cardFirst : ''}</p>
            <p>{this.props.cardSecond ? this.props.cardSecond : ''}</p>
          </div>
        </div>
      </div>  
    )
  }
}


const styles = StyleSheet.create({
  card_block: {
    position: 'relative',
    ':nth-child(1n) > div': {
      background: 'white',
      boxShadow: '0px 2px 6px 0px rgba(87,87,87,1)',
      zIndex: 99,
      position: 'relative',
      width: 330,
      height: 330,
      '@media (max-width: 500px)': {
        width: '100%'
      }
    },
    ':nth-child(1n) > div > h6': {
      color: '#38386e',
      fontSize: 24,
      fontWeight: 600, 
      paddingTop: 40,
      paddingLeft: 20,
    },
    ':nth-child(1n) > div > div': {
      marginTop: 20
    },
    ':nth-child(1n) > div > div > p': {
      fontSize: 16,
      fontWeight: 600,
      paddingLeft: 20,
      paddingTop: 10
    },
    ':after': {
      content: "''",
      background: 'url(' + cardBackground + ')',
      width: 330,
      height: 330,
      backgroundSize: 'cover',
      position: 'absolute',
      left: '-40px',
      top: 30,
      '@media (max-width: 500px)': {
        width: 'auto',
        height: 'auto',
        left: -30,
        right: -30,
        top: -30,
        bottom: -30
      }
    },
    '@media (max-width: 1200px)': {
      margin: 30
    },
    '@media (max-width: 500px)': {
      margin: '70px 30px'
    }
  }
})


export default Card;