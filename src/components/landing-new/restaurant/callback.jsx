import React, {Component} from 'react'
import {css, StyleSheet} from 'aphrodite'
import { connect } from 'react-redux'
import { NotificationManager } from 'react-notifications'
import * as messageAction from '../../../actions/messageAction'
import * as mailAction from '../../../actions/mailAction'
import email3 from '../../public/mails/email3'
import newLandingMail from '../../public/mails/newLandingMail'
import ScheduleCallback from '../corporate/scheduleCallback'
import MultiLanguage from 'react-redux-multilang'

import callbackBg from '../../../sources/landing/callback-bg.jpg'

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const numbersRegex = /^([^0-9]*)$/
const phoneRegex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/

const defaultState = {
  FirstName: '',
  LastName: '',
  Email: '',
  Phone: '',
  Message: ''
}

class Callback extends Component {

  state = { ...defaultState }

  async onSendMessage(e){
    if(e) e.preventDefault();

    try{
      if(this.state.FirstName === '') throw new Error(translate.firstName + ' is required field.');
      if(this.state.LastName === '') throw new Error(translate.lastName + ' is required field.');
      if(this.state.Email === '') throw new Error(translate.email + ' is required field.');
      if(this.state.Message === '') throw new Error(translate.message + ' is required field.');
  
      if(!numbersRegex.test(this.state.FirstName)) throw new Error(translate.firstName + ' field is invalid');
      if(!numbersRegex.test(this.state.LastName)) throw new Error(translate.lastName + ' field is invalid');
      if(!emailRegex.test(this.state.Email)) throw new Error(translate.email + ' field is invalid');
  
      if(this.state.Phone !== '' && !phoneRegex.test(this.state.Phone)) throw new Error(translate.phone + ' number field is invalid');

      let userMail = email3(this.state.FirstName);
      let adminMail = newLandingMail(
        this.state.FirstName,
        this.state.LastName,
        this.state.Email,
        this.state.Phone,
        this.state.Message
      )

      await mailAction.afterFeedback(this.state.Email, userMail);
      await messageAction.admin(adminMail);

      window.$('#callbackThanks').modal('show');
      setTimeout(() => {
        window.$('#callbackThanks').modal('hide');
      }, 2000);

      this.setState({ ...defaultState })
    } catch(e) {
      NotificationManager.error(e.message || e)
    }
  }

  render () {
    return (
      <div className={css(styles.block)}>
          
        <h2 className={css(styles.title)}>{translate.readyToStart}</h2>

        <div className={css(styles.form)}>
          <div>
            <ScheduleCallback button={
              <button className={css(styles.schedule)}>{translate.scheduleCallback}</button>
            } />
          </div>
          <div>
            <form onSubmit={this.onSendMessage.bind(this)} autoComplete="off">
              <div className={css(styles.inputs)}>
                <input 
                  placeholder={translate.firstName}
                  value={this.state.FirstName}
                  onChange={e => this.setState({ FirstName: e.target.value })} />
                <input 
                  placeholder={translate.lastName}
                  value={this.state.LastName}
                  onChange={e => this.setState({ LastName: e.target.value })} />
                <input 
                  placeholder={translate.email}
                  value={this.state.Email}
                  onChange={e => this.setState({ Email: e.target.value })} />
                <input 
                  placeholder={translate.phone}
                  value={this.state.Phone}
                  onChange={e => this.setState({ Phone: e.target.value })} />
                <textarea 
                  placeholder={translate.message}
                  value={this.state.Message}
                  onChange={e => this.setState({ Message: e.target.value })} />
              </div>

              <button className={css(styles.button)}>{translate.sendMessage}</button>
            </form>
          </div>
        </div>

      </div>
    )
  } 
}

const translate = new MultiLanguage({
  en: {
    readyToStart: 'Ready to start the conversation? Schedule a call-back or send us a message.',
    scheduleCallback: 'Schedule a call-back',
    firstName: 'First Name',
    lastName: 'Last Name',
    email: 'Email',
    phone: 'Phone',
    message: 'Your message',
    sendMessage: 'Send message'
  },
  nl: {
    readyToStart: 'Klaar om het gesprek aan te gaan? Maak een terugbelafspraak of stuur ons een bericht.',
    scheduleCallback: 'Maak een terugbelafspraak',
    firstName: 'Voornaam',
    lastName: 'Achternaam',
    email: 'Bedrijfsemail',
    phone: 'Telefoon',
    message: 'Jouw bericht',
    sendMessage: 'Verzend bericht'
  }
})



const styles = StyleSheet.create({
  block: {
    background: 'url('+ callbackBg +')',
    backgroundSize: 'cover',
    padding: '70px 0'
  },

  title: {
    textAlign: 'center',
    color: '#fff',
    width: 730,
    margin: '0 auto',
    '@media (max-width: 800px)': {
      width: 'calc(100% - 30px)',
      paddingLeft: 15,
      paddingRight: 15
    },
    '@media (max-width: 480px)': {
      fontSize: 25
    }
  },

  form: {
    display: 'flex',
    width: 1100,
    justifyContent: 'center',
    margin: '50px auto',
    ':nth-child(1n) > div': {
      flex: 1,
      padding: '20px 50px',
      display: 'flex',
      justifyContent: 'center',
      flexDirection: 'column',
    },
    ':nth-child(1n) > div:nth-child(2)': {
      borderLeft: '1px solid #fff',
      '@media (max-width: 720px)': {
        borderLeft: 0
      }
    },
    '@media (max-width: 1080px)': {
      width: '100%'
    },
    '@media (max-width: 720px)': {
      flexDirection: 'column',
    }
  },

  schedule: {
    background: '#581e4c',
    color: '#fff',
    fontSize: 27,
    fontWeight: 'bold',
    padding: '5px 15px',
    borderRadius: 10,
    border: '3px solid #fff',
    display: 'block',
    cursor: 'pointer',
    alignSelf: 'center',
    ':hover': {
      background: '#41529e'
    },
    '@media (max-width: 440px)': {
      fontSize: 20
    },
    '@media (max-width: 360px)': {
      fontSize: 17
    }
  },

  inputs: {
    width: '100%',
    display: 'block',
    margin: '0 auto',
    ':nth-child(1n) > input': {
      width: '100%',
      background: 'transparent',
      border: 0,
      color: '#fff',
      fontSize: 18,
      borderBottom: '2px solid #fff',
      marginTop: 50,
      fontWeight: 'bold',
      '::placeholder': {
        color: '#fff',
      },
    },
    ':nth-child(1n) > textarea': {
      width: '100%',
      background: 'transparent',
      border: 0,
      color: '#fff',
      fontSize: 18,
      borderBottom: '2px solid #fff',
      marginTop: 50,
      fontWeight: 'bold',
      '::placeholder': {
        color: '#fff',
      },
      ':focus': {
        outline: 0
      }
    },
    ':nth-child(1n) > input:nth-child(1)': {
      marginTop: 0
    }
  },

  button: {
    background: '#b15fc8',
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    padding: '5px 45px',
    borderRadius: 10,
    border: '3px solid #fff',
    marginTop: '50px',
    display: 'block',
    cursor: 'pointer',
    ':hover': {
      background: '#cc7ee2'
    }
  }
})


export default connect(state => ({ lang: state.lang }))(Callback);