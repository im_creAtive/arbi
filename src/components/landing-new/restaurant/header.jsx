import React, {Component} from 'react'
import {css, StyleSheet} from 'aphrodite'
import { Link, NavLink } from 'react-router-dom'

import headerBackground from '../../../sources/landing/rest/header.png'
import ipadCake from '../../../sources/landing/rest/ipad-cake.png'
import menu from '../../../sources/landing/rest/menu.png'

class Header extends Component {
  render () {
    return (
      <div className={css(styles.restaurant)}>
        <div className={css(styles.header)}>
          <div className={css(styles.headerText)}>
            <h1>The dining experience will never be the <br/> same again.</h1>
          </div>
          <div className={css(styles.pictures)}>
            <div className={css(styles.leftHeaderImage)}>

            </div>
            <div className={css(styles.rightHeaderImage)}>
              <img src={ipadCake} alt="Ipad-Cake" />
            </div>
            <div className={css(styles.menuImage)}>
              <img src={menu} alt="menu" />
            </div>
          </div>
        </div>

      </div>
    )
  } 
}


const styles = StyleSheet.create({
  restaurant : {
    backgroundColor: '#fff',
    overflow: 'hidden',
  },

  header: {
    height: '100vh',
    background: 'url(' + headerBackground + ')',
    backgroundSize: 'cover',
    overflow: 'hidden',
    position: 'relative',
  },

  headerText: {
    paddingTop: 150,
    position: 'relative',
    zIndex: 5,
    textShadow: '0px 0px 10px rgba(0,0,0,0.2)',
    ':nth-child(1n) > h1': {
      margin: '0 auto',
      display: 'table',
      color: 'black',
      fontSize: 36,
      textAlign: 'center'
    },
    '@media (max-width: 830px)': {
      width: 'calc(100% - 30px)',
      marginLeft: 15,
      marginRight: 15,
    }
  },

  pictures: {

  },

  rightHeaderImage: {
    float: 'right',
    width: '50%',
    marginTop: 30,
    position: 'relative',
    ':nth-child(1n) > img': {
      width: 700,
      zIndex: 3,
      position: 'relative',
      '@media (max-width: 700px)': {
        width: 300
      }
    },
    '@media (max-width: 1000px)': {
      float: 'none',
      left: 'calc(50% - 350px)',
      width: 700
    },
    '@media (max-width: 700px)': {
      float: 'none',
      left: 'calc(50% - 150px)',
      width: 300
    }
  },

  menuImage: {
    position: 'absolute',
    width: 800,
    bottom: -110,
    left: 60,
    ':nth-child(1n) > img': {
      width: '100%',
      zIndex: 2,
      position: 'relative',
    },
    '@media (max-width: 1250px)': {
      left: -60,
    },
    '@media (max-width: 1000px)': {
      display: 'none',
    }
  }
})


export default Header;