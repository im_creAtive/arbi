import React, {Component} from 'react'
import {css, StyleSheet} from 'aphrodite'

import cardBackground from '../../../sources/landing/rest/cube-bg.png'
import Card from './card'
import mail from '../../../api/mail';

class Cards extends Component {
  render () {
    return (
      <div className={css(styles.cardsBlock)}>
        <h5>
          Using Augmented Reality Food is suited for Restaurants,<br/>
          Catering, hotels and food retailers.
        </h5>
        <div className={css(styles.cards)}>
          <Card 
            cardTitle="Amaze your"
            cartTitleSecond=" customers"
            cardFirst="Competing in the food business is all about being innovative and improving the customer experience."
            cardSecond="Showcasing meals as 3D holograms will blow your customers away."
          />
          <Card 
            cardTitle="Better"
            cartTitleSecond=" storytelling"
            cardFirst="Deliver a more compelling story about the dishes, its ingredients and presentation."
            cardSecond="Allow waiters to effectively helping customers make a great food choice."
          />
          <Card 
            cardTitle="Go viral"
            cardFirst="Augmented Reality is the latest innovation in food technology and is effective for marketing."
            cardSecond="Amaze customers, stimulate virality and get more media-exposure. "
          />
        </div>
      </div>
    )
  } 
}


const styles = StyleSheet.create({
  cardsBlock: {
    backgroundColor: 'white',
    overflow: 'hidden',
    marginBottom: 100,
    ':nth-child(1n) > h5': {
      textAlign: 'center',
      display: 'table',
      margin: '0 auto',
      marginTop: 70,
      marginBottom: 70,
      fontSize: 26,
      fontWeight: 600,
      color: '#010101',
      '@media (max-width: 830px)': {
        width: 'calc(100% - 30px)',
        marginLeft: 15,
        marginRight: 15
      }
    }
  },

  cards: {
    display: 'flex',
    justifyContent: 'space-around',
    paddingBottom: 50,
    flexWrap: 'wrap',
  },
  card_block: {
    position: 'relative',

    ':nth-child(1n) > div': {
      background: 'white',
      boxShadow: '0px 2px 6px 0px rgba(87,87,87,1)',
      zIndex: 99,
      position: 'relative',
      width: 330,
      height: 330,
    },
    ':nth-child(1n) > div > h6': {
      color: '#38386e',
      fontSize: 24,
      fontWeight: 600, 
      paddingTop: 40,
      paddingLeft: 20,
    },
    ':nth-child(1n) > div > p': {
      fontSize: 16,
      fontWeight: 600,
      paddingLeft: 20,
      paddingTop: 25
    },
    ':after': {
      content: "''",
      background: 'url(' + cardBackground + ')',
      width: 330,
      height: 330,
      backgroundSize: 'cover',
      position: 'absolute',
      left: '-40px',
      top: 30

    }
  }
})


export default Cards;