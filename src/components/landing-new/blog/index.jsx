import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'
import { Switch, Route, Redirect } from 'react-router'

// components
import Topnav from '../restaurant/topnav'
import Footer from '../home/footer'
import BlogList from './list'
import BlogPost from './post'

class Blog extends Component {

  componentDidMount() {
    window.scroll(0, 0)
  }

  render() {
    return (
      <div className={css(styles.wrapper)}>
        <Topnav dark />
        <Switch>
          <Route exact path="/blog" component={BlogList} />
          <Route path="/blog/:id" component={BlogPost} />
        </Switch>
        <Footer />
      </div>
    )
  }

}

const styles = StyleSheet.create({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh'
  }

})

export default connect(state => ({ lang: state.lang, blog: state.blog }))(Blog);