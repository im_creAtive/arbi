import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'
import dateFormat from 'dateformat'
import { Link } from 'react-router-dom'

import * as blogActions from '../../../actions/blogAction'
import config from '../../../api/config'

import Header from './header'

class BlogList extends Component {

  componentDidMount() {
    window.scroll(0, 0)
    blogActions.get()
  }

  render() {
    const posts = this.props.blog.list.map((post, index) => {
      return (
        <div className={css(styles.post)} key={index}>
          <div>
            <h4 className={css(styles.postTitle)}>{post.NameOfArticle}</h4>
            <p className={css(styles.postDesc)}>{post.DiscriptionOfArticle}</p>
            <div className={css(styles.postBottom)}>
              <Link to={'/blog/' + post.Id} className={css(styles.postBtn)}>Read article</Link>
              <span className={css(styles.postDate)}>{dateFormat(post.CreateDate, 'mmmm dS, yyyy')}</span>
            </div>
          </div>
          <div style={{ background: `url(${config.baseHost + 'Files/Download' + post.Image + '&type=BlogArticle'})` }}>
            {/* <img src={config.baseHost + 'Files/Download' + post.Image + '&type=BlogArticle'} alt={post.NameOfArticle} /> */}
          </div>
        </div>
      )
    })

    return (
      <div className={css(styles.wrapper)}>
        <Header />
        <div className={css(styles.blogListContainer)}>
          <div>
            {posts.length > 0 ? posts : (
              <div className={css(styles.noPosts)}>
                <i className="fa fa-info-circle" aria-hidden="true"></i> Post list is empty
              </div>
            )}
          </div>
        </div>
      </div>
    )
  }

}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column'
  },

  blogListContainer: {
    display: 'block',
    background: '#E8E9ED',
    flex: 1,
    ':nth-child(1n) > div': {
      display: 'block',
      maxWidth: '1000px',
      width: '100%',
      margin: '70px auto'
    }
  },

  post: {
    background: '#fff',
    boxShadow: '0px 0px 27px 0px rgba(87,29,75,0.87)',
    display: 'flex',
    marginBottom: 75,
    ':nth-child(1n) > div:first-child':{
      flex: 3,
      padding: 30,

    },
    ':nth-child(1n) > div:last-child': {
      flex: 2,
      backgroundSize: 'cover',
      backgroundPosition: 'center'
    },
    '@media (max-width: 1015px)': {
      marginLeft: 15,
      marginRight: 15
    },
    '@media (max-width: 750px)': {
      flexDirection: 'column-reverse',
      ':nth-child(1n) > div:last-child': {
        minHeight: 200
      }
    }
  },

  postTitle: {
    color: '#2B2B58',
    fontWeight: 600,
    paddingTop: 15
  },

  postDesc: {
    marginTop: 10,
    display: 'inline-block',
    color: '#565656',
    fontSize: 14
  },

  postBottom: {
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: 10,
    '@media (max-width: 450px)': {
      flexDirection: 'column',
      textAlign: 'center'
    }
  },

  postBtn: {
    display: 'inline - block',
    color: '#fff',
    background: '#581E4C',
    textTransform: 'uppercase',
    padding: '7px 20px',
    borderRadius: 50,
    fontSize: 14,
    letterSpacing: 3,
    ':hover': {
      textDecoration: 'none',
      background: '#38386e'
    },
    '@media (max-width: 450px)': {
      marginBottom: 10
    } 
  },

  noPosts: {
    textAlign: 'center',
    alignSelf: 'center',
    fontSize: 22,
    color: '#717171',
    fontWeight: 'bold',
  }
})

export default connect(state => ({ lang: state.lang, blog: state.blog }))(BlogList);