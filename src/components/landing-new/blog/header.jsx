import React from 'react'
import { css, StyleSheet } from 'aphrodite'

export default function (props) {
  return (
    <div className={css(styles.header)}>
      <h3 className={css(styles.title)}>
        Learn everything on Augmented Reality, user cases,<br />trends & latest news
        <div className={css(styles.titleLine)}></div>
      </h3>
    </div>
  )
}

const styles = StyleSheet.create({
  header: {
    position: 'relative',
    zIndex: 2,
    background: '#fff'
  },  

  title: {
    lineHeight: '1.5',
    textAlign: 'center',
    color: '#38386E',
    margin: '0 auto',
    marginTop: 150,
    marginBottom: 100,
    position: 'relative',
    display: 'table',
    '@media (max-width: 950px)': {
      padding: '0 15px'
    },
    '@media (max-width: 450px)': {
      fontSize: 22
    }
  },

  titleLine: {
    position: 'absolute',
    content: '""',
    display: 'block',
    bottom: -30,
    left: 50,
    right: 50,
    borderBottom: '1px solid #581e4c'
  },
})