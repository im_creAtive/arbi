import React, { Component } from 'react'
import { connect } from 'react-redux'
import MultiLanguage from 'react-redux-multilang'
import { css, StyleSheet } from 'aphrodite'
import dateFormat from 'dateformat'
import { Link } from 'react-router-dom'
import { NotificationManager } from 'react-notifications'
import { withRouter } from 'react-router'

import * as blogActions from '../../../actions/blogAction'
import config from '../../../api/config'

import Header from './header'

import {
  ShareButtons,
  ShareCounts,
  generateShareIcon
} from 'react-share';

const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const LinkedinIcon = generateShareIcon('linkedin');

class BlogPost extends Component {

  async componentDidMount() {
    window.scroll(0, 0)
    try{
      let post = await blogActions.getById(this.props.match.params.id);
      if(!post) throw new Error('Post not found. Select an another post.')
      
      this.setState({ ...post })
    } catch(e) {
      console.log(e);
      NotificationManager.error(e.message)
      this.props.history.push('/blog')
    }
  }

  render() {

    console.log(this.props)

    return (
      <div className={css(styles.wrapper)}>
        <Header />
        <div className={css(styles.postContainer)}>
          {this.state && (
            <div>
              <h2 className={css(styles.title)}>{this.state.NameOfArticle}</h2>
              <p className={css(styles.desc)} dangerouslySetInnerHTML={{ __html: this.state.ArticleText.replace(/(?:\r\n|\r|\n)/g, '<br />') }}></p>
              <p className={css(styles.date)}>{dateFormat(this.state.CreateDate, 'mmmm dS, yyyy')}</p>
              <div className={css(styles.socials)}>
                <ShareButtons.FacebookShareButton url={window.location}>
                  <FacebookIcon size={32} round />
                </ShareButtons.FacebookShareButton>
                <ShareButtons.TwitterShareButton url={window.location} title={this.state.NameOfArticle}>
                  <TwitterIcon size={32} round />
                </ShareButtons.TwitterShareButton>
                <ShareButtons.LinkedinShareButton url={window.location} title={this.state.NameOfArticle}>
                  <LinkedinIcon size={32} round />
                </ShareButtons.LinkedinShareButton>
              </div>
            </div>
          )}
        </div>
      </div>
    )
  }

}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column'
  },

  postContainer: {
    display: 'flex',
    background: '#E8E9ED',
    flex: 1,
    justifyContent: 'center',
    ':nth-child(1n) > div': {
      display: 'block',
      maxWidth: '1000px',
      width: '100%',
      margin: 5,
      padding: 50,
      background: '#fff',
      boxShadow: '0px 0px 27px 0px rgba(87,29,75,0.87)',
      '@media (max-width: 750px)': {
        padding: 15
      }
    }
  },

  title: {
    textAlign: 'center',
    padding: '50px 20%',
    wordBreak: 'break-word',
    color: '#2B2B58',
    '@media (max-width: 750px)': {
      padding: '50px 15px',
    },
    '@media (max-width: 400px)': {
      fontSize: 22
    }
  },

  desc: {

  },

  date: {
    textAlign: 'right'
  },

  socials: {
    display: 'flex',
    ':nth-child(1n) > div': {
      marginRight: 10,
      cursor: 'pointer',
    },
    ':nth-child(1n) > div:focus': {
      outline: 0
    }
  }
})

export default connect(state => ({ lang: state.lang }))(withRouter(BlogPost));