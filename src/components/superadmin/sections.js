import React, { Component } from 'react';
import Accounts from './sections/accounts';
import UserTabs from './sections/userTabs';
import PaymentPlans from './sections/paymentPlans';
import Settings from './sections/settings';
import Explore from './sections/explore';
import Upload from './sections/explore/upload';
import Blog from './sections/blog';
import Knowledge from './sections/knowledge';
import Idea from './sections/idea';
import '../../sources/css/superadmin/main.css';

import {Switch, Route, Redirect} from 'react-router';
import landing from '../landing/sections/landing';

class SuperAdminSections extends Component {
    render() {
        return (
            <main className="content">
                <div className="section-contents">
                  <div className="content-card">
                    <section className="container-fluid">
                        <div className="row">
                            <div className="col-sm-12 test">
                                <Switch>
                                    <Route exact path="/superadmin/accounts" component={Accounts}/>
                                    <Route exact path="/superadmin/accounts/users/:id" component={UserTabs}/>
                                    <Route exact path="/superadmin/paymentplans" component={PaymentPlans}/>
                                    <Route exact path="/superadmin/settings" component={Settings}/>
                                    <Route exact path="/superadmin/explore" component={Explore}/>
                                    <Route exact path="/superadmin/explore/upload" component={Upload}/>
                                    <Route exact path="/superadmin/blog" component={Blog} />
                                    <Route exact path="/superadmin/knowledge" component={Knowledge} />
                                    <Route exact path="/superadmin/idea" component={Idea} />
                                    <Redirect from="/superadmin" to="/superadmin/accounts" />
                                </Switch>
                            </div>
                        </div>
                    </section>
                  </div>
                </div>
            </main>

        );
    }
}

export default SuperAdminSections;
