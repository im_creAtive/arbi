import React, { Component } from 'react';
import config from './../../api/config';

import Header from './sections/header'
import Menu from './sections/menu'
import SuperAdminSections from './sections'
import '../../sources/css/superadmin/app.css'

class SuperAdminPage extends Component {

    constructor(props){
		super(props);

        if (!config.getToken()){
            this.props.history.push("/");
        }

	}

    render() {
        return(
            <div id="content-admin">
                <Header />
                <Menu />
                <SuperAdminSections />
            </div>
        )
    }
}

export default SuperAdminPage;