import React, { Component } from 'react';
import '../../../sources/css/superadmin/menu.css';
import { Link, NavLink } from 'react-router-dom';

// import Profile from './profile';

class Menu extends Component {
    render() {
        return (
            <div className="left-menu">
                <span className="title1">
                    <i className="mdi-logo_nuevo"></i>
                </span>
                <nav className="navigation">
                    <NavLink to="/superadmin/accounts" href="#"  activeClassName="is-active" className="navigation-link">
                        <i className="mdi-group_add"></i>
                        <p>ACCOUNTS CONTROL</p>
                    </NavLink>
                    { /* <NavLink to="/superadmin/paymentplans" activeClassName="is-active" className="navigation-link" >
                        <i className="mdi-grid_on"></i>
                        <p>PAYMENT PLANS</p>
                    </NavLink>
                    <NavLink to="/superadmin/settings" activeClassName="is-active" className="navigation-link">
                        <i className="fa fa-cog" aria-hidden="true"></i>
                        <p>SETTINGS</p>
                    </NavLink> */ }
                    <NavLink  to="/superadmin/explore" activeClassName="active" className="navigation-link">
                        <i className="fa fa-search-plus" aria-hidden="true"></i>
                        <p>EXPLORE SAMPLES</p>
                    </NavLink>
                    <NavLink to="/superadmin/blog" activeClassName="active" className="navigation-link">
                        <i className="fa fa-rss" aria-hidden="true"></i>
                        <p>BLOG</p>
                    </NavLink>
                    <NavLink to="/superadmin/idea" activeClassName="active" className="navigation-link">
                        <i className="fa fa-lightbulb-o" aria-hidden="true"></i>
                        <p>IDEAS</p>
                    </NavLink>
                    <NavLink to="/superadmin/knowledge" activeClassName="active" className="navigation-link">
                        <i className="fa fa-info-circle" aria-hidden="true"></i>
                        <p>KNOWLEDGE BASE</p>
                    </NavLink>
                    <NavLink  to="/login" className="navigation-link"  activeClassName="is-active" >
                        <i className="fa fa-power-off"></i>
                        <p>EXIT</p>
                    </NavLink>
                </nav>
               {/*} <div className="modal fade" id="profileModalWindow" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <Profile />
                </div> */}
            </div>


        );
    }
}

export default Menu;
