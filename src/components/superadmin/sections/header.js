import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import nouser from '../../../sources/img/nouser.jpg';
import "../../../sources/css/superadmin/app.css";


class Header extends Component {
    render() {
        return (
            <header id="admin-header">
                <nav className="navbar navbar-expand-lg fixed-top navbar-light bg-faded" style={{ minHeight: 50 }}>
                           <div className="col">
                               <button className="navbar-toggler" type="button" data-toggle="collapse" 
                                      data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" 
                                      aria-expanded="false" aria-label="Toggle navigation">
                                   <span className="navbar-toggler-icon"></span>
                               </button>
                           </div>

                          {/* Mobile Nav */}

                            <div className="collapse navbar-collapse" id="navbarSupportedContent1">
                                <ul className="navbar-nav mr-auto">
                                  <li className="nav-item active">
                                    <NavLink to="/superadmin/accounts" href="#"  activeClassName="is-active" className="navigation-link">
                                        <i className="mdi-group_add"></i>
                                        <p>ACCOUNTS CONTROL</p>
                                    </NavLink>
                                  </li>
                                  <li className="nav-item">
                                    <NavLink to="/superadmin/payment" activeClassName="is-active" className="navigation-link" >
                                        <i className="mdi-grid_on"></i>
                                        <p>PAYMENT PLANS</p>
                                    </NavLink>
                                  </li>
                                  <li className="nav-item dropdown">
                                    <NavLink to="/superadmin/settings" activeClassName="is-active" className="navigation-link">
                                        <i className="fa fa-cog" aria-hidden="true"></i>
                                        <p>SETTINGS</p>
                                    </NavLink>
                                  </li>
                                  <li className="nav-item">
                                    <NavLink to="/superadmin/explore" activeClassName="active" className="navigation-link">
                                        <i className="fa fa-search-plus" aria-hidden="true"></i>
                                        <p>EXPLORE SAMPLES</p>
                                    </NavLink>
                                  </li>
                                  <li className="nav-item">
                                    <NavLink  to="/login" className="navigation-link"  activeClassName="is-active" >
                                        <i className="fa fa-power-off"></i>
                                        <p>EXIT</p>
                                    </NavLink>
                                  </li>
                                  <li className="text-center mt-3">
                                   {/* <div className="dropdown">
                                       <a className="drop btn btn-secondary dropdown-toggle"  id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                           Payment
                                       </a>
                                       <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                           <a className="dropdown-item" href="#">Option 1</a>
                                           <a className="dropdown-item" href="#">Option 2</a>
                                           <a className="dropdown-item pl-3" href="#">
                                              <button type="button" className="btn save-btn" data-toggle="modal" data-target="#confirmChanges">
                                                  Pay now
                                              </button>
                                           </a>
                                       </div>
                                   </div> */}
                                  </li>
                                </ul>
                              </div>

                            {/*End of Mobile Nav */}


                           {/* Profile link 

                           <div className="col" id="profileCol">
                               <div className="float-right">
                                   <div className="dropdown">
                                       <a className="drop btn btn-secondary dropdown-toggle"  id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                           <img className="inline user-logo"  src={nouser} alt="" />John Doe
                                       </a>
                                       <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                           <a className="dropdown-item" href="#">Profile</a>
                                           <a className="dropdown-item" href="#">Logout</a>
                                       </div>
                                   </div>
                               </div>
                           </div>

                         */} 

                           <div className="col" id="paymentCol">
                               <div className="float-right">
                                   {/* <div className="dropdown">
                                       <a className="drop btn btn-secondary dropdown-toggle"  id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                           Payment
                                       </a>
                                       <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                           <a className="dropdown-item" href="#">Option 1</a>
                                           <a className="dropdown-item" href="#">Option 2</a>
                                           <a className="dropdown-item pl-3" href="#">
                                              <button type="button" className="btn save-btn" data-toggle="modal" data-target="#confirmChanges">
                                                  Pay now
                                              </button>
                                           </a>
                                       </div>
                                   </div> */}
                               </div>
                           </div>
                   </nav>
            </header>

        );
    }
}

export default Header;