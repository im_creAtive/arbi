import React, { Component } from 'react';
import '../../../sources/css/superadmin/settings.css';

class Settings extends Component {
    render() {
        return(
        	<div id="settings">
	        	<div className="block mb-5">
	        	  <div className="title">
	        		<p>Vuforia key</p>
	        	  </div>
	        	  <form className="mt-3">
	        	    <div className="form-group">
					    <label>Access key</label>
					    <input type="password" className="form-control" placeholder="Password" />
					</div>
	        	    <div class="form-group">
					    <label>Security key</label>
					    <input type="password" className="form-control" placeholder="Password" />
					</div>
				  </form>
				</div>
	        	<div className="block">
	        	  <div className="title">
	        		<p>Payment system key</p>
	        	  </div>
	        	  <form className="mt-3">
	        	    <div className="form-group">
					    <label>Apy key</label>
					    <input type="password" className="form-control" placeholder="Password" />
					</div>
				  </form>
				</div>
                <div className="mt-5 text-center">
                	<button type="button" className="btn save-btn" data-toggle="modal" data-target="#confirmChanges">
                    	Save
                	</button>

					<div className="modal fade" id="confirmChanges" tabindex="-1" role="dialog" aria-labelledby="confirmChangesLabel" aria-hidden="true">
					  <div className="modal-dialog" role="document">
					    <div className="modal-content">
					      <div className="modal-body">
					        <p>Do you want to save changes?</p>
					      </div>
					      <div className="modal-footer">
                            <button type="button" className="btn save-btn" data-dismiss="modal">
                                Yes
                            </button>
                            <button type="button" className="btn cancel-btn" data-dismiss="modal">
                            	No
                            </button>
					      </div>
					    </div>
					  </div>
					</div>
                </div>
        	</div>
        )
    }
}

export default Settings;