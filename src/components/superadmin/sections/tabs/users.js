import React, { Component } from 'react';


class Users extends Component {
    render() {
    	return(
    	  <div id="tabUsers">	
			<div className="row">
                <div className="col-sm-12">
                    <div className="card-table">
                        <div className="pa-5" id="table-card">
                            <table className="table table-responsive">
                                <thead>
                                    <tr>
                                        <th>USERS</th>
                                        <th>LAST LOGGED IN</th>
                                        <th>SCANS USED</th>
                                        <th>ACTIVE/INACTIVE</th>
                                        <th>DELETE ACCOUNT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<tr>
					                    <td>KHALID BOUKHID</td>
					                    <td>30-05-2017</td>
					                    <td>54 SCANS</td>
					                    <td className="switcher">
					                        <label className="switch">
											  <input type="checkbox"/>
											  <span className="slider round"></span>
											</label>
					                    </td>
					                    <td>
					                        <button className="button-edit" data-toggle="modal" data-target="#modalNewUser" data-upgraded=",MaterialButton">
					                            <i className="fa fa-trash-o fa-2x" aria-hidden="true"></i>
					                        </button>
					                    </td>
					                </tr>
					                <tr>
					                    <td>KHALID BOUKHID</td>
					                    <td>30-05-2017</td>
					                    <td>54 SCANS</td>
					                    <td className="switcher">
					                        <label className="switch">
											  <input type="checkbox"/>
											  <span className="slider round"></span>
											</label>
					                    </td>
					                    <td>
					                        <button className="button-edit" data-toggle="modal" data-target="#modalNewUser" data-upgraded=",MaterialButton">
					                            <i className="fa fa-trash-o fa-2x" aria-hidden="true"></i>
					                        </button>
					                    </td>
					                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
			</div>
		  </div>
    	)
    }
}

export default Users;