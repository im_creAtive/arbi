import React, { Component } from 'react';
import car from '../../../../sources/img/car.jpg';
import obj from '../../../../sources/img/obj.png';
import unity from '../../../../sources/img/unity.png';


class Models extends Component {
    render() {
    	return(
    		<div id="tabModels">
    			<div className="row ml-0 mr-0 foldersCol">
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<p>
    						<i className="fa fa-folder" aria-hidden="true"></i>
    						ALL MODELS
    					</p>
    				</div>
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<p>
    						<i className="fa fa-folder" aria-hidden="true"></i>
    						3D MODELS
    					</p>
    				</div>
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<p>
    						<i className="fa fa-folder" aria-hidden="true"></i>
    						IMAGE TRACKERS
    					</p>
    				</div>
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<p>
    						<i className="fa fa-folder" aria-hidden="true"></i>
    						UNITY 3D
    					</p>
    				</div>
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<p>
    						<i className="fa fa-folder-open" aria-hidden="true"></i>
    						NEW FOLDER
    					</p>
    				</div>
    			</div>
    			<div className="row ml-0 mr-0 mt-5 imagesRow">
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<div className="albumDiv">
    						<img className="img-fluid" src={car} />
    						<div>
    							<p>Dodge Challenger</p>
	    						<span>
	    							<img src={obj} />
	    							<i className="fa fa-trash-o fa-lg" aria-hidden="true"></i>
	    						</span>
	    					</div>
    					</div>
    				</div>
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<div className="albumDiv">
    						<img className="img-fluid" src={car} />
    						<div>
    							<p>Dodge Challenger</p>
	    						<span>
	    							<img src={obj} />
	    							<i className="fa fa-trash-o fa-lg" aria-hidden="true"></i>
	    						</span>
	    					</div>
    					</div>
    				</div>    				
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<div className="albumDiv">
    						<img className="img-fluid" src={car} />
    						<div>
    							<p>Dodge Challenger</p>
	    						<span>
	    							<img src={obj} />
	    							<i className="fa fa-trash-o fa-lg" aria-hidden="true"></i>
	    						</span>
	    					</div>
    					</div>
    				</div>    				
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<div className="albumDiv">
    						<img className="img-fluid" src={car} />
    						<div>
    							<p>Dodge Challenger</p>
	    						<span>
	    							<img src={obj} />
	    							<i className="fa fa-trash-o fa-lg" aria-hidden="true"></i>
	    						</span>
	    					</div>
    					</div>
    				</div>    				
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<div className="albumDiv">
    						<img className="img-fluid" src={car} />
    						<div>
    							<p>Dodge Challenger</p>
	    						<span>
	    							<img src={obj} />
	    							<i className="fa fa-trash-o fa-lg" aria-hidden="true"></i>
	    						</span>
	    					</div>
    					</div>
    				</div>    		
    			</div>
    			<div className="row ml-0 mr-0 mt-5 imagesRow">
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<div className="albumDiv">
    						<img className="img-fluid" src={car} />
    						<div>
    							<p>Dodge Challenger</p>
	    						<span>
	    							<img src={unity} />
	    							<i className="fa fa-trash-o fa-lg ml-0" aria-hidden="true"></i>
	    						</span>
	    					</div>
    					</div>
    				</div>
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<div className="albumDiv">
    						<img className="img-fluid" src={car} />
    						<div>
    							<p>Dodge Challenger</p>
	    						<span>
	    							<img src={unity} />
	    							<i className="fa fa-trash-o fa-lg ml-0" aria-hidden="true"></i>
	    						</span>
	    					</div>
    					</div>
    				</div>    				
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<div className="albumDiv">
    						<img className="img-fluid" src={car} />
    						<div>
    							<p>Dodge Challenger</p>
	    						<span>
	    							<img className="img-fluid" src={unity} />
	    							<i className="fa fa-trash-o fa-lg ml-0" aria-hidden="true"></i>
	    						</span>
	    					</div>
    					</div>
    				</div>    				
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<div className="albumDiv">
    						<img className="img-fluid" src={car} />
    						<div>
    							<p>Dodge Challenger</p>
	    						<span>
	    							<img src={unity} />
	    							<i className="fa fa-trash-o fa-lg ml-0" aria-hidden="true"></i>
	    						</span>
	    					</div>
    					</div>
    				</div>    				
    				<div className="col-md-2 ml-auto mr-auto pl-0 pr-0">
    					<div className="albumDiv">
    						<img className="img-fluid" src={car} />
    						<div>
    							<p>Dodge Challenger</p>
	    						<span>
	    							<img src={unity} />
	    							<i className="fa fa-trash-o fa-lg ml-0" aria-hidden="true"></i>
	    						</span>
	    					</div>
    					</div>
    				</div>    		
    			</div>
			</div>
    	)
    }
}

export default Models;