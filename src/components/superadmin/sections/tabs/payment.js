import React, { Component } from 'react';
import * as paymentAction from './../../../../actions/paymentAction';
import { connect } from 'react-redux';

class Payment extends Component {
	
	componentDidMount(){
		paymentAction.get();
	}

    render() {
		let payments = this.props.store.payments.bundle.History.length ? this.props.store.payments.bundle.History.map((payment, index) => {
			debugger
			return (
				<div >
					<p>Type: {payment.PaymentType}</p>
					<p>Payment plan name: {payment.PaymentPlanName}</p>
					<p>Payment plan price: {payment.PaymentPlanPrice}</p>
					<p>Payment system: {payment.PaymentSystem}</p>
					<p className="h">€ {payment.Amount}</p>
					<p>Create date: {payment.CreateDate}</p>
					<br/>
				</div>
			);
		}) : null;

    	return(
    		<div id="tabPayment">
	    		<div className="paymentDiv">
	    			<p className="h">Balance</p>
	    			<div className="p">
	    				<p>Current balance</p>
	    				<p className="h">€ {this.props.store.payments.bundle.Balance.Total - this.props.store.payments.bundle.Balance.Used}</p>
	    			</div>
    			</div>
	    		<div className="paymentDiv">
	    			<p className="h">Payment history</p>
	    			{payments}
    			</div>
    		</div>
    	)
    }
}

export default connect(store => ({ store }))(Payment);