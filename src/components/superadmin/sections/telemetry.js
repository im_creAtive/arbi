import React, { Component } from 'react';

import "../../../sources/css/superadmin/app.css";
import "../../../sources/css/superadmin/users.css";


class Telemetry extends Component {
    render() {
        return (
            <div className="row">
                <div className="modal fade" id="modalAddUser" tabIndex="-1" role="dialog" 
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h2 className="modal-title" id="exampleModalLabel">User Info</h2>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>FIRST NAME</label>
                                        <input type="text" name="first-name" className="form-control"/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>LAST NAME</label>
                                        <input type="text" name="last-name"  className="form-control"/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>EMAIL</label>
                                        <input type="email" name="email" className="form-control"/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>PASSWORD</label>
                                        <input type="password" name="password" className="form-control" />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="col-sm-12 mdl-input p-0">
                                        <label>CONFIRM PASSWORD</label>
                                        <input type="password" name="re-password" className="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary save-btn" 
                                        data-dismiss="modal">
                                    Save
                                </button>
                                <button type="button" className="btn btn-primary cancel-btn">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default Telemetry;