import React, { Component } from 'react';
// import Users from './tabs/users';
import Users from '../../cabinet/sections/users';
// import Models from './tabs/models';
import Models from '../../cabinet/sections/library'
import Payment from './tabs/payment';
import '../../../sources/css/superadmin/tabs.css';
import config from '../../../api/config';

import * as userAction from '../../../actions/userAction';


class UserTabs extends Component {

    constructor(props){
      super(props);

      // this.props.match.params.id
      config.setSelectedUserId(this.props.match.params.id);
    }

    componentDidMount(){
		userAction.getCurrent();
	}

    componentWillUnmount(){
      config.delSelectedUserId();
    }

    render() {

    	return(
            <div className="userTabs">
                <ul className="nav nav-tabs nav-list" role="tablist">
                    <li className="nav-item col">
                        <a className="nav-link active" data-toggle="tab" href="#users" role="tab">
                        	USERS
                        </a>
                    </li>
                    <li className="nav-item col">
                        <a className="nav-link" data-toggle="tab" href="#models" role="tab">
                        	MODELS
                        </a>
                    </li>
                    <li className="nav-item col">
                        <a className="nav-link" data-toggle="tab" href="#payment" role="tab">
                        	PAYMENT
                        </a>
                    </li>
                </ul>
                <div className="tab-content">
                    <div className="tab-pane active" id="users" role="tabpanel">
                    	<Users />
                    </div>

                    <div className="tab-pane" id="models" role="tabpanel">
                    	<Models />
                    </div>

                    <div className="tab-pane" id="payment" role="tabpanel">
                    	<Payment />
                    </div>
                </div>
            </div>
    	)
    }
}

export default UserTabs;
