import React, { Component } from 'react';
import '../../../sources/css/superadmin/payment.css';
import * as paymentPlanAction from './../../../actions/paymentPlanAction';
import { connect } from 'react-redux';


class PaymentPlans extends Component{

	constructor(props){
		super(props);
		this.state = {
				edit: this.defValue() 
		};
}

	defValue(){
		return {
			Id: null,
			Name: 0,
			Type: 0,
			Price: 0,
			Duration: 0,
			IsDefault: 0,
			IsDisplayed: 0,
			Spaces: 0,
			Users: 0
		}
	}

	componentDidMount(){
		paymentPlanAction.getTypes();
		paymentPlanAction.get();
	}

	onLoadEdit(id){
		paymentPlanAction.getById(id).then(paymentPlan => {
			this.state.edit = paymentPlan;
			this.setState(this.state);
		}, error => {
			debugger
		});
	}

	onChnage(value, prop){
		this.state.edit[prop] = value;
		this.setState(this.state);
	}

	onSave(){
		if (!this.state.edit.Id){
			paymentPlanAction.add(this.state.edit).then(ok=>{ 
				paymentPlanAction.get();
				this.onCancel();
			}, error => {
				debugger
			});
		}else{
			paymentPlanAction.modify(this.state.edit.Id, this.state.edit).then(ok=>{ 
				paymentPlanAction.get();
				this.onCancel();
			}, error => {
				debugger
			});
		}
	}

	onCancel (){
		this.state.edit = this.defValue();
		this.setState(this.state);
	}

	render(){
		var paymentPlanTypes = this.props.store.paymentPlans.types.map((type, index) => {
			return (
					<option value={type}>
						{type}
					</option>
			)}
		);

		var paymentPlans = this.props.store.paymentPlans.list.map(function (paymentPlan, index) {
			return paymentPlan.Id != this.state.edit.Id ? (
					<tr>
							<td>{paymentPlan.Name}</td>
							<td>{paymentPlan.Type}</td>
							<td>{paymentPlan.Price}</td>
							<td>{paymentPlan.Duration}</td>
							<td>{paymentPlan.IsDefault ? "This" : ""}</td>
							<td>{paymentPlan.IsDisplayed ? "Show" : "Hidden"}</td>
							<td>{paymentPlan.Spaces}</td>
							<td>{paymentPlan.Users}</td>
							<td><a onClick={()=>{this.onLoadEdit(paymentPlan.Id)}}>Edit</a></td>
					</tr>
			) : (
					<tr>
						<td>
							<div className="input-group">
								<input type="text" value={this.state.edit.Name} onChange={e=>{this.onChnage(e.target.value, 'Name')}} className="form-control" />
							</div>
						</td>
						<td>
								<select value={this.state.edit.Type} onChange={e=>{this.onChnage(e[e.target.selectedIndex], 'Type')}}>
										{paymentPlanTypes}
								</select>
						</td>
						<td>
							<div className="input-group">
								<input type="number" value={this.state.edit.Price} onChange={e=>{this.onChnage(e.target.value, 'Price')}} className="form-control" />
							</div>
						</td>
						<td>
							<div className="input-group">
								<input type="number" value={this.state.edit.Duration} onChange={e=>{this.onChnage(e.target.value, 'Duration')}} className="form-control" />
							</div>
						</td>
						<td className="switcher">
								<label className="switch">
									<input type="checkbox" value={this.state.edit.IsDefault} onChange={e=>{this.onChnage(e.target.checked, 'IsDefault')}}/>
									<span className="slider round"></span>
								</label>
						</td>
						<td className="switcher">
								<label className="switch" value={this.state.edit.IsDisplayed} onChange={e=>{this.onChnage(e.target.checked, 'IsDisplayed')}}>
								<input type="checkbox"/>
								<span className="slider round"></span>
							</label>
						</td>
						<td>
							<div className="input-group">
								<input type="number" className="form-control" value={this.state.edit.Spaces} onChange={e=>{this.onChnage(e.target.value, 'Spaces')}} />
							</div>
						</td>
						<td>
							<div className="input-group">
								<input type="number" className="form-control" value={this.state.edit.Users} onChange={e=>{this.onChnage(e.target.value, 'Users')}}/>
							</div>
						</td>
						<td>
							<a onClick={this.onSave.bind(this)}>Save</a>
							<a onClick={this.onCancel.bind(this)}>Cancel</a>
						</td>
				</tr>
			);
		}.bind(this));

		return(
		  <div className="container" id="payment">
			<div className="row">
                <div className="col-sm-12">
                    <div className="card-table">
                        <div className="pa-5" id="table-card">
                            <table className="table table-responsive">
                                <thead>
                                    <tr>
                                        <th>NAME</th>
                                        <th>TYPE</th>
                                        <th>PRICE</th>
                                        <th>DURATION</th>
                                        <th>DEFAULT</th>
                                        <th>DISPLAY</th>
                                        <th>SPACE</th>
                                        <th>SCANS</th>
                                    </tr>
                                </thead>
                                <tbody>
																	{paymentPlans}
																
                                </tbody>
                            </table>
                        </div>


                        <div className="pt-5 pb-5 text-center">
	                	<button type="button" className="btn save-btn" data-toggle="modal" data-target="#confirmChanges">
	                    	Save
	                	</button>
							<div className="modal fade" id="confirmChanges" tabindex="-1" role="dialog" aria-labelledby="confirmChangesLabel" aria-hidden="true">
								<div className="modal-dialog" role="document">
									<div className="modal-content">
										<div className="modal-body">
											<p>Do you want to save changes?</p>
										</div>
										<div className="modal-footer">
																<button type="button" className="btn save-btn" data-dismiss="modal">
																		Yes
																</button>
																<button type="button" className="btn cancel-btn" data-dismiss="modal">
																	No
																</button>
										</div>
									</div>
								</div>
							</div>
						</div>
							</div>
					</div>
			</div>
		  </div>
		)
	}
}
export default connect(store => ({ store }))(PaymentPlans);