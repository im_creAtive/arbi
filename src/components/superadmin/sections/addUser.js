import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as authorizationAction from '../../../actions/authorizationAction';
import * as accountAction from '../../../actions/accountAction';
import * as notification from '../../../helpers/notificationHelper';
import * as usersActions from '../../../actions/userAction';
import * as rolesActions from '../../../actions/roleAction';

const defState = {
  FirstName: "",
  LastName: "",
  Email: "",
  Password: "",
  PasswordConfirm: "",
  IsActive: true,
  IsEnable: true,
  PhoneNumber: "",
}

class AddUser extends Component {

    state = {...defState}

    componentDidMount(){
      rolesActions.get()

      window.addEventListener('editUser', e => {
        console.log('editUser', e)
        this.setState(e.detail ? { ...e.detail } : {...defState})
      })
    }

    async onSubmit (){
      let user = {...this.state}

      user.RoleIds = this.props.roles.list.map(role => role.Name);

      try{
        if(user.Id){
          if(user.Password && user.Password === ''){
            delete user.Password
            delete user.PasswordConfirm
          }

          let response = await usersActions.modify(user.Id, user)

          console.log(response);
          if(response.Message) throw new Error(response.Message);

          notification.success('Account successfully updated')
        } else {
          let response = await usersActions.createByAccount(user);
          if(response.Message) throw new Error(response.Message);

          notification.success('Account successfully created')
        }

        this.setState({ ...defState })
        window.$('#modalAddUser').modal('hide')
        accountAction.get();
      } catch(e) {
        try {
          e = JSON.parse(e)
        } catch (e) {

        }
        console.log(e)
        notification.error(e.Message || e.message || 'Unknown error')
      }
    }

    render(){

        return (
          <div className="modal fade" id="modalAddUser" tabIndex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
             <div className="modal-dialog" role="document">
                <div className="modal-content">
                   <div className="col-sm-12">
                      <div className="modal-header">
                         <h2 className="modal-title" id="exampleModalLabel">
                          { this.state.Id ? 'EDIT ACCOUNT' : 'ADD NEW ACCOUNT' }
                         </h2>
                      </div>
                      <div className="modal-body">
                        <div className="form-group">
                           <div className="col-sm-12 mdl-input p-0">
                              <label>FIRST NAME</label>
                              <input
                                type="text"
                                name="first-name"
                                className="form-control"
                                value={this.state.FirstName}
                                onChange={e => this.setState({ FirstName: e.target.value })}
                              />
                           </div>
                        </div>
                        <div className="form-group">
                           <div className="col-sm-12 mdl-input p-0">
                              <label>LAST NAME</label>
                              <input
                                type="text"
                                name="first-name"
                                className="form-control"
                                value={this.state.LastName}
                                onChange={e => this.setState({ LastName: e.target.value })}
                              />
                           </div>
                        </div>
                        <div className="form-group">
                            <div className="col-sm-12 mdl-input p-0">
                              <label>EMAIL</label>
                              <input
                                type="email"
                                name="email"
                                className="form-control"
                                value={this.state.Email}
                                onChange={e => this.setState({ Email: e.target.value })}
                              />
                            </div>
                         </div>
                         <div className="form-group">
                            <div className="row">
                              <div className="col-4">
                                <span className="superadmin-user-passwords">CREATE PASSWORD</span>
                              </div>
                              <div className="col-8">
                                <input
                                  className="form-control"
                                  value={this.state.Password}
                                  onChange={e => this.setState({ Password: e.target.value })}
                                  type="password"
                                />
                              </div>
                            </div>
                         </div>
                         <div className="form-group">
                            <div className="row">
                              <div className="col-4">
                                <span className="superadmin-user-passwords">REPEAT PASSWORD</span>
                              </div>
                              <div className="col-8">
                                <input
                                  type="password"
                                  className="form-control"
                                  value={this.state.PasswordConfirm}
                                  onChange={e => this.setState({ PasswordConfirm: e.target.value })}
                                />
                              </div>
                            </div>
                         </div>
                      </div>
                      <div className="modal-footer">
                        <button type="button" className="btn save-btn" onClick={this.onSubmit.bind(this)}>
                          Save
                        </button>
                        <button type="button" className="btn cancel-btn" data-dismiss="modal">
                          Cancel
                        </button>
                      </div>
                   </div>
                </div>
             </div>
          </div>
        )
    }
}

export default connect(state => ({ roles: state.roles, paymentPlans: state.paymentPlans }))(AddUser);
