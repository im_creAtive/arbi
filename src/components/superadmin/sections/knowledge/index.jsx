import React, { Component } from 'react'
import { connect } from 'react-redux'
import dateFormat from 'dateformat'
import { NotificationManager } from 'react-notifications'

import KnowledgeModal from './modal'
import * as knowledgeActions from '../../../../actions/knowledgeAction'

class Knowledge extends Component {

  state = {
    search: ''
  }

  componentDidMount() {
    knowledgeActions.get()
  }

  async onDeletePost(id) {
    try {
      await knowledgeActions.remove(id);

      NotificationManager.success('Post successfully deleted')
      knowledgeActions.get()
    } catch (e) {
      console.log(e)
      NotificationManager.error(e.message)
    }
  }

  filter(){
    let {search} = this.state;
    search = search.toLowerCase();

    if (this.state.search === "") return this.props.knowledge.list;

    return this.props.knowledge.list.filter(post => {
      return post.TitleOfArticle.toLowerCase().indexOf(search) > -1 || post.DiscriptionOfArticle.toLowerCase().indexOf(search) > -1
    })
  }

  render() {

    const posts = this.filter().map((post, index) => {
      return (
        <tr key={index}>
          <td style={{ textAlign: 'left' }}>{post.TitleOfArticle}</td>
          <td style={tr}>{post.DiscriptionOfArticle}</td>
          <td>
            <KnowledgeModal post={post} />
            <button className="button-edit" onClick={() => this.onDeletePost(post.Id)} data-upgraded=",MaterialButton">
              <i className="fa fa-trash-o fa-2x" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
      )
    })

    return (
      <div className="container" id="superAdminAccounts">

        <div className="row">
          <div className="col-md-4">
            <KnowledgeModal />
          </div>
          <div className="col-md-8">
            <div className="input-group" id="searchUsers">
              <span className="input-group-addon" id="basic-addon1">
                <i className="fa fa-search" aria-hidden="true"></i>
              </span>
              <input 
                type="text" 
                className="form-control" 
                placeholder="Search knowledge post by title and description"
                aria-label="Username" 
                aria-describedby="basic-addon1"
                value={this.state.search}
                onChange={e => this.setState({ search: e.target.value })} />
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-sm-12">
            <div className="card-table">
              <div className="pa-5" id="table-card">
                <table className="table table-responsive">
                  <thead>
                    <tr>
                      <th>TITLE</th>
                      <th>DESCRIPTION</th>
                      <th>ACTIONS</th>
                    </tr>
                  </thead>
                  <tbody>
                    {posts}
                  </tbody>
                </table>

                { posts.length === 0 && (
                  <p className="empty-text-arbi"><i className="fa fa-info" aria-hidden="true"></i> Knowledge post list is empty</p>
                ) }
              </div>
            </div>
          </div>
        </div>

      </div>
    )
  }

}

const tr = {
  maxWidth: 300,
  textAlign: 'left'
}


export default connect(state => ({ knowledge: state.knowledge }))(Knowledge);