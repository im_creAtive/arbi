import React, { Component } from 'react'
import { NotificationManager } from 'react-notifications'

import * as knowledgeActions from '../../../../actions/knowledgeAction'

const defState = {
  TitleOfArticle: "",
  DiscriptionOfArticle: "",
  ArticleText: "",
}

class KnowledgeModal extends Component {

  state = { ...defState }

  constructor(props) {
    super(props)

    this.state = this.props.post ? { ...this.props.post } : { ...defState }
  }

  async onSubmit() {
    let post = Object.assign({}, this.state)

    try {
      if (post.TitleOfArticle === '') throw new Error('Title is required field')
      if (post.ArticleText === '') throw new Error('Text is required field')

      if (this.props.post) {
        let response = await knowledgeActions.modify(post)

        window.$("#modalBlog-" + this.props.post.Id).modal('hide');
        this.setState({ ...response })
        NotificationManager.success('Post successfully updated')
      } else {
        let response = await knowledgeActions.add(post)

        window.$("#modalBlog").modal('hide');
        this.setState({ ...defState })
        NotificationManager.success('Post successfully created')
      }

      knowledgeActions.get()
    } catch (e) {
      console.log(e)
      NotificationManager.error(e.message)
    }
  }

  render() {

    const modal = (
      <div className="modal fade" id={"modalBlog" + (this.props.post ? '-' + this.props.post.Id : '')} tabIndex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document" style={{ textAlign: 'left' }} onClick={e => e.stopPropagation()}>
          <div className="modal-content">
            <div className="col-sm-12">
              <div className="modal-header">
                <h2 className="modal-title" id="exampleModalLabel">Knowledge post</h2>
              </div>
              <div className="modal-body">
                <div className="form-group">
                  <div className="col-sm-12 mdl-input p-0">
                    <label>TITLE</label>
                    <input
                      type="text"
                      name="first-name"
                      className="form-control"
                      value={this.state.TitleOfArticle}
                      onChange={e => this.setState({ TitleOfArticle: e.target.value })}
                    />
                  </div>
                </div>
                <div className="form-group">
                  <div className="col-sm-12 mdl-input p-0">
                    <label>DESCRIPRION</label>
                    <textarea
                      className="form-control"
                      value={this.state.DiscriptionOfArticle}
                      onChange={e => this.setState({ DiscriptionOfArticle: e.target.value })}
                    ></textarea>
                  </div>
                </div>
                <div className="form-group">
                  <div className="col-sm-12 mdl-input p-0">
                    <label>TEXT</label>
                    <textarea
                      className="form-control"
                      value={this.state.ArticleText}
                      onChange={e => this.setState({ ArticleText: e.target.value })}
                      style={{ minHeight: 200 }}
                    ></textarea>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn save-btn" onClick={this.onSubmit.bind(this)} style={{ color: '#fff' }}>Save</button>
                <button type="button" className="btn cancel-btn" data-dismiss="modal" style={{ color: '#fff' }}>Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )

    return this.props.post ? (
      <button className="button-edit" data-upgraded=",MaterialButton" onClick={() => window.$('#modalBlog-' + this.props.post.Id).modal('show')}>
        <i className="fa fa-pencil fa-2x" aria-hidden="true"></i>
        {modal}
      </button>
    ) : (
        <div>
          <div className="input-group" id="addAccount" data-toggle="modal" data-target="#modalBlog">
            <button type="button">
              <i className="fa fa-plus" aria-hidden="true"></i>
            </button>
            <input type="text" className="form-control" placeholder="Add post"
              aria-label="Username" aria-describedby="basic-addon1" disabled />
          </div>

          {modal}
        </div>
      )
  }

}

export default KnowledgeModal;