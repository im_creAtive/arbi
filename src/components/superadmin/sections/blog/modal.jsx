import React, { Component } from 'react'
import { NotificationManager } from 'react-notifications'

import * as blogActions from '../../../../actions/blogAction'
import config from '../../../../api/config'

const defState = {
  NameOfArticle: "",
  DiscriptionOfArticle: "",
  ArticleText: "",
}

class BlogModal extends Component {

  state = {...defState}

  constructor(props){
    super(props)

    this.state = this.props.post ? {...this.props.post} : {...defState}
  }

  async onSubmit(){
    let post = Object.assign({}, this.state)

    try{
      if (post.NameOfArticle === '') throw new Error('Name is required field')
      if (post.ArticleText === '') throw new Error('Text is required field')

      if (this.props.post){
        let response = await blogActions.modify(post)
        if (this.file) {
          response = await blogActions.uploadFile(response.Id, this.file)
          this.file = null;
          this.fileUrl = null;
        }

        window.$("#modalBlog-" + this.props.post.Id).modal('hide');
        this.setState({ ...response })
        NotificationManager.success('Post successfully updated')
      } else {
        let response = await blogActions.add(post)
        if (this.file) {
          response = await blogActions.uploadFile(response.Id, this.file)
          this.file = null;
          this.fileUrl = null;
        }

        window.$("#modalBlog").modal('hide');
        this.setState({ ...defState })
        NotificationManager.success('Post successfully created')
      }

      blogActions.get()
    } catch(e) {
      console.log(e)
      NotificationManager.error(e.message)
    }
  }

  onChangeFile(e){
    if(e.target.files.length < 1) return;

    this.file = e.target.files[0];
    e.target.value = null;

    this.forceUpdate();

    var reader = new FileReader();
    reader.readAsDataURL(this.file);
    reader.onload = () => {
      this.fileUrl = reader.result;
      this.forceUpdate();
    }
    reader.onerror = e => {
      NotificationManager.error(e.message)
    }
  }

  render() {

    const modal = (
      <div className="modal fade" id={"modalBlog" + (this.props.post ? '-' + this.props.post.Id : '')} tabIndex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document" style={{ textAlign: 'left' }} onClick={e => e.stopPropagation()}>
          <div className="modal-content">
            <div className="col-sm-12">
              <div className="modal-header">
                <h2 className="modal-title" id="exampleModalLabel">Blog post</h2>
              </div>
              <div className="modal-body">
                <div className="form-group">
                  <div className="col-sm-12 mdl-input p-0">
                    <label>NAME OF ATRICLE</label>
                    <input
                      type="text"
                      name="first-name"
                      className="form-control"
                      value={this.state.NameOfArticle}
                      onChange={e => this.setState({ NameOfArticle: e.target.value })}
                    />
                  </div>
                </div>
                <div className="form-group">
                  <div className="col-sm-12 mdl-input p-0">
                    <label>DESCRIPRION</label>
                    <textarea
                      className="form-control"
                      value={this.state.DiscriptionOfArticle}
                      onChange={e => this.setState({ DiscriptionOfArticle: e.target.value })}
                    ></textarea>
                  </div>
                </div>
                <div className="form-group">
                  <div className="col-sm-12 mdl-input p-0">
                    <label>TEXT</label>
                    <textarea
                      className="form-control"
                      value={this.state.ArticleText}
                      onChange={e => this.setState({ ArticleText: e.target.value })}
                      style={{ minHeight: 200 }}
                    ></textarea>
                  </div>
                </div>
                <div className="form-group">
                  <div className="col-sm-12 mdl-input p-0">
                    <label>IMAGE</label>
                    {this.fileUrl && (
                      <img src={this.fileUrl} alt="upload" style={{ maxWidth: '100%', width: 200, display: 'block', margin: '15px auto' }} />
                    )}
                    {(this.state.Image && !this.fileUrl) && (
                      <img src={config.baseHost + 'Files/Download' + this.state.Image + '&type=BlogArticle'} alt="upload" style={{ maxWidth: '100%', width: 200, display: 'block', margin: '15px auto' }} />
                    )}
                    <input onChange={e => this.onChangeFile(e)} type="file" className="form-control" />
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn save-btn" onClick={this.onSubmit.bind(this)} style={{ color: '#fff' }}>Save</button>
                <button type="button" className="btn cancel-btn" data-dismiss="modal" style={{ color: '#fff' }}>Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )

    return this.props.post ? (
      <button className="button-edit" data-upgraded=",MaterialButton" onClick={() => window.$('#modalBlog-' + this.props.post.Id).modal('show')}>
        <i className="fa fa-pencil fa-2x" aria-hidden="true"></i>
        {modal}
      </button>
    ) : (
      <div>
        <div className="input-group" id="addAccount" data-toggle="modal" data-target="#modalBlog">
          <button type="button">
            <i className="fa fa-plus" aria-hidden="true"></i>
          </button>
          <input type="text" className="form-control" placeholder="Add post"
            aria-label="Username" aria-describedby="basic-addon1" disabled />
        </div>

        {modal}
      </div>
    )
  }

}

export default BlogModal;