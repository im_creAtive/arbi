import React, { Component } from 'react';
import '../../../sources/css/superadmin/accounts.css';
import AddUser from './addUser';
import { Link, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import {NotificationManager} from 'react-notifications';
import dateFormat from 'dateformat';

import * as accountAction from '../../../actions/accountAction';
import * as paymentPlanAction from '../../../actions/paymentPlanAction';
import * as userAction from '../../../actions/userAction';

class Accounts extends Component {

  state = { search: '' }

	componentDidMount(){
		accountAction.get();
		paymentPlanAction.get();
	}

	async onChangeAccountActive(id, isActive){
		try{
			let user = await userAction.modifyStatus(id, isActive);
			NotificationManager.success('Status changed successfully');
			accountAction.get();
		} catch(e) {
			NotificationManager.error(e.message || 'Unknown error');
			console.error(e);
		}
	}

	async onChangeAccountPaymentPlan(id, paymentPlanId){
		try{
			let user = await accountAction.modifyPaymentPlan(id, paymentPlanId);
			NotificationManager.success('Payment plan changed');
			accountAction.get();
		} catch(e) {
			NotificationManager.success(e.message);
			console.error(e);
		}
  }
  
  async onDeleteAccount(id, userId){
    if(!window.confirm('Delete user?')) return;

    try{
      let response = await accountAction.invisible(id, userId);

      if(response.Message) throw new Error(response.Message);

      NotificationManager.success('Account successfully deleted');
      accountAction.get();
    } catch(e) {
      console.log(e)
      NotificationManager.error(e.message || 'Unknown error')
    }
  }

  onEditUser(id){
    const event = new CustomEvent('editUser', { 'detail': id });
    window.dispatchEvent(event)
  }

  filter(){
    if(this.state.search === "") return this.props.accounts.list;

    return this.props.accounts.list.filter(account => {
      return (
        account.User.FirstName.indexOf(this.state.search) > -1 ||
        account.User.LastName.indexOf(this.state.search) > -1 ||
        account.User.Email.indexOf(this.state.search) > -1
      )
    })
  }

	render(){

		const accounts = this.filter().map((account, index) => {
			const paymentPlans = this.props.paymentPlans.list.map((paymentType, index) => {
				return (
					<option
						className={"dropdown-item " + (account.PaymentPlanId === paymentType.Id ? 'active' : '')}
						href="/"
						key={index}
						value={paymentType.Id}
					>
						{paymentType.Name}
					</option>
				)
			})

			return (
				<tr key={index}>
				   <td>
				      <NavLink style={{ textAlign: 'left' }} to={"/superadmin/accounts/users/" + account.UserId} activeClassName="is-active" className="navigation-link">
				         {account.User.FirstName} {account.User.LastName}
				      </NavLink>
           </td>
           <td>
            <button 
              className="button-edit" 
              data-toggle="modal" 
              data-target="#modalAddUser" 
              data-upgraded=",MaterialButton"
              onClick={e => this.onEditUser(account.User)}>
              <i className="fa fa-pencil fa-2x" aria-hidden="true" style={{ color: '#57386d', cursor: 'pointer' }}></i>
            </button>
           </td>
				   <td>{account.User.LastLogged ? dateFormat(account.User.LastLogged, 'dd.mm.yyyy HH:MM') : 'never'}</td>
				   <td className="switcher">
				      <label className="switch">
				      <input
								type="checkbox"
								checked={ account.IsActive }
								onChange={e => this.onChangeAccountActive(account.User.Id, e.target.checked) }
							/>
				      <span className="slider round"></span>
				      </label>
				   </td>
				   { /* <td>
				      <div className="">
	
				         <select onChange={e=>{this.onChangeAccountPaymentPlan(account.Id, e.target.value)}} className="" aria-labelledby="dropdownMenuButton">
				            {paymentPlans}
				         </select>
				      </div>
           </td> */ }
				   <td>
				      <button onClick={() => this.onDeleteAccount(account.Id, account.UserId)} className="button-edit">
				        <i className="fa fa-trash-o fa-2x" aria-hidden="true"></i>
				      </button>
				   </td>
				</tr>
			)
		})

		return(
			<div className="container" id="superAdminAccounts">
			   <div className="row">
			      <div className="col-md-4">
			         <div onClick={() => this.onEditUser(null)} className="input-group" id="addAccount" data-toggle="modal" data-target="#modalAddUser">
			            <button type="button">
			            <i className="fa fa-plus" aria-hidden="true"></i>
			            </button>
			            <input type="text" className="form-control" placeholder="Add account"
			               aria-label="Username" aria-describedby="basic-addon1" disabled />
			         </div>
			      </div>
			      <div className="col-md-8">
			         <div className="input-group" id="searchUsers">
			            <span className="input-group-addon" id="basic-addon1">
			            <i className="fa fa-search" aria-hidden="true"></i>
			            </span>
                  <input 
                    value={this.state.search}
                    onChange={e => this.setState({ search: e.target.value })}
                    type="text" 
                    className="form-control" 
                    placeholder="Search users by first name, last name and eamil"
                    aria-label="Username" 
                    aria-describedby="basic-addon1" />
			         </div>
			      </div>
			      <AddUser/>
			   </div>
			   <div className="row">
			      <div className="col-sm-12">
			         <div className="card-table">
			            <div className="pa-5" id="table-card">
			               <table className="table table-responsive">
			                  <thead>
			                     <tr>
                              <th>ACCOUNT</th>
                              <th>EDIT</th>
			                        <th>LAST LOGGED IN</th>
			                        <th>ACTIVE/INACTIVE</th>
			                        { /* <th>PLAN TYPE</th> */ }
			                        <th>DELETE ACCOUNT</th>
			                     </tr>
			                  </thead>
			                  <tbody>
													{accounts}
			                  </tbody>
			               </table>
			            </div>
			         </div>
			      </div>
			   </div>
			</div>
		)
	}
}

export default connect(state => ({ accounts: state.accounts, paymentPlans: state.paymentPlans }))(Accounts);
