import React, { Component } from 'react'
import { connect } from 'react-redux'
import dateFormat from 'dateformat'
import { NotificationManager } from 'react-notifications'

import IdeaModal from './modal'
import * as ideaActions from '../../../../actions/ideaAction'

class Idea extends Component {

  state = {
    search: ''
  }

  componentDidMount() {
    ideaActions.get()
  }

  async onDeletePost(id) {
    try {
      await ideaActions.remove(id);

      NotificationManager.success('Post successfully deleted')
      ideaActions.get()
    } catch (e) {
      console.log(e)
      NotificationManager.error(e.message)
    }
  }

  filter() {
    let { search } = this.state;
    search = search.toLowerCase();

    if (this.state.search === "") return this.props.idea.list;

    return this.props.idea.list.filter(post => {
      return post.TitleOfIdea.toLowerCase().indexOf(search) > -1 || post.DiscriptionOfIdea.toLowerCase().indexOf(search) > -1 || post.UserName.toLowerCase().indexOf(search) > -1
    })
  }

  render() {

    const posts = this.filter().map((post, index) => {
      return (
        <tr key={index}>
          <td style={{ textAlign: 'left' }}>{post.UserName}</td>
          <td style={{ textAlign: 'left' }}>{post.TitleOfIdea}</td>
          <td style={tr}>{post.DiscriptionOfIdea}</td>
          <td>
            <IdeaModal post={post} />
            <button className="button-edit" onClick={() => this.onDeletePost(post.Id)} data-upgraded=",MaterialButton">
              <i className="fa fa-trash-o fa-2x" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
      )
    })

    return (
      <div className="container" id="superAdminAccounts">

        <div className="row">
          <div className="col-md-4">
            <IdeaModal />
          </div>
          <div className="col-md-8">
            <div className="input-group" id="searchUsers">
              <span className="input-group-addon" id="basic-addon1">
                <i className="fa fa-search" aria-hidden="true"></i>
              </span>
              <input
                type="text"
                className="form-control"
                placeholder="Search idea post by title, description and username"
                aria-label="Username"
                aria-describedby="basic-addon1"
                value={this.state.search}
                onChange={e => this.setState({ search: e.target.value })} />
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-sm-12">
            <div className="card-table">
              <div className="pa-5" id="table-card">
                <table className="table table-responsive">
                  <thead>
                    <tr>
                      <th>USERNAME</th>
                      <th>TITLE</th>
                      <th>DESCRIPTION</th>
                      <th>ACTIONS</th>
                    </tr>
                  </thead>
                  <tbody>
                    {posts}
                  </tbody>
                </table>

                {posts.length === 0 && (
                  <p className="empty-text-arbi"><i className="fa fa-info" aria-hidden="true"></i> Idea post list is empty</p>
                )}
              </div>
            </div>
          </div>
        </div>

      </div>
    )
  }

}

const tr = {
  maxWidth: 300,
  textAlign: 'left'
}


export default connect(state => ({ idea: state.idea }))(Idea);