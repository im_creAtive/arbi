import React, { Component } from 'react'
import { NotificationManager } from 'react-notifications'

import * as ideaActions from '../../../../actions/ideaAction'
import * as ideaCommentActions from '../../../../actions/ideaCommentsAction'

const defState = {
  UserName: "Administrator",
  TitleOfIdea: "",
  DiscriptionOfIdea: "",
  TextOfIdea: "",
  comments: []
}

class IdeaModal extends Component {

  state = { ...defState }

  constructor(props) {
    super(props)

    this.state = this.props.post ? { ...this.props.post, comments: [] } : { ...defState }
  }

  async onSubmit() {
    let post = Object.assign({}, this.state)
    delete post.comments;

    try {
      if (post.TitleOfIdea === '') throw new Error('Title is required field')
      if (post.DiscriptionOfIdea === '') throw new Error('Text is required field')

      if (this.props.post) {
        let response = await ideaActions.modify(post)

        window.$("#modalBlog-" + this.props.post.Id).modal('hide');
        this.setState({ ...response })
        NotificationManager.success('Post successfully updated')
      } else {
        let response = await ideaActions.add(post)

        window.$("#modalBlog").modal('hide');
        this.setState({ ...defState })
        NotificationManager.success('Post successfully created')
      }

      ideaActions.get()
    } catch (e) {
      console.log(e)
      NotificationManager.error(e.message)
    }
  }

  async handleOpen(){
    let comments = await ideaCommentActions.getByIdeaId(this.props.post.Id)
    console.log('comments', comments)
    this.setState({ comments })
    window.$('#modalBlog-' + this.props.post.Id).modal('show')
  }

  async onDeleteComment(id){
    try{
      await ideaCommentActions.remove(id);
      let comments = await ideaCommentActions.getByIdeaId(this.props.post.Id)
      this.setState({ comments })

      NotificationManager.success('Comment successfully deleted')
    } catch(e) {
      console.log(e)
      NotificationManager.error(e.message)
    } 
  }

  render() {

    const modal = (
      <div className="modal fade" id={"modalBlog" + (this.props.post ? '-' + this.props.post.Id : '')} tabIndex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog modal-lg" role="document" style={{ textAlign: 'left' }} onClick={e => e.stopPropagation()}>
          <div className="modal-content">
            <div className="col-sm-12">
              <div className="modal-header">
                <h2 className="modal-title" id="exampleModalLabel">Idea post</h2>
              </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-6">
                    <div className="form-group">
                      <div className="col-sm-12 mdl-input p-0">
                        <label>USERNAME</label>
                        <input
                          type="text"
                          name="first-name"
                          className="form-control"
                          value={this.state.UserName}
                          onChange={e => this.setState({ UserName: e.target.value })}
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-sm-12 mdl-input p-0">
                        <label>TITLE OF IDEA</label>
                        <input
                          type="text"
                          name="first-name"
                          className="form-control"
                          value={this.state.TitleOfIdea}
                          onChange={e => this.setState({ TitleOfIdea: e.target.value })}
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-sm-12 mdl-input p-0">
                        <label>DESCRIPRION</label>
                        <textarea
                          className="form-control"
                          value={this.state.DiscriptionOfIdea}
                          onChange={e => this.setState({ DiscriptionOfIdea: e.target.value })}
                        ></textarea>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-sm-12 mdl-input p-0">
                        <label>TEXT</label>
                        <textarea
                          className="form-control"
                          value={this.state.TextOfIdea}
                          onChange={e => this.setState({ TextOfIdea: e.target.value })}
                          style={{ minHeight: 200 }}
                        ></textarea>
                      </div>
                    </div>
                  </div>
                  <div className="col-6" style={commentsStyle}>                    
                    {this.state.comments.length > 0 ? this.state.comments.map((comment, index) => {
                      return (
                        <div className="card arbi-card" key={index}>
                          <div className="card-block">
                            <p className="card-text">
                              <strong>{comment.UserName}</strong>
                              <p>{comment.TextOfComments}</p>
                              <a onClick={() => this.onDeleteComment(comment.Id)} href="#" className="btn btn-danger"><i className="fa fa-trash" aria-hidden="true"></i> Delete</a>
                            </p>
                          </div>
                        </div>
                      )
                    }) : (
                      <div style={ commentEmptyContainer }>
                        <p className="empty-text-arbi" style={{ alignSelf: 'center' }}><i className="fa fa-info" aria-hidden="true"></i> Comments list is empty</p>
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn save-btn" onClick={this.onSubmit.bind(this)} style={{ color: '#fff' }}>Save</button>
                <button type="button" className="btn cancel-btn" data-dismiss="modal" style={{ color: '#fff' }}>Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )

    return this.props.post ? (
      <button className="button-edit" data-upgraded=",MaterialButton" onClick={() => this.handleOpen()}>
        <i className="fa fa-pencil fa-2x" aria-hidden="true"></i>
        {modal}
      </button>
    ) : (
        <div>
          <div className="input-group" id="addAccount" data-toggle="modal" data-target="#modalBlog">
            <button type="button">
              <i className="fa fa-plus" aria-hidden="true"></i>
            </button>
            <input type="text" className="form-control" placeholder="Add post"
              aria-label="Username" aria-describedby="basic-addon1" disabled />
          </div>

          {modal}
        </div>
      )
  }

}

const commentsStyle = {
  overflowY: 'auto',
  flexDirection: 'column',
  maxHeight: 500
}
const commentEmptyContainer = {
  display: 'flex',
  flex: 1,
  justifyContent: 'center'
}

export default IdeaModal;