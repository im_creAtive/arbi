import { store } from './../index';
import { PAYMENTS } from './../reducers/constants';
import payments from './../api/payments';

export async function get() {
    let res = await payments.get();
    let data = await res.json();
    store.dispatch({ type: PAYMENTS.SET, payload: data });
}

export async function add(payment) {
    let res = await payments.add(payment);
    return await res.json();
}

export async function setPaymentPlan(email, planId, countOfMonth, dto){
    let res = await payments.setPaymentPlan(email, planId, countOfMonth, dto);
    return await res.json();
}
