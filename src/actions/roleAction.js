import { store } from './../index';
import { ROLES } from './../reducers/constants';
import roles from './../api/roles';

export async function get() {
    let res = await roles.get();
    let data = await res.json();
    store.dispatch({ type: ROLES.SET, payload: data });
}