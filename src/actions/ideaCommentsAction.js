import ideaComments from './../api/ideaComments';

export async function get() {
  let res = await ideaComments.get();
  let data = await res.json();
  
  return data;
}

export async function getByIdeaId(ideaId) {
  let res = await ideaComments.getByIdeaId(ideaId);
  return await res.json();
}

export async function add(comment, id) {
  let res = await ideaComments.add(comment, id);
  return await res.json();
}

export async function remove(id) {
  let res = await ideaComments.delete(id);
  return await res.json();
}
