import { store } from './../index';
import { PAYMENT_PLANS } from './../reducers/constants';
import paymentPlans from './../api/paymentPlans';

export async function get() {
    let res = await paymentPlans.get();
    let data = await res.json();
    store.dispatch({ type: PAYMENT_PLANS.SET, payload: data });
}

export async function getById(id) {
    let res = await paymentPlans.getById(id);
    return await res.json();
}

export async function getTypes() {
    let res = await paymentPlans.getTypes();
    let data = await res.json();
    store.dispatch({ type: PAYMENT_PLANS.TYPES, payload: data });
}

export async function modify(id, data) {
    let res = await paymentPlans.modify(id, data);
    return await res.json();
}

export async function add(data) {
    let res = await paymentPlans.add(data);
    return await res.json();
}

export async function remove(id) {
    let res = await paymentPlans.delete(id);
    return await res.json();
}

export function upgrade(id, countOfMonth = 1){
  return paymentPlans.upgrade(id, countOfMonth);
}

export function send(title, body){
  return paymentPlans.send(title, body);
}
