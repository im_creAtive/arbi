import { store } from './../index';
import { BLOG } from './../reducers/constants';
import { push } from 'react-router-redux';
import blog from './../api/blog';


export async function get() {
  let res = await blog.get();
  let data = await res.json();
  store.dispatch({ type: BLOG.SET, payload: data });
}

export async function getById(id) {
  let res = await blog.getById(id);
  return await res.json();
}

export async function modify(post) {
  let res = await blog.modify(post);
  return await res.json();
}

export async function add(post) {
  let res = await blog.add(post);
  return await res.json();
}

export async function remove(id) {
  let res = await blog.delete(id);
  return await res.json();
}

export async function uploadFile(id, file) {
  let res = await blog.uploadFile(id, file);
  return await res.json();
}
