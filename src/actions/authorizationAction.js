import config from './../api/config';
import authorization from './../api/authorization';
import { store } from '../index';
import { push } from 'react-router-redux';
 
export async function login(email, password) {
    let res = await authorization.login(email, password);
    var data = await res.json(); 
    config.setToken(data.access_token);
}

export async function logout() {
    config.delToken();
    store.dispatch(push('/login'));
}

export async function registration (user){
    let res = await authorization.registration(user);
    return await res.json(); 
}

export async function recoveryPassword (email){
    let res = await authorization.recovery(email);
    return await res.json(); 
}

export async function confirmPassword(pswd){
    let res = await authorization.confirmPassword(pswd);
    return await res.json(); 
}

