import { store } from './../index';
import { ACCOUNTS } from './../reducers/constants';
import accounts from './../api/accounts';

export async function get() {
    let res = await accounts.get();
    let data = await res.json();
    store.dispatch({ type: ACCOUNTS.SET, payload: data });
}

export async function modifyStatus(id, isActive){
    // console.log(id, isActive);
    let res = await accounts.modifyStatus(id, isActive);
    return await res.json();
}

export async function modifyPaymentPlan(id, paymentPlan){
    let res = await accounts.modifyPaymentPlan(id, paymentPlan);
    return await res.json();
}

export async function invisible(id, userId){
  let res = await accounts.invisible(id, userId);
  return await res.json();
}