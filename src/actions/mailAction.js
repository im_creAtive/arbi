import mail from './../api/mail';
import config from './../api/config';

export async function afterFeedback(email, body){
  let response = await mail.afterFeedback(email, body);

  if(response.status !== 200) throw new Error('Message not send');

  return true;
}