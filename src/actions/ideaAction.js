import { store } from './../index';
import { IDEA } from './../reducers/constants';
import { push } from 'react-router-redux';
import idea from './../api/idea';


export async function get() {
  let res = await idea.get();
  let data = await res.json();
  store.dispatch({ type: IDEA.SET, payload: data });
}

export async function getById(id) {
  let res = await idea.getById(id);
  return await res.json();
}

export async function modify(post) {
  let res = await idea.modify(post);
  return await res.json();
}

export async function add(post) {
  let res = await idea.add(post);
  return await res.json();
}

export async function remove(id) {
  let res = await idea.delete(id);
  return await res.json();
}
