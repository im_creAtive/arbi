import { store } from './../index';
import { MODELS } from './../reducers/constants';
import models from './../api/models';
import localforage from 'localforage';

localforage.clear();

export async function filter(filter) {
    let res = await models.filter(filter);
    return await res.json();
}

export async function add(model) {
    let res = await models.add(model);
    return await res.json();
}

export async function get() {
    let res = await models.get();
    return await res.json();
}

export async function remove(id) {
    let res = await models.delete(id);
    return await res.json();
}

export async function uploadPhoto(id, photo) {
    let res = await models.uploadPhoto(id, photo);
    return await res.json();
}

export async function uploadFiles(id, files) {
    let res = await models.uploadFiles(id, files);
    return await res.json();
}

export async function downloadFile(path, isShowLoader = true) {
    let blob = await localforage.getItem('@image:' + path);
    if(blob) {
      console.info(path + ' extracted from localforage');
      return blob;
    }

    let res = await models.downloadFile(path, isShowLoader);
    blob = await res.blob();

    localforage.setItem('@image:' + path, blob);

    return blob;
}