import { store } from './../index';
import { FOLDERS } from './../reducers/constants';
import folders from './../api/folders';
import config from './../api/config';

export async function get() {
    let res = await folders.get();
    let data = await res.json();

    data.sort((a, b) => {
      if(a === b) return 0;
      return Number(a.IndexOfFolder) > Number(b.IndexOfFolder) ? 1 : -1;
    })

    store.dispatch({ type: FOLDERS.SET, payload: data });
}

export async function add(folder) {
    let res = await folders.add(folder);
    return await res.json();
}

export async function remove(id) {
    let res = await folders.delete(id);
    return await res.json();
}

export function modify(folder){
  return folders.modify(folder.Id, folder);
}

export async function reIndex(folders){
  store.dispatch({ type: FOLDERS.SET, payload: folders });

  for(let i in folders){
    if(folders[i].Type === 'Custom'){
      folders[i].IndexOfFolder = "" + i;

      modify(folders[i]);
    }
  }

  store.dispatch({ type: FOLDERS.SET, payload: folders });
}
