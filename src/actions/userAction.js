import { store } from './../index';
import { USERS, USER } from './../reducers/constants';
import users from './../api/users';
import paymentPlans from './../api/paymentPlans'
import { push } from 'react-router-redux'
import { NotificationManager } from 'react-notifications'

export async function get() {
    let res = await users.get();
    let data = await res.json();
    store.dispatch({ type: USERS.SET, payload: data });
}

export async function getById(id) {
    let res = await users.getById(id);
    return await res.json();
}

export async function getCurrent() {
    try{
        let res = await users.getCurrent();
        var data = await res.json();
    
        // get user current plan
        res = await paymentPlans.current();
        let plan = await res.json();
    
        data.currentPlan = plan;
    
        store.dispatch({ type: USER.SET, payload: data });
        return data;  
    } catch(e) {
        NotificationManager.error('Account don\'t  exist or inactive. Please contact your admin.');
        localStorage.removeItem('SET_USER_TOKEN');
        store.dispatch(push('/login'));
    }
}

export async function modify(id, data) {
    let res = await users.modify(id, data);
    return await res.json();
}

export async function modifyCurrent(data) {
    let res = await users.modifyCurrent(data);
    return await res.json();
}

export async function add(data) {
    let res = await users.add(data);
    return await res.json();
}

export async function remove(id) {
    let res = await users.delete(id);
    return await res.json();
}

export async function modifyStatus(id, isActive){
    console.log(id, isActive);
    let res = await users.modifyStatus(id, isActive);
    return await res.json();
}

export async function createByAccount(user) {
    let res = await users.createByAccount(user);
    return await res.json();
}
