import { store } from './../index';
import { KNOWLEDGE } from './../reducers/constants';
import { push } from 'react-router-redux';
import knowledge from './../api/knowledge';


export async function get() {
  let res = await knowledge.get();
  let data = await res.json();
  store.dispatch({ type: KNOWLEDGE.SET, payload: data });
}

export async function getById(id) {
  let res = await knowledge.getById(id);
  return await res.json();
}

export async function modify(post) {
  let res = await knowledge.modify(post);
  return await res.json();
}

export async function add(post) {
  let res = await knowledge.add(post);
  return await res.json();
}

export async function remove(id) {
  let res = await knowledge.delete(id);
  return await res.json();
}
