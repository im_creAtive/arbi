import messages from './../api/messages';
import config from './../api/config';

export async function customer(email, body){
  let response = await messages.customer(email, body);

  if(response.status !== 200) throw new Error('Message not send');

  return true;
}

export async function admin(body){
  let response = await messages.admin(body)

  if(response.status !== 200) throw new Error('Message not send');

  return true;
}