import { store } from './../index';
import { MODELS } from './../reducers/constants';
import targets from './../api/targets';
import config from './../api/config';



// export async function get() {
//     let res = await targets.get();
//     let data = await res.json();
//     store.dispatch({ type: TARGETS.SET, payload: data });
// }

export async function filter(filter) {
    let res = await targets.filter(filter);
    return await res.json();
}

export async function add(target) {
    let res = await targets.add(target);
    return await res.json();
}

export async function uploadFile(id, file) {
    let res = await targets.uploadFile(id, file);
    return await res.json();
}

export async function remove(id) {
    let res = await targets.delete(id);
    return await res.json();
}
