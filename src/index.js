import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createHistory from 'history/createBrowserHistory';
import { BrowserRouter, Link, Route, Switch, Redirect } from 'react-router-dom';
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux';
import { NotificationContainer } from 'react-notifications';
import { syncWithStore, setLanguage } from 'react-redux-multilang';

import reducer from './reducers/reducer';
//Notification Module styles
import 'react-notifications/lib/notifications.css';

import './sources/css/bootstrap/css/bootstrap.min.css';
import './sources/css/font-awesome-4.7.0/css/font-awesome.min.css';
import './index.css';

import registerServiceWorker from './registerServiceWorker';
import LandingPage from './components/landing/landingPage';
import LoginPage from './components/landing/loginPage';
import CabinetPage from './components/cabinet/cabinetPage';
import SuperAdminPage from './components/superadmin/superAdminPage';
import SignUp from './components/landing/signup';
import EmailConfirmed from './components/landing/emailConfirmed';
// import Landing from './components/landing-new';
import Landing from './components/landing/landingPage';

import Loader from './components/public/loader/loader';


// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory();
const routerMW = routerMiddleware(history);

// Build the middleware for intercepting and dispatching navigation actions
const middleware = routerMiddleware(history)

// Add the reducer to your store on the `router` key
// Also apply our middleware for navigating
export const store = createStore(reducer, applyMiddleware(routerMW, thunk));
// set language
let language = localStorage.getItem('language');
if(!language || ['en','nl'].indexOf(language) < 0) language = 'en'
syncWithStore(store);
setLanguage(language);

// Now you can dispatch navigation actions from anywhere!
// store.dispatch(push('/foo'))

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history} >
      <div>
        <Loader />
        <NotificationContainer/>
        <Switch>
          <Route path="/login" component={LoginPage} />
          <Route path="/cabinet" component={CabinetPage} />
          <Route path="/signup/:planId" component={SignUp} />
          <Route path="/emailconfirmed/:email" component={EmailConfirmed} />
          <Route path="/superadmin" component={SuperAdminPage} />
          <Route path="/" component={Landing} />
        </Switch>
      </div>
    </ConnectedRouter>
  </Provider>,
	document.getElementById('root'));
registerServiceWorker();

