import { PAYMENT_PLANS } from './constants';

const initialState = {
  list: [],
  types: []
};

export default function(state = initialState, action){

  switch (action.type) {
        case PAYMENT_PLANS.SET:
            return {...state, list: action.payload };
        case PAYMENT_PLANS.TYPES:
            return {...state, types: action.payload };
        default:
            return state;
    }
}
