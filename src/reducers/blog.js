import { BLOG } from './constants';

const initialState = {
  list: []
};

export default function (state = initialState, action) {

  switch (action.type) {
    case BLOG.SET:
      return { ...state, list: action.payload };
    default:
      return state;
  }
}
