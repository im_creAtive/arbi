import { MODELS } from './constants';

const initialState = {
    list: []
};

export default function(state = initialState, action){

  switch (action.type) {
        case MODELS.SET:
            return {...state, page: action.payload };
        default:
            return state;
    }
}
