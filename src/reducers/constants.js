export const LOADER = {
    SHOW_LOADER: 'SHOW_LOADER',
    HIDE_LOADER: 'HIDE_LOADER'
  }

  export const USERS = {
    SET: 'USERS'
  }

  export const USER = {
    SET_USER_TOKEN: 'SET_USER_TOKEN',
    SET: 'USER'
  }

  export const ROLES = {
    SET: 'ROLES'
  }

  export const ACCOUNTS = {
    SET: 'ACCOUNTS'
  }

  export const PAYMENTS = {
    SET: 'PAYMENTS'
  }

  export const BLOG = {
    SET: 'BLOG'
  }

  export const KNOWLEDGE = {
    SET: 'KNOWLEDGE'
  }

  export const IDEA = {
    SET: 'IDEA'
  }

  export const PAYMENT_PLANS = {
    SET: 'PAYMENT_PLANS',
    TYPES: 'PAYMENT_PLAN_TYPES'
  }

  export const FOLDERS = {
    SET: 'FOLDERS',
  }

  export const MODELS = {
    SET: 'MODELS'
  }

  export const CURRENT_USER = {
    SET: 'CURRENT_USER_SET'
  }

  export const TARGETS = {
    SET: 'TARGETS'
  }
