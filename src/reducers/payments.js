import { PAYMENTS } from './constants';

const initialState = {
    bundle: {
        Balance:{
            Total: 0,
            Used: 0,
        },
        History: []
    }
};

export default function(state = initialState, action){

  switch (action.type) {
        case PAYMENTS.SET:
            return {...state, bundle: action.payload };
        default:
            return state;
    }
}
