import { combineReducers } from 'redux';
import { langReducer } from 'react-redux-multilang';
import loader from './loader';
import user from './user';
import users from './users';
import folders from './folders';
import models from './models';
import roles from './roles';
import accounts from './accounts';
import paymentPlans from './paymentPlans';
import payments from './payments';
import blog from './blog';
import knowledge from './knowledge'
import idea from './idea'

//import userModal from './modals/user';
/*import visibilityFilter from './visibilityFilter'*/

export default combineReducers({
  /*modal: combineReducers(
      {
        userModal
      }
    ),*/
  lang: langReducer,
  loader,
  user,
  users,
  folders,
  models,
  roles,
  accounts,
  payments,
  paymentPlans,
  blog,
  knowledge,
  idea
})
