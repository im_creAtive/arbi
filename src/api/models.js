import config from './config';
import req from './req';

const apiRoute = 'models';
const fullApiPath = config.apiHost + apiRoute;

class models{

    static filter(filter){
        return req.post(fullApiPath + '/filter', filter);
    }

    static getById(id){
        return req.get(fullApiPath + '/' + id);
    }

    static add(model){
        return req.post(fullApiPath, model);
    }

    static get(){
        return req.get(fullApiPath);
    }

    static uploadPhoto(id, photo){
        return req.uploadFile(fullApiPath + "/UploadPhoto?id=" + id , photo);
    }

    static uploadFiles(id, files){
        return req.uploadFiles(fullApiPath + "/UploadFiles?id=" + id , files);
    }

    static downloadFile(path, isShowLoader){
        if(!path) throw new Error('Path is empty')
        return req.get(config.baseHost + "Files/Download" + path , null, false, isShowLoader);
    }

    static modify(id, model){
        return req.put(fullApiPath + '/' + id, model);
    }

    static delete(id){
        return req.delete(fullApiPath + '/' + id);
    }
}

export default models;
