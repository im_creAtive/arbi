import config from './config';
import req from './req';

const apiRoute = 'CommentsFeatureIdeas';
const fullApiPath = config.apiHost + apiRoute;

class ideaComments {

  static get() {
    return req.get(fullApiPath);
  }

  static getByIdeaId(ideaId) {
    return req.get(fullApiPath + '/GetAllComments?id=' + ideaId);
  }

  static add(comment, id) {
    return req.post(fullApiPath + '/' + id, comment);
  }

  static delete(id) {
    return req.delete(fullApiPath + '/' + id);
  }

}

export default ideaComments;
