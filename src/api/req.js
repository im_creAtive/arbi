import config, { selectedUserId } from './config';
import { LOADER } from '../reducers/constants';
import { store } from '../index';

class req{

    static async get(url, headers = null, isJson = true, isShowLoader = true){
        return await this.request(url,"GET", null, headers, isJson, isShowLoader);
    }

    static async post(url, body, headers = null, isJson = true, isShowLoader = true){
        return await this.request(url,"POST", body, headers, isJson, isShowLoader);
    }

    static async put(url, body, headers = null, isJson = true, isShowLoader = true){
        return await this.request(url,"PUT", body, headers, isJson, isShowLoader);
    }

    static async patch(url, body, headers = null, isJson = true, isShowLoader = true){
        return await this.request(url,"PATCH", body, headers, isJson, isShowLoader);
    }

    static async delete(url, body, headers = null, isJson = true, isShowLoader = true){
        return await this.request(url,"DELETE", body, headers, isJson, isShowLoader);
    }

    static async uploadFile(url, file, headers = null, isShowLoader = true){
        var base64File = await this.createBase64File(file);
        return await this.request(url,"POST", base64File, headers, true, isShowLoader);
    }

    static async uploadFiles(url, files, headers = null, isShowLoader = true){
        var base64Files = [];
        for(var i = 0; i < files.length ; i++){
            base64Files.push(await this.createBase64File(files[i]))
        }
        return await this.request(url,"POST", base64Files, headers, true, isShowLoader);
    }

    static async createBase64File (file){
        var base64File = {
            Name: "",
            Body: "",
            Type: ""
        };
        base64File.Name = file.name;
        base64File.Type = file.target ? file.target : file.type;
        base64File.Body = await this.getBase64(file);

        return base64File;
    }

    static async getBase64(file) {
        return new Promise((resolve, reject) => {
          const reader = new FileReader();
          reader.readAsBinaryString(file);
          reader.onload = () => resolve(btoa(reader.result));
          reader.onerror = error => reject(error);
        });
      }

    static async request (url, method, body, headers, isJson, isShowLoader){
        try{
            if (isShowLoader){
                store.dispatch({ type: LOADER.SHOW_LOADER });
            }
            console.log('Req: ' + method + ':' + url);
            if (!headers){
                headers= { 'Content-Type': 'application/json' };
            }
            let data = {
                mode: 'cors',
                method: method,
                headers: headers,
            };

            if (body && isJson){
                body = JSON.stringify(body);
                headers['Content-Length'] = body.length;
            }

            var token = config.getToken();
            if (token){
                headers['Authorization'] = 'bearer ' + token;
            }

            if(selectedUserId){
              headers['Selected-User-Id'] = selectedUserId;
            }

            if (body){
                data.body = body;
            }
            var r = await fetch(url, data);
            if (r.status != 200 && r.status != 201){
                let data = await r.text();
                return Promise.reject(data);
            }
            return r;
        }catch(e){
            console.error('Req: ' + method + ':' + url + ' ::' + e);
            throw e;
        }finally{
            if (isShowLoader){
                store.dispatch({ type: LOADER.HIDE_LOADER });
            }
        }
    }
}

export default req;
