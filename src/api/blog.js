import config from './config';
import req from './req';

const apiRoute = 'BlogArticle';
const fullApiPath = config.apiHost + apiRoute;

class blog {

  static get() {
    return req.get(fullApiPath);
  }

  static getById(id) {
    return req.get(fullApiPath + '/' + id);
  }

  static add(post) {
    return req.post(fullApiPath, post);
  }

  static modify(post) {
    return req.patch(fullApiPath + '/' + post.Id, post);
  }

  static delete(id) {
    return req.delete(fullApiPath + '/' + id);
  }

  static uploadFile(id, file) {
    return req.uploadFile(fullApiPath + "/UploadPhoto?id=" + id, file);
  }
  
}

export default blog;
