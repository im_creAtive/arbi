import config from './config';
import req from './req';

const apiRoute = 'payments';
const fullApiPath = config.apiHost + apiRoute;

class payments{

    static get(){
        return req.get(fullApiPath);
    }

    static getById(id){
        return req.get(fullApiPath + '/' + id);
    }

    static add(payment){
        return req.post(fullApiPath, payment);
    }

    static modify(id, payment){
        return req.put(fullApiPath + '/' + id, payment);
    }

    static delete(id){
        return req.delete(fullApiPath + '/' + id);
    }

    static setPaymentPlan(email, planId, countOfMonth, dto){
        return req.post(fullApiPath + `/setPaymentPlan?email=${email}&paymentPlanId=${planId}&countOfMonth=${countOfMonth}`, dto);
    }
}

export default payments;
