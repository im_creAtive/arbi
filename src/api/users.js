import config from './config';
import req from './req';

const apiRoute = 'users';
const fullApiPath = config.apiHost + apiRoute;

class users {

    static get(){
        return req.get(fullApiPath);
    }

    static getById(id){
        return req.get(fullApiPath + '/' + id);
    }

    static getCurrent(){
        return req.get(fullApiPath + '/CurrentUser');
    }

    static add(user){
        return req.post(fullApiPath, user);
    }

    static modify(id, user){
        return req.patch(fullApiPath + '/' + id, user);
    }

    static modifyStatus(id, isActive){
        return req.patch(fullApiPath + '/Status?id=' + id + '&isActive='+isActive);
    }

    static modifyCurrent(user){
        return req.patch(fullApiPath + '/CurrentUser', user);
    }

    static delete(id){
        return req.delete(fullApiPath + '/' + id);
    }

    static createByAccount(user){
        return req.post(fullApiPath + '/CreateByAccount', user);
    }
}

export default users;
