import config from './config';
import req from './req';

const apiRoute = 'FeatureIdeas';
const fullApiPath = config.apiHost + apiRoute;

class idea {

  static get() {
    return req.get(fullApiPath);
  }

  static getById(id) {
    return req.get(fullApiPath + '/GetById?id=' + id);
  }

  static add(post) {
    return req.post(fullApiPath, post);
  }

  static modify(post) {
    return req.patch(fullApiPath + '/' + post.Id, post);
  }

  static delete(id) {
    return req.delete(fullApiPath + '/' + id);
  }

}

export default idea;
