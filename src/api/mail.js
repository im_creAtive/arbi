import config from './config';
import req from './req';

const apiRoute = 'mail';
const fullApiPath = config.apiHost + apiRoute;

class mail{

    static afterFeedback(email, body){
      return req.post(fullApiPath + '/afterFeedback?email=' + email, body);
    }

}

export default mail;
