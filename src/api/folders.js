import config from './config';
import req from './req';

const apiRoute = 'folders';
const fullApiPath = config.apiHost + apiRoute;

class folders{

    static get(){
        return req.get(fullApiPath);
    }
    static getById(id){
        return req.get(fullApiPath + '/' + id);
    }

    static add(folder){
        return req.post(fullApiPath, folder);
    }

    static modify(id, folder){
        return req.patch(fullApiPath + '/' + id, folder);
    }

    static delete(id){
        return req.delete(fullApiPath + '/' + id);
    }
}

export default folders;
