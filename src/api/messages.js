import config from './config';
import req from './req';

const apiRoute = 'messages';
const fullApiPath = config.apiHost + apiRoute;

class messages{

    static customer(email, body){
      return req.post(fullApiPath + '?email=' + email, body);
    }

    static admin(body){
      return req.post(fullApiPath + '/FeedbackMessages', body);
    }
}

export default messages;
