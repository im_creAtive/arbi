import { store } from '../index';

const url = {
    baseHost: 'https://arbi.io:444/'/*http://dev.bidon-tech.com/dev.arbi/*/,
    apiPrefix: 'api/'
};

export let selectedUserId = null;

const config = {
    baseHost: url.baseHost,
    apiHost: url.baseHost + url.apiPrefix,
    getToken: () => {
        return store.getState().user.access_token ? store.getState().user.access_token : localStorage.getItem('SET_USER_TOKEN');
    },
    setToken: (token) => {
        localStorage.setItem('SET_USER_TOKEN', token);
        return token;
    },
    delToken: (token) => {
        localStorage.removeItem('SET_USER_TOKEN');
    },
    setSelectedUserId: (id) => {
      selectedUserId = id;
    },
    delSelectedUserId: () => {
      selectedUserId = null;
    }
}

export default config;
