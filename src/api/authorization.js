import config from './config';
import req from './req';

const apiRoute = 'login';
const fullApiPath = config.apiHost + apiRoute;

class authorization{
    static login(email, password){
        return req.post(fullApiPath + '/token', 'grant_type=password&userName=' + email + '&password=' + password, null, false);
    }

    static recovery(email){
        return req.post(fullApiPath + '/recovery', {email});
    }

    static registration(user){
        return req.post(fullApiPath + '/register', user);
    }

    static getCurrentUser(){
        return req.get(fullApiPath + '/user');
    }

    static confirmPassword(pswd){
        return req.post(fullApiPath + '/confirmPassword', pswd);
    }
}

export default authorization;
