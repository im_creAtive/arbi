import config from './config';
import req from './req';

const apiRoute = 'accounts';
const fullApiPath = config.apiHost + apiRoute;

class accounts {

    static get(){
        return req.get(fullApiPath);
    }

    // static getById(id){
    //     return req.get(fullApiPath + '/' + id);
    // }
    //
    // static getCurrent(){
    //     return req.get(fullApiPath + '/CurrentUser');
    // }
    //
    // static add(user){
    //     return req.post(fullApiPath, user);
    // }
    //
    // static modify(id, user){
    //     return req.put(fullApiPath + '/' + id, user);
    // }

    static modifyStatus(id, isActive){
        return req.put(fullApiPath + '/Status?id=' + id + '&isActive='+isActive);
    }

    static modifyPaymentPlan(id, paymentPlan){
        return req.put(fullApiPath + '/PaymentPlan?id=' + id + '&paymentPlanId='+paymentPlan);
    }
    // static modifyCurrent(user){
    //     return req.put(fullApiPath + '/CurrentUser', user);
    // }
    //
    // static delete(id){
    //     return req.delete(fullApiPath + '/' + id);
    // }

    static invisible(id, userId){
        return req.put(fullApiPath + '/Invisible?id='+ id +'&userId='+ userId)
    }
}

export default accounts;
