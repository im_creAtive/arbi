import config from './config';
import req from './req';

const apiRoute = 'targets';
const fullApiPath = config.apiHost + apiRoute;

class targets{
    static get(){
        return req.get(fullApiPath);
    }

    static filter(filter){
        return req.post(fullApiPath + '/filter', filter);
    }

    static getById(id){
        return req.get(fullApiPath + '/' + id);
    }

    static add(target){
        return req.post(fullApiPath, target);
    }

    static modify(id, target){
        return req.put(fullApiPath + '/' + id, target);
    }

    static delete(id){
        return req.delete(fullApiPath + '/' + id);
    }

    static uploadFile(id, file){
        return req.uploadFile(fullApiPath + "/Upload?id=" + id , file);
    }
}

export default targets;
