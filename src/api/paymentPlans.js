import config from './config';
import req from './req';

const apiRoute = 'paymentPlans';
const fullApiPath = config.apiHost + apiRoute;

class paymentPlans{

    static getTypes(){
        return req.get(fullApiPath + '/types');
    }

    static get(){
        return req.get(fullApiPath);
    }

    static getById(id){
        return req.get(fullApiPath + '/' + id);
    }

    static add(paymentType){
        return req.post(fullApiPath, paymentType);
    }

    static modify(id, paymentType){
        return req.put(fullApiPath + '/' + id, paymentType);
    }

    static delete(id){
        return req.delete(fullApiPath + '/' + id);
    }

    static upgrade(id, countOfMonth = 1){
        return req.put(config.apiHost + `Payments/PaymentPlan?paymentPlanId=${id}&countOfMonth=${countOfMonth}`);
    }

    static send(title, body){
        return req.post(fullApiPath + '/send?title='+ title +'&body='+ body);
    }

    static current(){
        return req.get(fullApiPath + '/current');
    }
}

export default paymentPlans;
