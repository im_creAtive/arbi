import config from './config';
import req from './req';

const apiRoute = 'KnowledgeBaseArticle';
const fullApiPath = config.apiHost + apiRoute;

class knowledge {

  static get() {
    return req.get(fullApiPath);
  }

  static getById(id) {
    return req.get(fullApiPath + '/' + id);
  }

  static add(post) {
    return req.post(fullApiPath, post);
  }

  static modify(post) {
    return req.patch(fullApiPath + '/' + post.Id, post);
  }

  static delete(id) {
    return req.delete(fullApiPath + '/' + id);
  }

}

export default knowledge;
