import {NotificationManager} from 'react-notifications';

export async function success(message) {
    NotificationManager.success(message);
}

export async function error(obj){
    if (IsJsonString(obj)){
        obj = JSON.parse(obj);
    }

    if (typeof obj === 'object'){
        errorAsObject(obj)
    }else{
        errorAsMessage(obj);
    }
}

export async function errorAsObject(obj){
    var message = obj.Message ? obj.Message : "Error";
    errorAsMessage(message);
}

export async function errorAsMessage(message){
    NotificationManager.error(message);
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
